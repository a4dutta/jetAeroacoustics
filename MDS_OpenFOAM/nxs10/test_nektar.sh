#!/bin/bash
#SBATCH -n 160
#SBATCH --mem-per-cpu 3000
#SBATCH -o slurm.%N.%j.out
#SBATCH -t 3:00:00 





# Set up the environment


module load intel/2016.4

module load openmpi/2.1.1

module load nektar++


mpirun -np 160 IncNavierStokesSolver nxs10.xml > a.out

echo "Finished at `date`"
