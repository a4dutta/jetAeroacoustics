# Pointwise V18.0R4 Journal file - Wed May 23 08:58:09 2018

package require PWI_Glyph 2.18.0

pw::Application setUndoMaximumLevels 5
pw::Application reset
pw::Application markUndoLevel {Journal Reset}

pw::Application clearModified

set _TMP(mode_1) [pw::Application begin Create]
  set _TMP(PW_1) [pw::SegmentSpline create]
  $_TMP(PW_1) addPoint {0 0.5 0}
  $_TMP(PW_1) addPoint {0 -0.5 0}
  set _TMP(con_1) [pw::Connector create]
  $_TMP(con_1) addSegment $_TMP(PW_1)
  unset _TMP(PW_1)
  $_TMP(con_1) calculateDimension
$_TMP(mode_1) end
unset _TMP(mode_1)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_2) [pw::Application begin Create]
  set _TMP(PW_2) [pw::SegmentSpline create]
  $_TMP(PW_2) addPoint {0 -0.5 0}
  $_TMP(PW_2) addPoint {50 -0.5 0}
  unset _TMP(con_1)
  set _TMP(con_2) [pw::Connector create]
  $_TMP(con_2) addSegment $_TMP(PW_2)
  unset _TMP(PW_2)
  $_TMP(con_2) calculateDimension
$_TMP(mode_2) end
unset _TMP(mode_2)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_3) [pw::Application begin Create]
  set _TMP(PW_3) [pw::SegmentSpline create]
  pw::Display resetView -Z
  $_TMP(PW_3) addPoint {50 -0.5 0}
  $_TMP(PW_3) addPoint {50 0.5 0}
  unset _TMP(con_2)
  set _TMP(con_3) [pw::Connector create]
  $_TMP(con_3) addSegment $_TMP(PW_3)
  unset _TMP(PW_3)
  $_TMP(con_3) calculateDimension
$_TMP(mode_3) end
unset _TMP(mode_3)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_4) [pw::Application begin Create]
  set _TMP(PW_4) [pw::SegmentSpline create]
  $_TMP(PW_4) addPoint {0 0.5 0}
  $_TMP(PW_4) addPoint {50 0.5 0}
  unset _TMP(con_3)
  set _TMP(con_4) [pw::Connector create]
  $_TMP(con_4) addSegment $_TMP(PW_4)
  unset _TMP(PW_4)
  $_TMP(con_4) calculateDimension
$_TMP(mode_4) end
unset _TMP(mode_4)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_5) [pw::Application begin Create]
  set _TMP(PW_5) [pw::SegmentSpline create]
  $_TMP(PW_5) addPoint {50 0.5 0}
  $_TMP(PW_5) addPoint {50 2.5 0}
  unset _TMP(con_4)
  set _TMP(con_5) [pw::Connector create]
  $_TMP(con_5) addSegment $_TMP(PW_5)
  unset _TMP(PW_5)
  $_TMP(con_5) calculateDimension
$_TMP(mode_5) end
unset _TMP(mode_5)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_6) [pw::Application begin Create]
  set _TMP(PW_6) [pw::SegmentSpline create]
  $_TMP(PW_6) addPoint {50 2.5 0}
  $_TMP(PW_6) addPoint {50 0 0}
  unset _TMP(con_5)
  set _TMP(con_6) [pw::Connector create]
  $_TMP(con_6) addSegment $_TMP(PW_6)
  unset _TMP(PW_6)
  $_TMP(con_6) calculateDimension
$_TMP(mode_6) end
unset _TMP(mode_6)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_7) [pw::Application begin Create]
  set _TMP(PW_7) [pw::SegmentSpline create]
  $_TMP(PW_7) delete
  unset _TMP(PW_7)
$_TMP(mode_7) abort
unset _TMP(mode_7)
$_TMP(con_6) delete
pw::Application markUndoLevel {Delete Last Curve}

set _TMP(mode_8) [pw::Application begin Create]
  set _TMP(PW_8) [pw::SegmentSpline create]
  unset _TMP(con_6)
  $_TMP(PW_8) delete
  unset _TMP(PW_8)
$_TMP(mode_8) abort
unset _TMP(mode_8)
set _TMP(mode_9) [pw::Application begin Create]
  set _TMP(PW_9) [pw::SegmentSpline create]
  $_TMP(PW_9) addPoint {0 2.5 0}
  $_TMP(PW_9) addPoint {50 2.5 0}
  set _TMP(con_7) [pw::Connector create]
  $_TMP(con_7) addSegment $_TMP(PW_9)
  unset _TMP(PW_9)
  $_TMP(con_7) calculateDimension
$_TMP(mode_9) end
unset _TMP(mode_9)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_10) [pw::Application begin Create]
  set _TMP(PW_10) [pw::SegmentSpline create]
  $_TMP(PW_10) addPoint {0 2.5 0}
  $_TMP(PW_10) addPoint {0 0.5 0}
  unset _TMP(con_7)
  set _TMP(con_8) [pw::Connector create]
  $_TMP(con_8) addSegment $_TMP(PW_10)
  unset _TMP(PW_10)
  $_TMP(con_8) calculateDimension
$_TMP(mode_10) end
unset _TMP(mode_10)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_10) [pw::Application begin Create]
  set _TMP(PW_11) [pw::SegmentSpline create]
  $_TMP(PW_11) addPoint {0 -0.5 0}
  $_TMP(PW_11) addPoint {0 -2.5 0}
  unset _TMP(con_8)
  set _TMP(con_9) [pw::Connector create]
  $_TMP(con_9) addSegment $_TMP(PW_11)
  unset _TMP(PW_11)
  $_TMP(con_9) calculateDimension
$_TMP(mode_10) end
unset _TMP(mode_10)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_10) [pw::Application begin Create]
  set _TMP(PW_12) [pw::SegmentSpline create]
  $_TMP(PW_12) addPoint {0 -2.5 0}
  $_TMP(PW_12) addPoint {50 -2.5 0}
  unset _TMP(con_9)
  set _TMP(con_10) [pw::Connector create]
  $_TMP(con_10) addSegment $_TMP(PW_12)
  unset _TMP(PW_12)
  $_TMP(con_10) calculateDimension
$_TMP(mode_10) end
unset _TMP(mode_10)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_10) [pw::Application begin Create]
  set _TMP(PW_13) [pw::SegmentSpline create]
  $_TMP(PW_13) addPoint {50 -2.5 0}
  $_TMP(PW_13) addPoint {50 -0.5 0}
  unset _TMP(con_10)
  set _TMP(con_11) [pw::Connector create]
  $_TMP(con_11) addSegment $_TMP(PW_13)
  unset _TMP(PW_13)
  $_TMP(con_11) calculateDimension
$_TMP(mode_10) end
unset _TMP(mode_10)
pw::Application markUndoLevel {Create 2 Point Connector}

set _TMP(mode_10) [pw::Application begin Create]
  set _TMP(PW_14) [pw::SegmentSpline create]
  $_TMP(PW_14) delete
  unset _TMP(PW_14)
$_TMP(mode_10) abort
unset _TMP(mode_10)
unset _TMP(con_11)
set _CN(1) [pw::GridEntity getByName "con-4"]
set _CN(2) [pw::GridEntity getByName "con-9"]
set _CN(3) [pw::GridEntity getByName "con-6"]
set _CN(4) [pw::GridEntity getByName "con-2"]
set _TMP(PW_15) [pw::Collection create]
$_TMP(PW_15) set [list $_CN(1) $_CN(2) $_CN(3) $_CN(4)]
$_TMP(PW_15) do setDimension 100
$_TMP(PW_15) delete
unset _TMP(PW_15)
pw::Application markUndoLevel {Dimension}

set _TMP(PW_16) [pw::Collection create]
$_TMP(PW_16) set [list $_CN(1) $_CN(2) $_CN(3) $_CN(4)]
$_TMP(PW_16) do setRenderAttribute PointMode All
$_TMP(PW_16) delete
unset _TMP(PW_16)
pw::Application markUndoLevel {Modify Entity Display}

set _CN(5) [pw::GridEntity getByName "con-3"]
set _CN(6) [pw::GridEntity getByName "con-1"]
set _TMP(PW_17) [pw::Collection create]
$_TMP(PW_17) set [list $_CN(5) $_CN(6)]
$_TMP(PW_17) do setDimension 5
$_TMP(PW_17) delete
unset _TMP(PW_17)
pw::Application markUndoLevel {Dimension}

set _TMP(PW_18) [pw::Collection create]
$_TMP(PW_18) set [list $_CN(5) $_CN(6)]
$_TMP(PW_18) do setRenderAttribute PointMode All
$_TMP(PW_18) delete
unset _TMP(PW_18)
pw::Application markUndoLevel {Modify Entity Display}

set _CN(7) [pw::GridEntity getByName "con-5"]
set _CN(8) [pw::GridEntity getByName "con-10"]
set _CN(9) [pw::GridEntity getByName "con-7"]
set _CN(10) [pw::GridEntity getByName "con-8"]
set _TMP(PW_19) [pw::Collection create]
$_TMP(PW_19) set [list $_CN(7) $_CN(8) $_CN(9) $_CN(10)]
$_TMP(PW_19) do setDimension 10
$_TMP(PW_19) delete
unset _TMP(PW_19)
pw::Application markUndoLevel {Dimension}

set _TMP(PW_20) [pw::Collection create]
$_TMP(PW_20) set [list $_CN(7) $_CN(8) $_CN(9) $_CN(10)]
$_TMP(PW_20) do setRenderAttribute PointMode All
$_TMP(PW_20) delete
unset _TMP(PW_20)
pw::Application markUndoLevel {Modify Entity Display}

set _TMP(PW_21) [pw::DomainStructured createFromConnectors -reject _TMP(unusedCons) -solid [list $_CN(7) $_CN(8) $_CN(9) $_CN(10)]]
unset _TMP(unusedCons)
unset _TMP(PW_21)
pw::Application markUndoLevel {Assemble Domains}

set _TMP(PW_22) [pw::DomainStructured createFromConnectors -reject _TMP(unusedCons) -solid [list $_CN(7) $_CN(8) $_CN(9) $_CN(10)]]
unset _TMP(unusedCons)
unset _TMP(PW_22)
pw::Application markUndoLevel {Assemble Domains}

set _TMP(PW_23) [pw::DomainStructured createFromConnectors -reject _TMP(unusedCons) -solid [list $_CN(1) $_CN(7) $_CN(2) $_CN(8) $_CN(5) $_CN(9) $_CN(10) $_CN(3) $_CN(6) $_CN(4)]]
unset _TMP(unusedCons)
unset _TMP(PW_23)
pw::Application markUndoLevel {Assemble Domains}

set _TMP(PW_24) [pw::Collection create]
$_TMP(PW_24) set [list $_CN(1) $_CN(2) $_CN(3) $_CN(4)]
$_TMP(PW_24) do setDimension 125
$_TMP(PW_24) delete
unset _TMP(PW_24)
pw::Application markUndoLevel {Dimension}

set _TMP(PW_25) [pw::Collection create]
$_TMP(PW_25) set [list $_CN(1) $_CN(2) $_CN(3) $_CN(4)]
$_TMP(PW_25) do setDimension 150
$_TMP(PW_25) delete
unset _TMP(PW_25)
pw::Application markUndoLevel {Dimension}

pw::Display resetView -Z
