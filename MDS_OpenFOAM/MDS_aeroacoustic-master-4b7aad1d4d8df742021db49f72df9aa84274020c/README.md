MDS-Waterloo project
_______________________________________________
This folder contains all the information for the MDS/Waterloo project.


Deliverables:

    -Code V&V, scaling (month 1)
    -Boundary condition implementation (months 1 and 2)
    -Steady-state parametrization (month 2 and 3)
    -Unsteady simulations (month 3 to 6)
    -Partially confined jet (month 5 to 11)
    -Full simulations (month 6 to 11)
    -Low-order model (7 to 12)


The repo should contain all completed simulations results (minimize size of files)
