// Gmsh project created on Mon Dec 12 10:21:54 2016
ls1 = 0.005;
//+
Point(1) = {0, 0, 0, ls1};
//+
Point(2) = {0.17043, 0, 0, ls1};
//+
Point(3) = {0.202798974573, 0, 0, ls1};
//+
Point(4) = {0.213778974573, 0, 0, ls1};
//+
Point(5) = {0.263698974573, 0, 0, ls1};
//+
Point(6) = {1, 0, 0, ls1};
//+
Point(7) = {0, 0.038545, 0, ls1/4};
//+
Point(8) = {0.17043, 0.038545, 0, ls1/2};
//+
Point(9) = {0.202798974573, 0.038545, 0, ls1/2};
//+
Point(10) = {0.213778974573, 0.038545, 0, ls1/2};
//+
Point(11) = {0.263698974573, 0.038545, 0, ls1/2};
//+
Point(12) = {1, 0.038545, 0, ls1};
//+
Point(13) = {0, 0.043545, 0, ls1/4};
//+
Point(14) = {0.17043, 0.043545, 0, ls1/2};
//+
Point(15) = {0.202798974573, 0.043545, 0, ls1/2};
//+
Point(16) = {0.213778974573, 0.043545, 0, ls1/2};
//+
Point(17) = {0.263698974573, 0.043545, 0, ls1/2};
//+
Point(18) = {1, 0.043545, 0, ls1};
//+
Point(19) = {0, 0.099755, 0, ls1};
//+
Point(20) = {0.17043, 0.099755, 0, ls1/4};
//+
Point(21) = {0.202798974573, 0.07709, 0, ls1/4};
//+
Point(22) = {0.213778974573, 0.06943, 0, ls1/4};
//+
Point(23) = {0.263698974573, 0.06943, 0, ls1/4};
//+
Point(24) = {0.263698974573, 0.07709, 0, ls1/4};
//+
Point(25) = {1, 0.07709, 0, ls1/4};
//+
Point(26) = {1, 0.07509, 0, ls1/4};
//+
Point(27) = {-0.4, 0, 0, ls1};
//+
Point(28) = {-0.4, 0.052, 0, ls1};
//+
Point(29) = {-0.4, 0.099755, 0, ls1};
//+
Point(30) = {-0.4, 0.1778, 0, ls1/4};
//+
Point(31) = {-0.2667, 0.1778, 0, ls1/4};
//+
Point(32) = {-0.054, 0.1778, 0, ls1/4};
//+
Point(33) = {0, 0.1778, 0, ls1/4};
//+
Point(34) = {0.17043, 0.1778, 0, ls1/4};
//+
Point(35) = {-0.2667, 0, 0, ls1/4};
//+
Point(36) = {-0.2667, 0.052, 0, ls1/4};
//+
Point(37) = {-0.054, 0.052, 0, ls1/4};
//+
Point(38) = {-0.2667, 0.099755, 0, ls1};
//+
Point(39) = {-0.054, 0.099755, 0, ls1};
//+
Point(40) = {-0.1778, 0, 0, ls1};
//+
Point(41) = {-0.0889, 0, 0, ls1};
//+
Point(42) = {-0.1778, 0.045, 0, ls1/4};
//+
Point(43) = {-0.0889, 0.045, 0, ls1/4};
//+
Point(44) = {-0.4, 0.1758, 0, ls1/4};
//+
Point(45) = {-0.2667, 0.1758, 0, ls1/4};
//+
Point(46) = {-0.054, 0.1758, 0, ls1/4};
//+
Point(47) = {0, 0.1758, 0, ls1/4};
//+
Point(48) = {0.17043, 0.1758, 0, ls1/4};
//+
Point(49) = {0.16843, 0.1758, 0, ls1/4};
//+
Point(50) = {0.16843, 0.099755, 0, ls1/4};
//+
Point(51) = {0.17043, 0.097755, 0, ls1/4};
//+
Point(52) = {0.202798974573, 0.07509, 0, ls1/4};
//+
Point(53) = {0.213778974573, 0.06743, 0, ls1/4};
//+
Point(54) = {0.263698974573, 0.06743, 0, ls1/4};
//+
Point(55) = {0.265698974573, 0.06743, 0, ls1/4};
//+
Point(56) = {0.265698974573, 0.07509, 0, ls1/4};
//+
Point(57) = {-0.2687, 0, 0, ls1/4};
//+
Point(58) = {-0.2687, 0.052, 0, ls1/4};
//+
Point(59) = {-0.2687, 0.054, 0, ls1/4};
//+
Point(60) = {-0.2667, 0.054, 0, ls1/4};
//+
Point(61) = {-0.054, 0.054, 0, ls1/4};
//+
Point(62) = {0, 0.045545, 0, ls1/4};
//+
Point(63) = {0.002, 0.043545, 0, ls1/4};
//+
Point(64) = {0.002, 0.038545, 0, ls1/4};
//+
Point(65) = {0.002, 0.036545, 0, ls1/4};
//+
Point(66) = {0, 0.036545, 0, ls1/4};
//+
Point(67) = {-0.0889, 0.043, 0, ls1/4};
//+
Point(68) = {-0.1778, 0.043, 0, ls1/4};
//+
Line(1) = {1, 66};
//+
Line(2) = {66, 7};
//+
Line(3) = {66, 65};
//+
Line(4) = {65, 64};
//+
Line(5) = {64, 7};
//+
Line(6) = {7, 13};
//+
Line(7) = {64, 63};
//+
Line(8) = {63, 13};
//+
Line(9) = {1, 2};
//+
Line(10) = {2, 8};
//+
Line(11) = {8, 64};
//+
Line(12) = {7, 43};
//+
Line(13) = {67, 66};
//+
Line(14) = {43, 67};
//+
Line(15) = {67, 41};
//+
Line(16) = {41, 1};
//+
Line(17) = {63, 62};
//+
Line(18) = {62, 61};
//+
Line(19) = {37, 13};
//+
Line(20) = {43, 42};
//+
Line(21) = {68, 67};
//+
Line(22) = {42, 68};
//+
Line(23) = {68, 40};
//+
Line(24) = {40, 41};
//+
Line(25) = {37, 36};
//+
Line(26) = {61, 60};
//+
Line(27) = {60, 36};
//+
Line(28) = {60, 59};
//+
Line(29) = {59, 58};
//+
Line(30) = {58, 36};
//+
Line(31) = {36, 35};
//+
Line(32) = {35, 57};
//+
Line(33) = {57, 58};
//+
Line(34) = {62, 19};
//+
Line(35) = {19, 50};
//+
Line(36) = {50, 20};
//+
Line(37) = {20, 51};
//+
Line(38) = {51, 14};
//+
Line(39) = {14, 8};
//+
Line(40) = {63, 14};
//+
Line(41) = {57, 27};
//+
Line(42) = {27, 28};
//+
Line(43) = {28, 58};
//+
Line(44) = {28, 29};
//+
Line(45) = {29, 38};
//+
Line(46) = {38, 60};
//+
Line(47) = {29, 44};
//+
Line(48) = {44, 30};
//+
Line(49) = {30, 31};
//+
Line(50) = {31, 45};
//+
Line(51) = {44, 45};
//+
Line(52) = {45, 38};
//+
Line(53) = {38, 39};
//+
Line(54) = {39, 19};
//+
Line(55) = {39, 61};
//+
Line(56) = {61, 37};
//+
Line(57) = {62, 13};
//+
Line(58) = {45, 46};
//+
Line(59) = {46, 39};
//+
Line(60) = {32, 46};
//+
Line(61) = {32, 31};
//+
Line(62) = {46, 47};
//+
Line(63) = {32, 33};
//+
Line(64) = {33, 47};
//+
Line(65) = {47, 19};
//+
Line(66) = {47, 49};
//+
Line(67) = {33, 34};
//+
Line(68) = {34, 48};
//+
Line(69) = {49, 48};
//+
Line(70) = {49, 50};
//+
Line(71) = {48, 20};
//+
Line(72) = {20, 21};
//+
Line(73) = {21, 22};
//+
Line(74) = {50, 51};
//+
Line(75) = {51, 52};
//+
Line(76) = {52, 53};
//+
Line(77) = {22, 23};
//+
Line(78) = {53, 54};
//+
Line(79) = {21, 52};
//+
Line(80) = {22, 53};
//+
Line(81) = {52, 15};
//+
Line(82) = {15, 14};
//+
Line(83) = {8, 9};
//+
Line(84) = {9, 15};
//+
Line(85) = {9, 3};
//+
Line(86) = {2, 3};
//+
Line(87) = {3, 4};
//+
Line(88) = {4, 10};
//+
Line(89) = {9, 10};
//+
Line(90) = {15, 16};
//+
Line(91) = {16, 53};
//+
Line(92) = {16, 10};
//+
Line(93) = {16, 17};
//+
Line(94) = {10, 11};
//+
Line(95) = {4, 5};
//+
Line(96) = {5, 11};
//+
Line(97) = {11, 17};
//+
Line(98) = {17, 54};
//+
Line(99) = {54, 23};
//+
Line(100) = {23, 24};
//+
Line(101) = {24, 25};
//+
Line(102) = {25, 26};
//+
Line(103) = {26, 56};
//+
Line(104) = {56, 23};
//+
Line(105) = {54, 55};
//+
Line(106) = {55, 56};
//+
Line(107) = {17, 18};
//+
Line(108) = {11, 12};
//+
Line(109) = {18, 12};
//+
Line(110) = {18, 26};
//+
Line(111) = {12, 6};
//+
Line(112) = {6, 5};
//+
Line Loop(113) = {42, 43, -33, 41};
//+
Plane Surface(114) = {113};
//+
Line Loop(115) = {44, 45, 46, 28, 29, -43};
//+
Plane Surface(116) = {115};
//+
Line Loop(117) = {47, 51, 52, -45};
//+
Plane Surface(118) = {117};
//+
Line Loop(119) = {48, 49, 50, -51};
//+
Plane Surface(120) = {119};
//+
Line Loop(121) = {33, 30, 31, 32};
//+
Plane Surface(122) = {121};
//+
Line Loop(123) = {28, 29, 30, -27};
//+
Plane Surface(124) = {123};
//+
Line Loop(125) = {26, 27, -25, -56};
//+
Plane Surface(126) = {125};
//+
Line Loop(127) = {18, 56, 19, -57};
//+
Plane Surface(128) = {127};
//+
Line Loop(129) = {46, -26, -55, -53};
//+
Plane Surface(130) = {129};
//+
Line Loop(131) = {52, 53, -59, -58};
//+
Plane Surface(132) = {131};
//+
Line Loop(133) = {61, 50, 58, -60};
//+
Plane Surface(134) = {133};
//+
Line Loop(135) = {60, 62, -64, -63};
//+
Plane Surface(136) = {135};
//+
Line Loop(137) = {59, 54, -65, -62};
//+
Plane Surface(138) = {137};
//+
Line Loop(139) = {55, -18, 34, -54};
//+
Plane Surface(140) = {139};
//+
Line Loop(141) = {23, 24, -15, -21};
//+
Plane Surface(142) = {141};
//+
Line Loop(143) = {20, 22, 21, -14};
//+
Plane Surface(144) = {143};
//+
Line Loop(145) = {12, 14, 13, 2};
//+
Plane Surface(146) = {145};
//+
Line Loop(147) = {15, 16, 1, -13};
//+
Plane Surface(148) = {147};
//+
Line Loop(149) = {3, 4, 5, -2};
//+
Plane Surface(150) = {149};
//+
Line Loop(151) = {8, -6, -5, 7};
//+
Plane Surface(152) = {151};
//+
Line Loop(153) = {17, 57, -8};
//+
Plane Surface(154) = {153};
//+
Line Loop(155) = {65, 35, -70, -66};
//+
Plane Surface(156) = {155};
//+
Line Loop(157) = {67, 68, -69, -66, -64};
//+
Plane Surface(158) = {157};
//+
Line Loop(159) = {70, 36, -71, -69};
//+
Plane Surface(160) = {159};
//+
Line Loop(161) = {35, 74, 38, -40, 17, 34};
//+
Plane Surface(162) = {161};
//+
Line Loop(163) = {11, 7, 40, 39};
//+
Plane Surface(164) = {163};
//+
Line Loop(165) = {1, 3, 4, -11, -10, -9};
//+
Plane Surface(166) = {165};
//+
Line Loop(167) = {-36, 74, -37};
//+
Plane Surface(168) = {167};
//+
Line Loop(169) = {37, 75, -79, -72};
//+
Plane Surface(170) = {169};
//+
Line Loop(171) = {76, -80, -73, 79};
//+
Plane Surface(172) = {171};
//+
Line Loop(173) = {81, 82, -38, 75};
//+
Plane Surface(174) = {173};
//+
Line Loop(175) = {82, 39, 83, 84};
//+
Plane Surface(176) = {175};
//+
Line Loop(177) = {10, 83, 85, -86};
//+
Plane Surface(178) = {177};
//+
Line Loop(179) = {76, -91, -90, -81};
//+
Plane Surface(180) = {179};
//+
Line Loop(181) = {84, 90, 92, -89};
//+
Plane Surface(182) = {181};
//+
Line Loop(183) = {85, 87, 88, -89};
//+
Plane Surface(184) = {183};
//+
Line Loop(185) = {77, -99, -78, -80};
//+
Plane Surface(186) = {185};
//+
Line Loop(187) = {93, 98, -78, -91};
//+
Plane Surface(188) = {187};
//+
Line Loop(189) = {93, -97, -94, -92};
//+
Plane Surface(190) = {189};
//+
Line Loop(191) = {88, 94, -96, -95};
//+
Plane Surface(192) = {191};
//+
Line Loop(193) = {104, -99, 105, 106};
//+
Plane Surface(194) = {193};
//+
Line Loop(195) = {101, 102, 103, 104, 100};
//+
Plane Surface(196) = {195};
//+
Line Loop(197) = {103, -106, -105, -98, 107, 110};
//+
Plane Surface(198) = {197};
//+
Line Loop(199) = {97, 107, 109, -108};
//+
Plane Surface(200) = {199};
//+
Line Loop(201) = {96, 108, 111, 112};
//+
Plane Surface(202) = {201};
//+
Point(69) = {0.265698974573, 0.07709, 0, ls1/4};
//+
Point(70) = {0.16843, 0.1778, 0, ls1/4};
//+
Delete {
  Surface{158};
}
//+
Delete {
  Surface{196};
}
//+
Delete {
  Line{67};
}
//+
Delete {
  Line{101};
}
//+
Line(203) = {33, 70};
//+
Line(204) = {70, 49};
//+
Line(205) = {34, 70};
//+
Line(206) = {24, 69};
//+
Line(207) = {69, 56};
//+
Line(208) = {69, 25};
//+
Line Loop(209) = {103, -207, 208, 102};
//+
Plane Surface(210) = {209};
//+
Line Loop(211) = {100, 206, 207, 104};
//+
Plane Surface(212) = {211};
//+
Line Loop(213) = {69, -68, 205, 204};
//+
Plane Surface(214) = {213};
//+
Line Loop(215) = {66, -204, -203, 64};
//+
Plane Surface(216) = {215};
//+
Transfinite Line {100, 104, 207, 206} = 10 Using Progression 1;
//+
Transfinite Surface {212};
//+
Recombine Surface {212};
//+
Transfinite Line {99, 104, 106, 105} = 10 Using Progression 1;
//+
Transfinite Surface {194};
//+
Recombine Surface {194};
//+
Transfinite Line {207} = 10 Using Progression 1;
//+
Transfinite Line {208, 103} = 1000 Using Progression 1;
//+
Transfinite Line {102} = 10 Using Progression 1;
//+
Transfinite Surface {210};
//+
Recombine Surface {210};
//+
Transfinite Line {37, 79} = 10 Using Progression 1;
//+
Transfinite Line {72, 75} = 60 Using Progression 1;
//+
Transfinite Surface {170};
//+
Recombine Surface {170};
//+
Transfinite Line {36, 69} = 10 Using Progression 1;
//+
Transfinite Line {70, 71} = 100 Using Progression 1;
//+
Transfinite Surface {160};
//+
Recombine Surface {160};
//+
Transfinite Line {36, 37} = 10 Using Progression 1;
//+
Transfinite Line {74} = 5 Using Progression 1;
//+
Transfinite Surface {168};
//+
Recombine Surface {168};
//+
Transfinite Line {79, 80} = 10 Using Progression 1;
//+
Transfinite Line {73, 76} = 20 Using Progression 1;
//+
Transfinite Surface {172};
//+
Recombine Surface {172};
//+
Transfinite Line {80, 99} = 10 Using Progression 1;
//+
Transfinite Line {77, 78} = 100 Using Progression 1;
//+
Transfinite Surface {186};
//+
Recombine Surface {186};
//+
Transfinite Line {204, 69, 68, 205} = 10 Using Progression 1;
//+
Transfinite Surface {214};
//+
Recombine Surface {214};
//+
Transfinite Line {204, 64} = 10 Using Progression 1;
//+
Transfinite Line {203, 66} = 200 Using Progression 1;
//+
Transfinite Surface {216};
//+
Recombine Surface {216};
//+
Transfinite Line {60, 64} = 10 Using Progression 1;
//+
Transfinite Line {63, 62} = 60 Using Progression 1;
//+
Transfinite Surface {136};
//+
Recombine Surface {136};
//+
Transfinite Line {50, 60} = 10 Using Progression 1;
//+
Transfinite Line {61, 58} = 200 Using Progression 1;
//+
Transfinite Surface {134};
//+
Recombine Surface {134};
//+
Transfinite Line {48, 50} = 10 Using Progression 1;
//+
Transfinite Line {49, 51} = 100 Using Progression 1;
//+
Transfinite Surface {120};
//+
Recombine Surface {120};
//+
Transfinite Line {32, 30} = 10 Using Progression 1;
//+
Transfinite Line {33, 31} = 60 Using Progression 1;
//+
Transfinite Surface {122};
//+
Recombine Surface {122};
//+
Transfinite Line {30, 28, 29, 27} = 10 Using Progression 1;
//+
Transfinite Line {27, 56} = 10 Using Progression 1;
//+
Transfinite Line {26, 25} = 200 Using Progression 1;
//+
Transfinite Surface {126};
//+
Recombine Surface {126};
//+
Transfinite Line {56, 57} = 10 Using Progression 1;
//+
Transfinite Line {18, 19} = 60 Using Progression 1;
//+
Transfinite Surface {128};
//+
Recombine Surface {128};
//+
Transfinite Line {8, 5, 7, 6} = 10 Using Progression 1;
//+
Transfinite Surface {152};
//+
Recombine Surface {152};
//+
Delete {
  Surface{162, 154};
}
//+
Delete {
  Line{17};
}
//+
Point(71) = {0.002, 0.045545, 0, ls1/4};
//+
Line(217) = {62, 71};
//+
Line(218) = {71, 63};
//+
Line Loop(219) = {57, -8, -218, -217};
//+
Plane Surface(220) = {219};
//+
Line Loop(221) = {34, 35, 74, 38, -40, -218, -217};
//+
Plane Surface(222) = {221};
//+
Transfinite Line {57, 217, 218, 8} = 10 Using Progression 1;
//+
Transfinite Surface {220};
//+
Recombine Surface {220};
//+
Transfinite Line {5, 4, 3, 2} = 10 Using Progression 1;
//+
Transfinite Surface {150};
//+
Recombine Surface {150};
//+
Transfinite Line {14, 2} = 10 Using Progression 1;
//+
Transfinite Line {13, 12} = 100 Using Progression 1;
//+
Transfinite Surface {146};
//+
Recombine Surface {146};
//+
Transfinite Line {22, 14} = 10 Using Progression 1;
//+
Transfinite Line {20, 21} = 100 Using Progression 1;
//+
Transfinite Surface {144};
//+
Recombine Surface {144};
//+
Transfinite Line {29, 30, 27, 28} = 10 Using Progression 1;
//+
Transfinite Surface {124};
//+
Recombine Surface {124};
//+
Point(72) = {0.265698974573, 0.06943, 0, ls1/4};
//+
Point(73) = {0.263698974573, 0.07509, 0, ls1/4};
//+
Delete {
  Surface{198};
}
//+
Delete {
  Surface{194};
}
//+
Delete {
  Surface{212};
}
//+
Delete {
  Line{106};
}
//+
Delete {
  Line{104};
}
//+
Delete {
  Line{100};
}
//+
Line(223) = {55, 72};
//+
Line(224) = {72, 23};
//+
Line(225) = {23, 73};
//+
Line(226) = {73, 24};
//+
Line(227) = {73, 56};
//+
Line(228) = {56, 72};
//+
Line Loop(229) = {105, 223, 224, -99};
//+
Plane Surface(230) = {229};
//+
Line Loop(231) = {225, 227, 228, 224};
//+
Plane Surface(232) = {231};
//+
Line Loop(233) = {226, 206, 207, -227};
//+
Plane Surface(234) = {233};
//+
Line Loop(235) = {105, 223, -228, -103, -110, -107, 98};
//+
Plane Surface(236) = {235};
//+
Transfinite Line {99, 105, 223, 224} = 10 Using Progression 1;
//+
Transfinite Surface {230};
//+
Recombine Surface {230};
//+
Transfinite Line {224, 227} = 10 Using Progression 1;
//+
Transfinite Line {225, 228} = 20 Using Progression 1;

//+
Transfinite Surface {232};
//+
Recombine Surface {232};
//+
Transfinite Line {226, 227, 207, 206} = 10 Using Progression 1;
//+
Transfinite Surface {234};
//+
Recombine Surface {234};
//+
Transfinite Line {-226, 207, 102} = 15 Using Progression 1.3;
//+
Transfinite Line {206, 227, -224, 105} = 15 Using Progression 1.3;
//+
Transfinite Line {-223, -99, 80, 79, 37} = 15 Using Progression 1.3;
//+
Transfinite Line {-36, -69, 205} = 15 Using Progression 1.3;
//+
Transfinite Line {68, 204, 64, 60, 50, -48} = 15 Using Progression 1.3;
//+
Transfinite Line {32, -30, 28} = 15 Using Progression 1.3;
//+
Transfinite Line {-29, -27, -56, -57, -218} = 15 Using Progression 1.3;
//+
Transfinite Line {217, -8, -5, 3} = 15 Using Progression 1.3;
//+
Transfinite Line {-4, -2, 14, 22} = 15 Using Progression 1.3;
//+
Physical Line("inlet") = {42, 44, 47, 48};
//+
Physical Line("walls") = {49, 61, 63, 203, 205, 68, 71, 72, 73, 77, 225, 226, 206, 208};
//+
Physical Line("axis") = {41, 32, 24, 16, 9, 86, 87, 95, 112};
//+
Physical Line("enginein") = {31};
//+
Physical Line("engineout") = {23, 22};
//+
Physical Line("engsurfin") = {20, 12, 6};
//+
Physical Line("engsurfout") = {25, 19};
//+
Physical Line("outlet") = {111, 109, 110, 102};
//+
Physical Surface("empty") = {202, 200, 236, 210, 230, 232, 234, 186, 188, 190, 192, 172, 180, 182, 184, 170, 174, 176, 178, 168, 222, 164, 166, 156, 216, 160, 214, 136, 138, 140, 128, 220, 152, 150, 146, 148, 142, 144, 130, 126, 132, 134, 120, 118, 116, 114, 124, 122};
//+
Delete {
  Surface{116, 124};
}
//+
Delete {
  Line{28};
}
//+
Delete {
  Line{29};
}
//+
Delete {
  Point{59};
}
//+
Circle(237) = {58, 36, 60};
//+
Line Loop(238) = {30, -27, -237};
//+
Plane Surface(239) = {238};
//+
Line Loop(240) = {44, 45, 46, -237, -43};
//+
Plane Surface(241) = {240};
//+
Transfinite Line {-27, -30} = 15 Using Progression 1.3;
//+
Transfinite Line {237} = 5 Using Progression 1;
//+
Transfinite Surface {239};
//+
Recombine Surface {239};
//+
Physical Surface("empty") += {241, 239};
//+
Delete {
  Surface{222, 220};
}
//+
Delete {
  Line{218, 217};
}
//+
Delete {
  Point{71};
}
//+
Delete {
  Surface{166, 150};
}
//+
Delete {
  Line{4, 3};
}
//+
Delete {
  Point{65};
}

//+
Circle(242) = {62, 13, 63};
//+
Circle(243) = {64, 7, 66};
//+
Line Loop(244) = {57, -8, -242};
//+
Plane Surface(245) = {244};
//+
Line Loop(246) = {2, -5, 243};
//+
Plane Surface(247) = {246};
//+
Line Loop(248) = {34, 35, 74, 38, -40, -242};
//+
Plane Surface(249) = {248};
//+
Line Loop(250) = {11, 243, -1, 9, 10};
//+
Plane Surface(251) = {250};
//+
Transfinite Line {-57, -8} = 15 Using Progression 1.3;
//+
Transfinite Line {242} = 5 Using Progression 1;
//+
Transfinite Surface {245};
//+
Recombine Surface {245};
//+
Transfinite Line {-5, -2} = 15 Using Progression 1.3;
//+
Transfinite Line {243} = 5 Using Progression 1;
//+
Transfinite Surface {247};
//+
Recombine Surface {247};
//+
Physical Surface("empty") += {245, 249, 247, 251};
//+
Delete {
  Surface{236, 230};
}
//+
Delete {
  Line{105, 223};
}
//+
Delete {
  Point{55};
}
//+
Circle(252) = {54, 23, 72};
//+
Line Loop(253) = {99, -224, -252};
//+
Plane Surface(254) = {253};
//+
Transfinite Line {-99, -224} = 15 Using Progression 1.3;
//+
Transfinite Line {252} = 6 Using Progression 1;
//+
Transfinite Surface {254};
//+
Recombine Surface {254};
//+
Line Loop(255) = {98, 252, -228, -103, -110, -107};
//+
Plane Surface(256) = {255};
