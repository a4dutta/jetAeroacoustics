import numpy as np
import matplotlib.pyplot as plt
import pdb
#from Tkinter import Tk, Frame, BOTH


class engine:
  #Zero defined as centerline of exhaust jet
  def __init__(self):
    self.nbpts = 8
    self.name = engine
    self.length     = 266.7
    self.radius_ext = 52
    self.radius_int = 45
    self.jet_ext = 43.545
    self.jet_int = 38.545
    self.conv_length = 54   #convergence length
    self.angleJet = np.arcsin((self.radius_ext-self.jet_ext)/self.conv_length)
    self.sideWall = self.radius_ext-self.radius_int
    self.conv_start_top = -self.conv_length
    self.conv_start_bot = -((self.radius_int-self.jet_int)/np.sin(self.angleJet))
    self.index=[6, 12, 34, 35, 36,39,41,42]
    self.index2=[49,55,77,78,79,82,84,85]
    self.xpos =[0,     0    , -self.length,   -self.length, self.conv_start_top, -self.length+self.sideWall,  -self.length+self.sideWall ,  self.conv_start_bot ]
    self.ypos =[self.jet_int, self.jet_ext , 0    , self.radius_ext  ,self.radius_ext,0.0,self.radius_int,self.radius_int ]


class augmenterInsert:
  #Zero defined at the augmenter lip
  def __init__(self):
    self.nbpts = 6
    self.radius_in  =  99.755
    self.radius_base=  77.09
    self.insert_heigth = 7.66
    self.insert_length = 60.9
    self.insert_angle = 35.0
    self.base_angle = 35.0
    ang_radian1 = np.radians(self.insert_angle)
    ang_radian2 = np.radians(self.base_angle)
    temp_top= (self.radius_in-self.radius_base)/np.sin(ang_radian2)
    temp_insert= (self.insert_heigth)/np.sin(ang_radian2)
    self.index =[19,20,21,22,23,24]
    self.index2=[62,63,64,65,66,67]
    self.xpos=[0.0,temp_top, temp_top+temp_insert,temp_top+self.insert_length,temp_top+self.insert_length,1000]
    self.ypos=[self.radius_in, self.radius_base,self.radius_base-self.insert_heigth,self.radius_base-self.insert_heigth,self.radius_base,self.radius_base]
  def position(self,xloc):
    self.xpos=+np.ones(self.nbpts)*xloc


class mesh:
   def __init__(self,engine,augmenter,e2a,rad_in=177.8,back=-400):
     self.header="FoamFile \n  {\n version 2.0;\n format  ascii;\n class dictionary;\n object blockMeshDict;\n }\n \n convertToMeters 0.001;\n"
     self.totalPoints = 86
     self.aug=augmenter
     self.eng=engine
     self.x = np.zeros(self.totalPoints)
     self.y = np.zeros(self.totalPoints)
     self.z = np.ones(self.totalPoints)
     augmenter.position(e2a)
     #behind Engine
     y_1 = max(engine.ypos)
     y_2 = max(augmenter.ypos)
     y_3 = rad_in
     x_1 = back
     x_2 = min(engine.xpos)
     x_3_top = engine.conv_start_top
     x_3_bot =engine.conv_start_bot
     x_4 = 0.0
     x_5 = e2a
     #ahead of engine
     y_a = engine.jet_ext
     y_b = engine.jet_int
     y_25 = augmenter.radius_base-augmenter.insert_heigth
     xrow=[augmenter.xpos[0],augmenter.xpos[1],augmenter.xpos[2],augmenter.xpos[3],augmenter.xpos[5]]
     yrow=[0]*5
     yrow.extend([y_b]*5)
     yrow.extend([y_a]*5)
     yrow.extend([y_25])
     #yrow=[[0]*5, [y_b]*5,[y_a]*5,y_25]
     yedge = [0.,y_1,y_2,y_3,y_3,y_3,y_3,y_3]
     yinternal= [y_2]*3
     xexternal = [ x_1,  x_1, x_1, x_1,x_2,x_3_top,x_4,e2a]
     xinternal = [x_2,x_3_top,x_4,x_3_bot]
     self.index=[0,1,2,3,4,5,7,8,9,10,11,13,14,15,16,17,25,26,27,28,29,30,31,32,33,37,38,18,40]
     self.index2=[43,44,45,46,47,48,50,51,52,53,53,54,56,57,58,59,60,68,69,70,71,72,73,74,75,76,80,81,83]
     temp_x=[0]
     temp_x.extend(xrow)
     temp_x.extend(xrow)
     temp_x.extend(xrow)
     temp_x.extend([xrow[-1]])
     temp_x.extend(xexternal)
     temp_x.extend(xinternal)
     temp_y=[0]
     temp_y.extend(yrow)
     temp_y.extend(yedge)
     temp_y.extend(yinternal)
     temp_y.extend([0])

     self.x[self.index]=temp_x
     self.x[self.index2]=temp_x
     self.x[self.aug.index]=self.aug.xpos
     self.x[self.aug.index2]=self.aug.xpos
     self.x[self.eng.index]=self.eng.xpos
     self.x[self.eng.index2]=self.eng.xpos

     self.y[self.index]=temp_y
     self.y[self.index2]=temp_y
     self.y[self.aug.index]=self.aug.ypos
     self.y[self.aug.index2]=self.aug.ypos
     self.y[self.eng.index]=self.eng.ypos
     self.y[self.eng.index2]=self.eng.ypos

     self.z[:self.totalPoints/2]=0.0
     self.z[self.totalPoints/2:]=-1.0
   def griding(self):
     self.nbBlocks=26
     self.grid_x_e2a= 50
     self.grid_y_engine= 10
     self.grid_y_lip = 2
     self.grid_y_insert=17
     self.grid_y_bfacestep=5
     self.grid_y_aheadAugmenter=25
     self.grid_x_insert1= 10
     self.grid_x_insert2= 3
     self.grid_x_insert3= 15
     self.grid_x_augmenter= 100
     self.grid_x_engineconv=70
     self.grid_x_engineflat=30
     self.grid_x_engineinside=30
     self.grid_x_aheadEngine=30
     self.edge = "edges\n  (\n  );"
     self.blocks=[None]*self.nbBlocks
     self.blocks[0]="hex (43 44 50 49 0 1 7 6)({0} {1} {2} )simpleGrading (1 0.5 1)".format(self.grid_x_e2a,self.grid_y_engine,1)
     self.blocks[1]="hex (44 45 51 50 1 2 8 7)({0} {1} {2})simpleGrading (1 0.5 1)".format(self.grid_x_insert1,self.grid_y_engine,1)
     self.blocks[2]="hex (45 46 52 51 2 3 9 8)({0} {1} {2})simpleGrading (1 0.5 1)".format(self.grid_x_insert2,self.grid_y_engine,1)
     self.blocks[3]="hex (46 47 53 52 3 4 10 9)({0} {1} {2})simpleGrading (1 0.5 1)".format(self.grid_x_insert3,self.grid_y_engine,1)
     self.blocks[4]="hex (47 48 54 53 4 5 11 10)({0} {1} {2})simpleGrading (3 0.5 1)".format(self.grid_x_augmenter,self.grid_y_engine,1)
     self.blocks[5]="hex (49 50 56 55 6 7 13 12)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_e2a,self.grid_y_lip,1)
     self.blocks[6]="hex (50 51 57 56 7 8 14 13)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert1,self.grid_y_lip,1)
     self.blocks[7]="hex (51 52 58 57 8 9 15 14)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert2,self.grid_y_lip,1)
     self.blocks[8]="hex (52 53 59 58 9 10 16 15)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert3,self.grid_y_lip,1)
     self.blocks[9]="hex (53 54 60 59 10 11 17 16)({0} {1} {2})simpleGrading (3 1 1)".format(self.grid_x_augmenter,self.grid_y_lip,1)
     self.blocks[10]="hex (55 56 62 61 12 13 19 18)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_e2a,self.grid_y_insert,1)
     self.blocks[11]="hex (56 57 63 62 13 14 20 19)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert1,self.grid_y_insert,1)
     self.blocks[12]="hex (57 58 64 63 14 15 21 20)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert2,self.grid_y_insert,1)
     self.blocks[13]="hex (58 59 65 64 15 16 22 21)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_insert3,self.grid_y_insert,1)
     self.blocks[14]="hex (59 60 68 65 16 17 25 22)({0} {1} {2})simpleGrading (3 1 1)".format(self.grid_x_augmenter,self.grid_y_insert,1)
     self.blocks[15]="hex (65 68 67 66 22 25 24 23)({0} {1} {2}1)simpleGrading (3 1 1)".format(self.grid_x_augmenter,self.grid_y_bfacestep,1)
     self.blocks[16]="hex (69 77 78 70 26 34 35 27)({0} {1} {2})simpleGrading (0.5 1 1)".format(self.grid_x_aheadEngine,self.grid_y_engine,1)
     self.blocks[17]="hex (70 78 80 71 27 35 37 28)({0} {1} {2})simpleGrading (0.5 1 1)".format(self.grid_x_aheadEngine,self.grid_y_insert,1)
     self.blocks[18]="hex (78 79 81 80 35 36 38 37)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_y_aheadAugmenter,self.grid_y_insert,1)
     self.blocks[19]="hex (79 55 61 81 36 12 18 38)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_engineconv,self.grid_y_insert,1)
     self.blocks[20]="hex (71 80 73 72 28 37 30 29)({0} {1} {2})simpleGrading (0.5 1 1)".format(self.grid_x_aheadEngine,self.grid_y_aheadAugmenter,1)
     self.blocks[21]="hex (80 81 74 73 37 38 31 30)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_y_aheadAugmenter,self.grid_y_aheadAugmenter,1)
     self.blocks[22]="hex (81 61 75 74 38 18 32 31)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_engineconv,self.grid_y_aheadAugmenter,1)
     self.blocks[23]="hex (61 62 76 75 18 19 33 32)({0} {1} {2})simpleGrading (1 1 1)".format(self.grid_x_e2a,self.grid_y_aheadAugmenter,1)
     self.blocks[24]="hex (82 83 85 84 39 40 42 41)({0} {1} {2})simpleGrading (1 0.5 1)".format(self.grid_x_engineflat,self.grid_y_engine,1)
     self.blocks[25]="hex (83 43 49 85 40 0 6 42)({0} {1} {2})simpleGrading (1 0.5 1)".format(self.grid_x_engineconv,self.grid_y_engine,1)

   def output(self,fname):
     f = open('template_blockMeshDict','r')

     filedata = f.read()
     f.close()
     #create vertices
     vertices=" "
     blocks=" "
     for ii in range(self.totalPoints):
       vertices+= "({0} {1} {2}) \n".format(self.x[ii],self.y[ii],self.z[ii])
     for ii in range(self.nbBlocks):
       blocks+=self.blocks[ii]+"\n"
     newdata = filedata.replace("[VERTICES]",vertices)
     newdata = newdata.replace("[BLOCKS]",blocks)
     newdata = newdata.replace("[EDGE]",self.edge)

     f = open(fname,'w')
     f.write(newdata)
     f.close()


fname = "blockMeshDict"
engine2Augmentor  = 170.43
mesh = mesh(engine(),augmenterInsert(),engine2Augmentor)
mesh.griding()
mesh.output(fname)
