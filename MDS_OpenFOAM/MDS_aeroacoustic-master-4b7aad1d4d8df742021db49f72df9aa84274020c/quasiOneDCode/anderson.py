
import numerics as num
import pdb


def getConservativeFromPrimitive(rho,U,p,A,gamma):
    rhoA = rho*A
    V = U
    T = p/rho
    return rhoA ,V,T

def getPrimitiveFromConservative(rhoA,V,T,A,gamma):
    rho = rhoA/A
    U = V
    p = rho*T
    return rho,U,p

def sos(rhoA,V,T,A,gamma):
    return T**0.5

def compute_rho(rho, U, A, numerics):
  rhs = - num.ddx_inside(rho*U*A,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return rhs


def compute_rhoU(rho, p, U, A, numerics ):
  T=p/rho
  dTdx =  num.ddx_inside(T,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  drhodx =  num.ddx_inside(rho,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  dmomdx = num.ddx_inside(p_rhoU2A,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return pdAdx - dmomdx


def compute_rhoE(rho, p, U, A, g_gm1, numerics ):
  term  = U*A*(p * g_gm1 +0.5*rho*U**2)
  rhs = - num.ddx_inside(term,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return rhs
