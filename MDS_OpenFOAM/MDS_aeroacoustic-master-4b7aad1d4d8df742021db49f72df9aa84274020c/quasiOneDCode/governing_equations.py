
import numerics as num
import pdb


def getConservativeFromPrimitive(rho,U,p,A,gamma):
    rhoA = rho*A
    rhoUA = U*rhoA
    rhoEA = (p/(gamma-1.0)+ 0.5*rho*U*U)*A
    return rhoA ,rhoUA,rhoEA

def getPrimitiveFromConservative(rhoA,rhoUA,rhoEA,A,gamma):
    rho = rhoA/A
    U = rhoUA/rhoA
    p = (rhoEA/A-0.5*rho*U**2)*(gamma-1.0)
    return rho,U,p

def sos(rhoA,rhoUA,rhoEA,A,gamma):
    rho = rhoA/A
    U = rhoUA/rhoA
    p = (rhoEA/A-0.5*rho*U**2)*(gamma-1.0)
    return (p/rho)**0.5

def compute_rho(rho, U, A, numerics,pert=False):
  rhs = - num.ddx_inside(rho*U*A,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return rhs


def compute_rhoU(rho, p, U, A, numerics ):
  dUdx = num.ddx_inside(U,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  d2Udx2 = num.ddx_inside(dUdx,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])

  pdAdx = p* num.ddx_inside(A,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  p_rhoU2A = (p + rho*U**2)*A
  dmomdx = num.ddx_inside(p_rhoU2A,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return pdAdx - dmomdx #+ 0.1*d2Udx2


def compute_rhoE(rho, p, U, A, g_gm1, numerics ):
  term  = U*A*(p * g_gm1 +0.5*rho*U**2)
  rhs = - num.ddx_inside(term,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return rhs
