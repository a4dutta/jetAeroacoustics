# These equations are taken from Duran and Moreau 2014self.

import numerics as num
import pdb


def getConservativeFromPrimitive(rho,U,p,A,gamma):
    rhoA = rho*A
    rhoUA = U*rhoA
    rhoEA = (p/(gamma-1.0)+ 0.5*rho*U*U)*A
    return rhoA ,rhoUA,rhoEA

def getPrimitiveFromConservative(rhoA,rhoUA,rhoEA,A,gamma):
    rho = rhoA/A
    U = rhoUA/rhoA
    p = (rhoEA/A-0.5*rho*U**2)*(gamma-1.0)
    return rho,U,p

def sos(rhoA,rhoUA,rhoEA,A,gamma):
    rho = rhoA/A
    U = rhoUA/rhoA
    p = (rhoEA/A-0.5*rho*U**2)*(gamma-1.0)
    return (p/rho)**0.5

def compute_rho(p, U, p_p, U_p, numerics,gamma=1.4):
  rhs_1 = - U*num.ddx_inside(p_p/(gamma*p),numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  rhs_2 = - U*num.ddx_inside(U_p/(U),numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return (gamma*p)*(rhs_1+rhs_2)


def compute_rhoU(p, U, p_p, U_p,s_p, SoS, numerics,gamma=1.4, cp=0.7 ):
  rhs_1=-U*num.ddx_inside(U_p/(U),numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  rhs_2= -SoS**2/U*num.ddx_inside(p_p/(gamma*p),numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])

  dudx= num.ddx_inside(U,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  coef=-(2*U_p/(U)-(gamma-1.0)*p_p/(gamma*p)-s_p/cp)
  return U*(rhs_1+rhs_2+coef*dudx)


def compute_rhoE(U, s_p, numerics,cp=0.7 ):
  rhs = - U*num.ddx_inside(s_p,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
  return rhs
