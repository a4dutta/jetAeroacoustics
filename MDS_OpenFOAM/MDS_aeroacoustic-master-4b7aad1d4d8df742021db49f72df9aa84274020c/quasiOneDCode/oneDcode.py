import numpy as np
import matplotlib.pyplot as plt
import temporal_schemes
import numerics as num
import pdb


###############SETUP PROBLEM################
test       = True
testType   = 'pulse'  #'anderson', 'pulse'
if testType=="anderson":
    import anderson as gov
elif testType=="perturbation"
    import perturbation_equations as gov
else:
    import governing_equations as gov

filterOn   = False
sponge     = False
filterOrder  = 2
filter_alpha =10.2
N_dom      =100                       # number of points in domain
N_bc       =100                      # number of points on the homogeneous boundaries
N          = 2*N_bc + N_dom           # total number of points
s          = N_dom
e          = N_dom+N_dom-1
L          = 2.0                       # length of nozzle
x          = np.linspace(-L,2*L,N)     # discretized distance
tend       = 5.0                      # total time
tsteps     = 20000                      # number of timesteps
A_tot      =  np.ones(N)               #-0.2*np.sin(x/L*np.pi)#(1+2.2*(x**2-3*x+2.25)) # area of the nozzle
num_scheme = 'DPR4'       # numerical scheme used 'upwind1''FDs9p','DPR4'
bypass     = 2.2                # bypass ratio
gamma      = 1.4                   # specific heat ratio
time       = np.linspace(0,tend,tsteps)
R          = 287.058                 # universal gas constant air
t          = 0.0
#if testType=="perturbation":


####### computed quantities
dt     = time[1:]-time[:-1]
dx     = np.ones(N)*(x[2]-x[1]) # dx of each cell
sId    = range(N)
g_gm1  = gamma/(gamma-1.0)
storage = np.zeros(tsteps)
slen   = L/2.

########## boundary conditions ##########
###Jet
p_tot = 160000
T_tot  = 500
M_jet  = 0.8
A_jet  = A_tot[0]/2.0  #initial guess on area

#derived values
T_jet = T_tot/(1+(gamma-1.)/2.*M_jet**2 )
c_jet = (gamma * R*T_jet)
U_jet = M_jet*c_jet
p_jet = p_tot*(T_jet/T_tot)**(g_gm1)
rho_jet = p_jet/(R*T_jet)
mdot_jet = rho_jet*U_jet * A_jet

#entain
T_ent    = 490     # static temperature of entrained air
rho_ent  = p_jet/(R*T_ent)
p_ent    = p_jet
c_ent    = (gamma * R*T_ent)
mdot_ent = bypass*mdot_jet
A_ent    = A_tot[0]-A_jet
U_ent    = mdot_ent/(rho_ent*A_ent)

##########setup matrix ################
streams = 1
rho    = np.ones([streams,N])
U      = np.ones([streams,N])
p      = np.ones([streams,N])
T      = np.ones([streams,N])
SoS    = np.ones([streams,N])
A      = np.ones([streams,N])
rhoA   = np.ones([streams,N])
rhoU   = np.ones([streams,N])
rhoE   = np.ones([streams,N])
BCs    = np.ones([streams,4,2])


##########Initialization ################
if streams>1:
  rho[0] *= rho_jet
  U[0]   *= U_jet
  p[0]   *= p_jet
  A[0]   *= A_tot
  rho[1] *= rho_ent
  U[1]   *= U_ent
  p[1]   *= p_ent
elif streams == 1 and test==False:
  rho[0] *= rho_jet
  U[0]   *= U_jet
  p[0]   *= p_jet
  A[0]   *= A_tot
elif streams == 1 and test==True and testType=="anderson":
  rho[0][s:e] = 1 - 0.3146*x[s:e]
  T[0][s:e]   = 1-0.2314*x[s:e]
  U[0][s:e]   = (0.1+1.09*x[s:e])*T[s:e]**0.5
  p[0][s:e]   = rho[0]*T[0]
  A[0][s:e]   = A_tot[s:e]
  #Apply the values to the boundaries:
  rho[0] = num.setBounds(rho[0],rho[0][s],rho[0][e-1],s,e)
  T[0]   = num.setBounds(T[0],T[0][s],T[0][e-1],s,e)
  U[0]   = num.setBounds(U[0],U[0][s],U[0][e-1],s,e)
  p[0]   = num.setBounds(p[0],p[0][s],p[0][e-1],s,e)
  A[0]   = num.setBounds(A[0],A[0][s],A[0][e-1],s,e)
elif streams == 1 and test==True and testType=="pulse":
  Na    = int(0.51*N)
  Nb    = int(0.56*N)
  U      = np.zeros([streams,N])
  p[0,Na:Nb] += 0.001*np.sin(np.pi*(x[Na:Nb]-x[Na])/(x[Nb]-x[Na]))
  rho[0,Na:Nb]+= 0.001*np.sin(np.pi*(x[Na:Nb]-x[Na])/(x[Nb]-x[Na]))
  A[0]   = np.ones([streams,N])
elif testType=="perturbation":
  # compute the mean flow quantities
  U_p      = np.ones([streams,N])
  p_p      = np.ones([streams,N])
  s_p      = np.ones([streams,N])



pdb.set_trace()
for jj in range(streams):
  rhoA[jj],rhoU[jj],rhoE[jj]= gov.getConservativeFromPrimitive(rho[jj],U[jj],p[jj],A[jj],gamma)

BCs[0][0]=[rho[0][s],rho[0][e]]
BCs[0][1]=[U[0][s],U[0][e]]
BCs[0][2]=[T[0][s],T[0][e]]
BCs[0][3]=[p[0][s],p[0][e]]

##########Setup the numerics################
numerics = num.getNumerics(num_scheme,sId,dx,filterOn,filter_alpha,filterOrder)

##########Plot initial flow
#if test:
#  plt.plot(x,U[0]/max(U[0]))
#  plt.plot(x,rho[0],'r')


####################################
# Test derivative of knonw function
####################################
#testVal=np.sin(x*np.pi)
#ans=num.ddx_inside(testVal,numerics['dx'],numerics['index'],numerics['coeff'],numeric#s['index_bc'],numerics['coeff_bc'])
#plt.plot(x,ans)
#plt.plot(x,np.cos(x*np.pi)*np.pi,'r')
#plt.show()


####################################
# Set up the RK4 scheme
####################################
time_advancement    = temporal_schemes.runge_kutta("RK2")
rk_coeff            = time_advancement.rk_coeff
rk_substep_fraction = time_advancement.rk_substep_fraction
scheme_type         = time_advancement.scheme
Nk                  = len(rk_substep_fraction)

it=0
simulation_done=False

ii=0
# time advancement loop
#pdb.set_trace()
while not(simulation_done):
  simulation_done = it >= tsteps

  rhodot_rkrhs  = [0]*len(rk_substep_fraction)
  rhoUdot_rkrhs = [0]*len(rk_substep_fraction)
  rhoEdot_rkrhs = [0]*len(rk_substep_fraction)

  rhoA_n = rhoA.copy()
  rhoU_n = rhoU.copy()
  rhoE_n = rhoE.copy()

  for rk_step in range(0,Nk):
    rk_dt = rk_substep_fraction[rk_step] * dt[ii]
    if numerics['filterOn']:
      rhoA[ii]   = num.filter(rhoA[ii],sId,numerics)
      rhoU[ii]   = num.filter(rhoU[ii],sId,numerics)
      rhoE[ii]   = num.filter(rhoE[ii],sId,numerics)
    if sponge ==True:
      rhoA_in  = BCs[ii][0][0]*A[ii][0]
      rhoA_out = BCs[ii][0][1]*A[ii][-1]
      rhoUA_in  = rhoA_in *BCs[ii][1][0]
      rhoUA_out = rhoA_in *BCs[ii][1][1]
      rhoE_in =   (BCs[ii][3][0]/(gamma-1.0)+0.5*  BCs[ii][0][0]*BCs[ii][1][0]**2)*A[ii][0]
      rhoE_out =  (BCs[ii][3][0]/(gamma-1.0)+0.5*  BCs[ii][0][1]*BCs[ii][1][1]**2)*A[ii][-1]
      rhoA[ii] = num.sponge(rhoA[ii],slen,N_bc/2,rhoA_in,rhoA_out)
      rhoU[ii]   = num.sponge(rhoU[ii],slen,N_bc/2,rhoUA_in,rhoUA_out)
      rhoE[ii]   = num.sponge(rhoE[ii],slen,N_bc/2,rhoE_in,rhoE_out)

    rho[ii], U[ii], p[ii] = gov.getPrimitiveFromConservative(rhoA[ii],rhoU[ii],rhoE[ii],A[ii],gamma)

    if numerics['filterOn']:
      rho[ii]   = num.filter(rho[ii],sId,numerics)
      U[ii]   = num.filter(U[ii],sId,numerics)
      p[ii]   = num.filter(p[ii],sId,numerics)
    #Apply boundary
    rho[ii], U[ii], p[ii] =num.bc(rho[ii],U[ii],p[ii],R, 0.01, time[it])
#(rho,U,p,R,A,t,freq=10)

    ''' Storing k_i'''
    # Compute RHS of equations
    rhodot_rkrhs[rk_step]       = gov.compute_rho(rho[ii], U[ii], A[ii], numerics)
    rhoUdot_rkrhs[rk_step]      = gov.compute_rhoU(rho[ii], p[ii], U[ii], A[ii], numerics )
    rhoEdot_rkrhs[rk_step]      = gov.compute_rhoE(rho[ii], p[ii], U[ii], A[ii], g_gm1, numerics )

    rhoA[ii]  = rhoA_n[ii]
    rhoU[ii] = rhoU_n[ii]
    rhoE[ii] = rhoE_n[ii]

  # for RK4, for example, rk_step = [0,1,2,3]
  # you will never access: rk_coeff[0]
  # Advancing RK step
  if rk_step < (rk_substep_fraction.size - 1):
    for rk_coeff_index in range(0,rk_step + 1):
      rhoA[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*rhodot_rkrhs[rk_coeff_index]
      rhoU[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*rhoUdot_rkrhs[rk_coeff_index]
      rhoE[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*rhoEdot_rkrhs[rk_coeff_index]

    # Update timestep with fractional RK
  t += dt*rk_substep_fraction[rk_step]
  SoS[ii] = gov.sos(rhoA[ii],rhoU[ii],rhoE[ii],A[ii],gamma)

  print "Done: ",it
  storage[it]=rho[0,N/2]
  if test and it%2000==0:
    plt.plot(x,p[ii])
    plt.plot(x,rho[ii],'r')
    #plt.plot(x,U[ii],'g')
    #plt.plot(x,U[0]/SoS[0],'r')
  #if it%50==0:
    plt.show()
    pdb.set_trace()

    #plt.figure()
    #plt.plot(time,storage)
    #pdb.set_trace()
  ## End of Runge-Kutta substeps
  ################################################################################
  for rk_step in range(0, Nk):
    rhoA[ii]  += dt[it]*rk_substep_fraction[rk_step]*rhodot_rkrhs[rk_step]
    rhoU[ii]  += dt[it]*rk_substep_fraction[rk_step]*rhoUdot_rkrhs[rk_step]
    rhoE[ii]  += dt[it]*rk_substep_fraction[rk_step]*rhoEdot_rkrhs[rk_step]
  # Completed RK cycle
  it += 1
