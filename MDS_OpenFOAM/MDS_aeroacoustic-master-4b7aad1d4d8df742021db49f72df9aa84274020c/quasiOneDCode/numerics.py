import numpy as np
import pdb

def setBounds(phi,phi_s,phi_e,s,e):  #sets the boundaries to the correct values
  BC = np.ones(s)
  phi[0,s]= BC*phi_s
  phi[e:] = BC*phi_e
  return phi



def inside_bounds(stencil,N):
  return (stencil-1)/(2),N-(stencil-1)/(2)

def boundary_bounds_left(stencil,N):
  return 1,(stencil-1)/(2)

def boundary_bounds_right(stencil,N):
  return N-(stencil-1)/(2),N-1

def ddx_inside(phi,dx,index,coeff,index_bc,coeff_bc):
    stencil=len(coeff)
    N=len(phi)
    a, b  = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    ddx=np.zeros(len(phi))
    for ii,cst in enumerate(coeff):
      ddx[a:b]   +=  cst*phi[index[ii]]
      ddx[al:bl] +=  coeff_bc[0][ii]*phi[index_bc[0][ii]]  # left
      ddx[ar:br] +=  coeff_bc[1][ii]*phi[index_bc[1][ii]]  # left
    # apply boundaries (naively)
    #ddx[0] = ddx[1]
    #ddx[-1] = ddx[-2]
    return ddx/dx

def getNumerics(select_scheme,sId,dx,filterOn=False,falpha = 0.5,filterOrder=2):
  N=np.size(sId)
  if select_scheme == "upwind1":
    # Central scheme 4th order  (http://www4.ncsu.edu/~cwsimmon/theory/4thOrderFDSchemes.pdf)
    stencil = 3                 # must be odd number
    coeff_bc = [[],[]]
    index_bc = [[],[]]
    a,b   = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    coeff = [0, -1.  ,1. ]
    index = [sId[a:b], sId[a:b],sId[a+1:b+1]]
    coeff_bc[0] = [0, -1., 1.]
    index_bc[0] = [sId[al:bl],sId[al:bl],sId[al+1:bl+1]]
    coeff_bc[1] = [0,-1., 1.]
    index_bc[1] = [sId[ar:br],sId[ar:br],sId[ar+1:br+1]]
    filterOn=False
  elif select_scheme == "central4":
    # Central scheme 4th order  (http://www4.ncsu.edu/~cwsimmon/theory/4thOrderFDSchemes.pdf)
    stencil = 5                 # must be odd number
    coeff_bc = [[],[]]
    index_bc = [[],[]]
    a,b   = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    coeff = [1./12.0     ,-2./3.      ,0       ,2.0/3.0     ,-1/12.0]
    index = [sId[a-2:b-2],sId[a-1:b-1],sId[a:b],sId[a+1:b+1],sId[a+2:b+2] ]
    coeff_bc[0] = [-3/12., -10/12., 18/12., -6/12., 1/12.]
    index_bc[0] = [sId[al-1:bl-1],sId[al:bl],sId[al+1:bl+1],sId[al+2:bl+2],sId[al+3:bl+3]]
    coeff_bc[1] = [-1/12., 6./12., -18./12., 10/12., 3./12.]
    index_bc[1] = [sId[ar-3:br-3],sId[ar-2:br-2],sId[ar-1:br-1],sId[ar:br],sId[ar+1:br+1]]
  elif select_scheme == "DPR4":
    # DPR 4th order  (Tam & Webb 1993)
    stencil = 7                 # must be odd number
    coeff_bc = [[],[]]
    index_bc = [[],[]]
    a,b   = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    coeff = [-0.02651995, 0.18941314     , -0.79926643      ,0       ,0.79926643     ,-0.18941314 ,0.02651995]
    index = [sId[a-3:b-3],sId[a-2:b-2],sId[a-1:b-1],sId[a:b],sId[a+1:b+1],sId[a+2:b+2],sId[a+3:b+3]]
    coeff_bc[0] = [0.0, -3/12., -10/12., 18/12., -6/12., 1/12.,0.0]
    index_bc[0] = [sId[al:bl],sId[al-1:bl-1],sId[al:bl],sId[al+1:bl+1],sId[al+2:bl+2],sId[al+3:bl+3],sId[al:bl]]
    coeff_bc[1] = [0.0,-1/12., 6./12., -18./12., 10/12., 3./12.,0.0]
    index_bc[1] = [sId[ar:br],sId[ar-3:br-3],sId[ar-2:br-2],sId[ar-1:br-1],sId[ar:br],sId[ar+1:br+1],sId[ar:br]]

  elif select_scheme == "FDs9p":
    # DPR 4th order  (Bogey & Bailley 2004)
    stencil = 9                 # must be odd number
    coeff_bc = [[],[]]
    index_bc = [[],[]]
    a,b   = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    coeff = [1./280.,-4./105., 1./5.     , -4.0/5.0     ,0       ,4.0/5.0      ,-1./5. ,4./105.,-1./280.]
    index = [sId[a-4:b-4],sId[a-3:b-3],sId[a-2:b-2],sId[a-1:b-1],sId[a:b],sId[a+1:b+1],sId[a+2:b+2],sId[a+3:b+3],sId[a+4:b+4]]
    coeff_bc[0] = [0.0,0.0, -3/12., -10/12., 18/12., -6/12., 1/12.,0.0,0.0]
    index_bc[0] = [sId[al:bl],sId[al:bl],sId[al-1:bl-1],sId[al:bl],sId[al+1:bl+1],sId[al+2:bl+2],sId[al+3:bl+3],sId[al:bl],sId[al:bl]]
    coeff_bc[1] = [0.0,0.0,-1/12., 6./12., -18./12., 10/12., 3./12.,0.0,0.0]
    index_bc[1] =  [sId[ar:br],sId[ar:br],sId[ar-3:br-3],sId[ar-2:br-2],sId[ar-1:br-1],sId[ar:br],sId[ar+1:br+1],sId[ar:br],sId[ar:br]]
  if filterOn:
    if filterOrder==8:
      fstencil = 9                 # must be odd number
      fcoeff_bc = [[],[]]
      findex_bc = [[],[]]
      a,b   = inside_bounds(fstencil,N)
      al,bl = boundary_bounds_left(fstencil,N)
      ar,br = boundary_bounds_right(fstencil,N)
      #fcoeff = [1.0/256.0,-1.0/32.0, 7.0/64.0 , -7.0/32.0 ,35.0/128.,-7.0/32.0,7.0/64.0 ,-1.0/32.0,1.0/256.0]
      fcoeff = (1)*[0.008228661760,-0.045211119360,0.120007591680 , -0.204788880640 ,0.243527493120,-0.204788880640,0.120007591680,-0.045211119360,0.008228661760]
      findex = [sId[a-4:b-4],sId[a-3:b-3],sId[a-2:b-2],sId[a-1:b-1],sId[a:b],sId[a+1:b+1],sId[a+2:b+2],sId[a+3:b+3],sId[a+4:b+4]]
    #[0.0,0.0,0.0,0,0,0,0.0,0.0,0.0]#
      fcoeff_bc[0] = [0.0,0.0,0.0,  1.,-2., 1.,0.0,0.0,0.0]
      findex_bc[0] = [sId[al:bl],sId[al:bl],sId[al:bl],sId[al-1:bl-1],sId[al:bl],sId[al+1:bl+1],sId[al:bl],sId[al:bl],sId[al:bl]]
      fcoeff_bc[1] = [0.0,0.0,0.0, 1.,-2., 1.,0.0,0.0,0.0]
      findex_bc[1] =  [sId[ar:br],sId[ar:br],sId[ar:br],sId[ar-1:br-1],sId[ar:br],sId[ar+1:br+1],sId[ar:br],sId[ar:br],sId[ar:br]]
    if filterOrder==2:
      fstencil = 3                # must be odd number
      fcoeff_bc = [[],[]]
      findex_bc = [[],[]]
      a,b   = inside_bounds(fstencil,N)
      al,bl = boundary_bounds_left(fstencil,N)
      ar,br = boundary_bounds_right(fstencil,N)
      fcoeff = [-1.,2., -1.]
      findex = [sId[a-1:b-1],sId[a:b],sId[a+1:b+1]]
      fcoeff_bc[0] = [ -1.,2., -1.]
      findex_bc[0] = [sId[al-1:bl-1],sId[al:bl],sId[al+1:bl+1]]
      fcoeff_bc[1] = [ -1.,2., -1.]
      findex_bc[1] =  [sId[ar-1:br-1],sId[ar:br],sId[ar+1:br+1]]
  else:
    fstencil=0
    fcoeff_bc = [[],[]]
    findex_bc = [[],[]]
    fcoeff = []
    findex = []
    fcoeff_bc = []
    findex_bc=[]

 #Create dictionnary
  numerics = {'stencil': stencil, 'coeff': coeff,  'index': index,'coeff_bc': coeff_bc,  'index_bc': index_bc, 'dx':dx, 'filterOn':filterOn, 'fstencil': fstencil, 'falpha':falpha, 'fcoeff': fcoeff,  'findex': findex,'fcoeff_bc': fcoeff_bc,  'findex_bc': findex_bc}

  return numerics


def bc(rho,U,p,R,A,t,freq=10):
  test=True
  p[0]    = A*np.sin(freq*t)
  U[0]    = 1.0
  rho[0]  = 2*rho[1]-rho[2]
  p[-1]   = 1.0
  U[-1]   = 2*U[-2]-U[-3]
  rho[-1] = 2*rho[-2]-rho[-3]

  if test:
    rho[0]= 1
    T  = 1
    p[0]  = 1
    U[0]  = 2*U[1]-U[2]
    rho[-1] = 2*rho[-2]-rho[-3]
    U[-1] = 2*U[-2]-U[-3]
    p[-1] = 2*p[-2]-p[-3]
  return rho,U,p

def filter(phi,sId,num):
    stencil=num['fstencil']
    N=np.size(sId)
    a,b   = inside_bounds(stencil,N)
    al,bl = boundary_bounds_left(stencil,N)
    ar,br = boundary_bounds_right(stencil,N)
    phi_n = phi.copy()
    D=np.zeros(np.size(phi))
    findex=num['findex']
    fcoeff=num['fcoeff']
    findex_bc=num['findex_bc']
    fcoeff_bc=num['fcoeff_bc']
    for ii,cst in enumerate(fcoeff):
      D[a:b]     +=  cst*phi[findex[ii]]
      D[al:bl]   +=  fcoeff_bc[0][ii]*phi[findex_bc[0][ii]]  # left
      D[ar:br]   +=  fcoeff_bc[1][ii]*phi[findex_bc[1][ii]]  # left
    phi_n[a:b]   -= num['falpha']*D[a:b]
    phi_n[al:bl] += num['falpha']*D[al:bl]
    phi_n[ar:br] += num['falpha']*D[ar:br]
    return phi_n

def sponge(phi,slen,N,phi_inlet,phi_outlet):  #lightly force solution
    #pdb.set_trace()
    x       = np.linspace(0,slen,N)
    ones    = np.ones(N)
    sinlet  = (x-slen)**3/(-slen)**3
    soutlet = (x)**3/(slen)**3
    #pdb.set_trace()
    phi[0:N]  -= (phi[0:N]-phi_inlet*ones)*sinlet
    phi[-N:]  -= (phi[-N:] -phi_outlet*ones)*soutlet
    return phi


def nscbc(rho, U, p,R , test=False):
    c1=-3./2.
    c2=2.0
    c3=-0.5
    '''drhodx=[[][]]
    dpdx=[[][]]
    dudx=[[][]]
    c=[[][]]
    L1 = np.zeros([2])
    L2 = np.zeros([2])
    L3 = np.zeros([2])
    L4 = np.zeros([2])
    L5 = np.zeros([2])
    L1[0] = U[0]*(drhodx[0]-dpdx[0]/c[0]**2)
    L2[0] = 0.0
    L3[0] = 0.0
    L4[0] = (U[0]+c[0])*(dudx[0]+dpdx[0]/(rho[0]*c[0])
    L5[0] = (U[0]-c[0])*(-dudx[0]+dpdx[0]/(rho[0]*c[0])
    L1[1] = U[-1]*(drhodx[1]-dpdx[1]/c[1]**2)
    L2[1] = 0.0
    L3[1] = 0.0
    L4[1] = (U[-1]+c[1])*(dudx[1]+dpdx[1]/(rho[-1]*c[1])
    L5[1] = (U[0]-c[0])*(-dudx[0]+dpdx[0]/(rho[0]*c[0])
    d1 =  L1[0]+rho[0]*0.5/c[0]*(L4[0]+L5[0])
    d2 =  0.5*(L4[0]-L5[0])
    d3 =  0.5*rho[0]*c[0]*(L4[0]+L5[0])

    if test:
      rho[0]= 1
      T  = 1
      p[0]  = 1
      U[0]  = 2*U[1]-U[2]
      rho[-1] = 2*rho[-2]-rho[-3]
      U[-1] = 2*U[-2]-U[-3]
      p[-1] = 2*p[-2]-p[-3]'''
    return rho,U,p


def applyPressBC(t, Amp=0.1, redFreq=3.0):
  return Amp*np.sin(t/redFreq)
def applyVelBC(t, Amp=0.1, redFreq=3.0):
  return -Amp*np.sin(t/redFreq)
