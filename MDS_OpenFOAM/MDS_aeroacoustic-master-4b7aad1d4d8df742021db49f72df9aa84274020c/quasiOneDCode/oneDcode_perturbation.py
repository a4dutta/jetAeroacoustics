import numpy as np
import matplotlib.pyplot as plt
import temporal_schemes
import numerics as num
import pdb


###############SETUP PROBLEM################
test       = True
testType   = 'pulse'  #'anderson', 'pulse'

import perturbation_equations as gov

filterOn     = False
sponge       = False
filterOrder  = 2
filter_alpha =10.2
N_dom        =100                       # number of points in domain
N_bc         = 2                      # number of points on the homogeneous boundaries
N            = 2*N_bc + N_dom           # total number of points
s            = N_dom
e            = N_dom+N_dom-1
L            = 2.0                       # length of nozzle
x          = np.linspace(-L,2*L,N)     # discretized distance
tend       = 1.0                      # total time
tsteps     = 20000                      # number of timesteps
A_tot      =  np.ones(N)               #-0.2*np.sin(x/L*np.pi)#(1+2.2*(x**2-3*x+2.25)) # area of the nozzle
num_scheme = 'DPR4'                # numerical scheme used 'upwind1''FDs9p','DPR4'
bypass     = 2.2                   # bypass ratio
gamma      = 1.4                   # specific heat ratio
time       = np.linspace(0,tend,tsteps)
R          = 287.058                 # universal gas constant air
t          = 0.0
#if testType=="perturbation":


####### computed quantities
dt     = time[1:]-time[:-1]
dx     = np.ones(N)*(x[2]-x[1]) # dx of each cell
sId    = range(N)
g_gm1  = gamma/(gamma-1.0)
storage = np.zeros(tsteps)
slen   = L/2.

########## boundary conditions ##########
###Jet


##########setup matrix ################
streams = 1
rho    = np.ones([streams,N])
U      = np.ones([streams,N])
p      = np.ones([streams,N])
T      = np.ones([streams,N])
SoS    = np.ones([streams,N])
A      = np.ones([streams,N])
rhoA   = np.ones([streams,N])
rhoU   = np.ones([streams,N])
rhoE   = np.ones([streams,N])
BCs    = np.ones([streams,4,2])


##########Initialization ################
U_p      = np.zeros([streams,N])
p_p      = np.zeros([streams,N])
s_p      = np.zeros([streams,N])



##########Setup the numerics################
numerics = num.getNumerics(num_scheme,sId,dx,filterOn,filter_alpha,filterOrder)

##########Plot initial flow
#if test:
#  plt.plot(x,U[0]/max(U[0]))
#  plt.plot(x,rho[0],'r')


####################################
# Test derivative of knonw function
####################################
#testVal=np.sin(x*np.pi)
#ans=num.ddx_inside(testVal,numerics['dx'],numerics['index'],numerics['coeff'],numerics['index_bc'],numerics['coeff_bc'])
#plt.plot(x,ans,'b.')
#plt.plot(x,np.cos(x*np.pi)*np.pi,'r')
#plt.show()


####################################
# Set up the RK4 scheme
####################################
time_advancement    = temporal_schemes.runge_kutta("RK2")
rk_coeff            = time_advancement.rk_coeff
rk_substep_fraction = time_advancement.rk_substep_fraction
scheme_type         = time_advancement.scheme
Nk                  = len(rk_substep_fraction)

it=0
simulation_done=False

# designed to compute stream
ii=0
# time advancement loop
#pdb.set_trace()
while not(simulation_done):
  simulation_done = it >= tsteps

  pdot_rkrhs  = [0]*len(rk_substep_fraction)
  Udot_rkrhs = [0]*len(rk_substep_fraction)
  sdot_rkrhs = [0]*len(rk_substep_fraction)

  p_p_n = p_p.copy()
  U_p_n = U_p.copy()
  s_p_n = s_p.copy()
  for rk_step in range(0,Nk):
    rk_dt = rk_substep_fraction[rk_step] * dt[ii]


    ''' Storing k_i'''
    # Compute RHS of equations
    pdot_rkrhs[rk_step]       = gov.compute_rho(p[ii], U[ii], p_p[ii], U_p[ii], numerics)
    Udot_rkrhs[rk_step]      = gov.compute_rhoU( p[ii], U[ii], p_p[ii], U_p[ii], s_p[ii],SoS[ii], numerics )
    sdot_rkrhs[rk_step]      = gov.compute_rhoE(U[ii], s_p[ii], numerics )

    p_p[ii] = p_p_n[ii]
    U_p[ii] = U_p_n[ii]
    s_p[ii] = s_p_n[ii]

  # for RK4, for example, rk_step = [0,1,2,3]
  # you will never access: rk_coeff[0]
  # Advancing RK step
  if rk_step < (rk_substep_fraction.size - 1):
    for rk_coeff_index in range(0,rk_step + 1):
      p_p[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*pdot_rkrhs[rk_coeff_index]
      U_p[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*Udot_rkrhs[rk_coeff_index]
      s_p[ii] += dt*rk_coeff[rk_step+1][rk_coeff_index]*sdot_rkrhs[rk_coeff_index]
    #Apply BC on substeps
    p_p[ii][0] = num.applyPressBC(time[it]+dt*rk_substep_fraction[rk_step])
    U_p[ii][0] = num.applyVelBC(time[it]+dt*rk_substep_fraction[rk_step])

    # Update timestep with fractional RK
  t += dt*rk_substep_fraction[rk_step]

  print "Done: ",it
  storage[it]=p_p[0,N/2]
  if test and it%2000==0:
    plt.title("TEST")
    plt.plot(x,p_p[ii])
    #plt.plot(x,U[ii],'g')
    #plt.plot(x,U[0]/SoS[0],'r')
  #if it%50==0:
    plt.show()
    pdb.set_trace()

    #plt.figure()
    #plt.plot(time,storage)
    #pdb.set_trace()
  ## End of Runge-Kutta substeps
  ################################################################################

  for rk_step in range(0, Nk):
    p_p[ii]  += dt[it]*rk_substep_fraction[rk_step]*pdot_rkrhs[rk_step]
    U_p[ii]  += dt[it]*rk_substep_fraction[rk_step]*Udot_rkrhs[rk_step]
    s_p[ii]  += dt[it]*rk_substep_fraction[rk_step]*sdot_rkrhs[rk_step]
  p_p[ii][0]=num.applyPressBC(time[it])
  U_p[ii][0]=num.applyVelBC(time[it])
  #U_p[ii][0]=0.1*np.sin(time[it]/3.)
  # Completed RK cycle
  it += 1
