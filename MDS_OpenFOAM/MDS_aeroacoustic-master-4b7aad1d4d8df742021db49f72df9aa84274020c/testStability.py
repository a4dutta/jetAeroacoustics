import numpy as np
import pdb
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

def computeMfromP(p,pt,g):
  return (2.0*((p/pt)**(-(g-1)/g)-1)/(g-1))**0.5

#thermodynamics
Rgas=287.0
g=1.4
Tjet=300.0
Tbypass=300.0
ggm1=g/(g-1.0)
c=(g*Rgas*Tjet)**0.5


# initial case
pt1     = 168000.0
pt2     = 101300.0
Mjet    = 0.9
BPR     = 2.18

setVal="pratio"
#setVal="pratio"
rtot = 0.178
mdot1=0.1
At = np.pi*rtot**2
if setVal=="area":
  #Total area at MDS 0.178
  # Area jet  0.03
  rjet=0.15
  A1=np.pi*rjet**2
  A2=At-A1
elif setVal=="pratio":
  p2_p1=0.5


 # Set the initial jet Mach number
U1   = Mjet*c
p1   = pt1 *(1+(g-1)*0.5*Mjet**2)**(-ggm1)
rho1 = p1/(Rgas*Tjet)
T1   = Tjet
A1=mdot1/(rho1*U1)
A2=At-A1
mdot2=BPR*mdot1
if setVal=="area":
  func = lambda p2 : p2  - pt2*(1+(g-1)*0.5* (mdot2**2*Rgas*Tbypass)/(g*p2**2*A2**2) )**(-ggm1)
  p2=fsolve(func, p1*0.6)[0]
  M2 = computeMfromP(p2,pt2,g)
  p2_p1=p2/p1
elif setVal=="pratio":
  p2=p1*p2_p1
  M2 = computeMfromP(p2,pt2,g)
rho2=p2/(Rgas*Tbypass)
T2=p2/(Rgas*rho2)
U2=mdot2/(A2*rho2)


def computeMjet(dp_perc,dA):
  # impose dp=1%
  dp=pt1*dp_perc
  if p2_p1<1.0:
    dA=abs(dA)*p2_p1
  else:
    dA=-abs(dA)*p2_p1
  du2=mdot2*T2*Rgas/((p2+dp)*(A2-dA))-U2
  du1=mdot1*T1*Rgas/((p1+dp)*(A1+dA))-U1
  #pdb.set_trace()
  #return (Tjet/Tbypass)**0.5  *(p2+dp)/(p1+dp)*(A2+dA)/(A1-dA)*Mbypass/BPR
  print p2/p1,(du2+U2)/(g*Rgas*T2)**0.5,du2,((p2+dp)*(A2-dA)*(U2+du2)/T2*Rgas)/((p1+dp)*(A1+dA)*(U1+du1)/T1*Rgas),dp/p2*100.
  return (U1-du1)/(g*Rgas*T1)**0.5,(U2-du2)/(g*Rgas*T2)**0.5


N=20
dp_perc_list = np.linspace(-0.1,0.1,N)
dA_list = A1*np.linspace(-0.1,0.1,N)
Mnew=np.zeros(N)
Mby=np.zeros(N)
for dA in dA_list:
  for ii,dp_perc in enumerate(dp_perc_list):
    Mnew[ii],Mby[ii]=computeMjet(dp_perc,dA)
  plt.plot(dp_perc_list,Mnew,'k')
  plt.plot(dp_perc_list,Mby,'r')
plt.ylim(0.0,1.05)
plt.show()
