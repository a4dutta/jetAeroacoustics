import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

def find_nearestIndex(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def interpolate(x1, x2, x3, y1, y2, y3):
    dy1 = y2 - y1
    dy2 = y3 - y1
    dx  = x3 - x1
    x2 = dx*dy1/dy2 + x1
    return x2

fig = plt.figure(figsize=[16,10])
ax = fig.add_subplot(111)

count = 0
# Need to find more efficient way to do this....
for fname in ('U_baseline0000.curve', 'U_baseline0001.curve', 'U_baseline0002.curve',
	      'U_baseline0003.curve', 'U_baseline0004.curve', 'U_baseline0005.curve', 	            'U_baseline0006.curve', 'U_baseline0007.curve', 'U_baseline0008.curve', 		      'U_baseline0009.curve', 'U_baseline0010.curve'):
    data=np.loadtxt(fname)
    data=data[:len(data)/2]
    X=data[:,0] *100
    Y=data[:,1]
    yHalf = max(Y)/2
    index = find_nearestIndex(Y, yHalf)
    nearestX = X[index]
    interpX = 0
    interpX = interpolate(X[index-1], interpX, X[index], Y[index-1], yHalf, Y[index])
    print nearestX
    plt.plot(X,Y, label='x = ' + str(count) + ' cm')
    count += 17
    

ax.set_ylabel('U Value', fontsize=15)
ax.set_xlabel("y, cm of Extracted Value", fontsize=15)
plt.ylim(0)
plt.xlim(0,X[-1])
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show() #or
#plt.savefig('figure.png')
