import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
import pdb

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

def find_nearestIndex(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def interpolate(x1, x2, x3, y1, y2, y3):
    dy1 = y2 - y1
    dy2 = y3 - y1
    dx  = x3 - x1
    x2 = dx*dy1/dy2 + x1
    return x2

fig = plt.figure(figsize=[14,10])
ax = fig.add_subplot(111)

halfX = []
halfY = []
interpX = 0

count = 0
# Need to find more efficient way to do this....
for fname in ('U_baseline0000.curve', 'U_baseline0001.curve', 'U_baseline0002.curve',
	      'U_baseline0003.curve', 'U_baseline0004.curve', 'U_baseline0005.curve', 'U_baseline0006.curve',
              'U_baseline0007.curve', 'U_baseline0008.curve', 'U_baseline0009.curve','U_baseline0010.curve'):
    data=np.loadtxt(fname)
    data=data[:len(data)/2]
    # X=data[:,0] *100

    X=data[:,0]
    Y=data[:,1]

    # Use this line for the half maximum from the centre of the curve
    yHalf = Y[-1]/2
    # Use this line for actual half maximum
    # yHalf = max(Y)/2


    # index = find_nearestIndex(Y, yHalf)
    # nearestX = X[index]
    # interpX = interpolate(X[index-1], interpX, X[index], Y[index-1], yHalf, Y[index])
    # print nearestX
    xtemp=np.interp(yHalf,Y,X)
    if count==0:
      xHalf_0=X[-1]-xtemp #0.77084  # diameter of jet

    xHalf=(X[-1]-xtemp)/xHalf_0

    #pdb.set_trace()
    halfX.append(count)
    halfY.append(xHalf)
    count += 1
for ii,hX in enumerate(halfX):
  halfX[ii]=hX/0.77084
#pdb.set_trace()
X=np.linspace(0,20)
Y=0.098*X*0.048
ax.scatter(halfX, halfY)
ax.plot(halfX, halfY, linestyle='--')
ax.set_ylabel('U_half/D', fontsize=15)
ax.set_xlabel("x/D", fontsize=15)
plt.xlim(0,halfX[-1])
#plt.plot(X,Y)
box = ax.get_position()
print halfX,halfY
print (halfY[-1]-halfY[-2])/(halfX[-1]-halfX[-2])
# ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#plt.show() #or
plt.savefig('figure.png')
