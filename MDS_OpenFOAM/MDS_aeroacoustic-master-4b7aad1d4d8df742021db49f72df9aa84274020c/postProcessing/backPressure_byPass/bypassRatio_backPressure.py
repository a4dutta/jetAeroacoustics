import matplotlib.pyplot as plt
import numpy as np

n=7
me=np.zeros(n)
mj=np.zeros(n)
bypass=np.zeros(n)
p=[75000,80000,85000,90000,95000,101300,105000]
# Extract mass flow at x=-1
me_1=[2.59494, 2.54584, 2.58569,1.97756,1.80675,1.3533,0.814096]
# at x=-0.5
me_2=[2.76083, 2.70956,2.74707,2.14256,1.96567,1.45765,0.910735]
#x=-0.25
me_3=[2.73587,2.68609,2.72781,2.14103,1.96467,1.45278,0.918867]


#x=0.12
mj_1=[3.54022,3.54022, 3.49041,2.97395,2.79338,2.26648, 1.74841]
#x=0.25
mj_2=[3.53459, 3.48786,3.51824,2.97176,2.79189,2.26429,1.74964]

#Flow jet at M=0.9
n9=3
bypass_M9=np.zeros(n9)
p_M9=[75000,85000,95000]
me_M9=[2.72349,2.25742,2.00137]
mj_M9=[3.59244,3.09405,2.88952]

for ii in range(n):
    me[ii]=(me_2[ii])#+me_2[ii]+me_3[ii])/3.
    mj[ii]=(mj_1[ii]+mj_2[ii])/2.
    bypass[ii]=1.0/(mj[ii]/me[ii]-1.0)
for ii in range(n9):
    bypass_M9[ii]=1.0/(mj_M9[ii]/me_M9[ii]-1.0)

plt.plot(p,bypass,label="M_jet=0.8")
plt.scatter(p_M9,bypass_M9,label="M_jet=0.9")
plt.scatter(101300,1.8194,label="M_fine",color='red')
plt.scatter(100000,1.8358,color='red')
plt.scatter(97000,2.15,color='red')
plt.xlabel('back pressure [pa]')
plt.plot([75000,105000],[2.0,2],"k-.")
plt.plot([75000,105000],[2.2,2.2],"k-.")
plt.ylabel('bypass ratio')
plt.legend()
plt.savefig("bypassRatio.png")
plt.show()
