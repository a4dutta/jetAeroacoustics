
# if any line contains this symbol, then the line will be removed, and all the remaining contents will be 
# moved to another file. Simply just change the name of the files you're reading and writing to.
bad_words = ['#']

with open('pressure_incline0000.curve') as oldfile, open('pressure_incline.curve', 'w') as newfile:
    for line in oldfile:
        if not any(bad_word in line for bad_word in bad_words):
            newfile.write(line)
