import numpy as np
import matplotlib.pyplot as plt
import Tkinter

machMeshResults = open("UnsteadyM9_V2.curve")
machRealResults = open('expV2.curve')

# file1 = raw_input("Input name of file containing simulated Mach values: ")
# file2 = raw_input("Input name of file containing experimental Mach values: ")

# machMeshResults = open(file1)
# machRealResults = open(file2)

# Will contain the data for the mesh Mach values
meshX = []
meshY = []

# Will contain the data for the real Mach values
realX = []
realY = []

# Used to read in the Mach values from .curve file
a = 0
b = 0
xaxis = []
count = 0

# Reads in the values for X and Y axis
with machMeshResults as o:
    next(o)
    for line in o:
        a, b = line.split()
        a = float(a)*100 # reads in distance in meters. Converts to cm.
        meshX.append(a)
        meshY.append(b)

with machRealResults as o:
    for line in o:
        a, b = line.split()
        realX.append(a)
        realY.append(b)

# Plots the grid
fig = plt.figure(figsize=[12,10])
ax = fig.add_subplot(111)

# plt.setp(meshDataLine, linewidth=2, color='r')
# plt.xticks(xaxis, xPlot, rotation='horizontal')

# ax2 = ax.twinx()
# ax2 = ax.twiny()

meshDataLine, = ax.plot(meshX, meshY, 'r')
rawDataLine, = ax.plot(realX, realY,'b')
# plt.setp(rawDataLine, linewidth=2, color='b')

# Sets the axis titles
ax.set_ylabel('Mach Value', fontsize=15)
ax.set_xlabel("y, cm of Extracted Value", fontsize=15)

plt.legend((meshDataLine, rawDataLine), ('Simulated', 'Experimental'))


plt.show()
