import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate


from scipy.fftpack import fft
import math as math
import pdb

fig = plt.figure(figsize=[14,10])
ax = fig.add_subplot(111)

count = 0
startTime = 0
endTime = 1.15

listOfFiles = []
listOfLabels = []

# fileInfo contains information about which results to read from and what their corresponding 
# labels should be in the plot
fileInfo = open("fileInformation")

with fileInfo as o:
    for line in o:
        a, b = line.split(', ')
        listOfFiles.append(a)
        listOfLabels.append(b)
        count = count + 1
        print(listOfLabels)

t_array = []
x_array = []

count = 0
# Reads in file names and labels from a text document 
for fname in listOfFiles:
    t1,x1=np.loadtxt(fname,unpack=True)
    # Selects time range to analyze
    t1 = t1[t1 <= endTime]
    x1 = x1[t1 <= endTime]
    N=len(t1)
    mean_x1=np.mean(x1)
    dtmax=t1[-1]-t1[0]
    dt_min = t1[44]-t1[43]
    yf = fft(x1-mean_x1)
    # pdb.set_trace()
    xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
    # Convert to dB after applying FFT
    SPL = 20*np.log10(2.0/N * np.abs(yf/(2*10**-5)))
    # maxVal=max(2.0/N * np.abs(yf[0:N/2]))
    ax.plot(xf, np.abs(SPL[0:N/2]),label=str(listOfLabels[count]),lw=2)
    #plt.xscale('log')
    count += 1

plt.xlim(2/dtmax,5000)
plt.xlabel('Frequency, Hz')
plt.ylabel('Amplitude, Db')
plt.xlim(0,xf[-1])
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()
plt.savefig("pressureSpectra.png")

# ax1 = plt.subplot(111)
# plt.plot(t_array, x_array,lw=2)
# plt.setp(ax1.get_xticklabels(), visible=False)
# # share x only
# plt.xlabel('time')
#
# plt.savefig("pressureSignal.png")
# plt.show()
