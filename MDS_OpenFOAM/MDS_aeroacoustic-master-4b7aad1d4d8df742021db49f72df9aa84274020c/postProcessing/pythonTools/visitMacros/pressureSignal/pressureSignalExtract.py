QueryOverTimeAtts = GetQueryOverTimeAttributes()
QueryOverTimeAtts.timeType = QueryOverTimeAtts.DTime  # Cycle, DTime, Timestep
QueryOverTimeAtts.startTimeFlag = 0
QueryOverTimeAtts.startTime = 0
QueryOverTimeAtts.endTimeFlag = 0
QueryOverTimeAtts.endTime = 1
QueryOverTimeAtts.strideFlag = 0
QueryOverTimeAtts.stride = 1
QueryOverTimeAtts.createWindow = 1
QueryOverTimeAtts.windowId = 2
SetQueryOverTimeAttributes(QueryOverTimeAtts)
QueryOverTimeAtts = GetQueryOverTimeAttributes()
QueryOverTimeAtts.timeType = QueryOverTimeAtts.DTime  # Cycle, DTime, Timestep
QueryOverTimeAtts.startTimeFlag = 0
QueryOverTimeAtts.startTime = 0
QueryOverTimeAtts.endTimeFlag = 0
QueryOverTimeAtts.endTime = 1
QueryOverTimeAtts.strideFlag = 0
QueryOverTimeAtts.stride = 1
QueryOverTimeAtts.createWindow = 1
QueryOverTimeAtts.windowId = 2
SetQueryOverTimeAttributes(QueryOverTimeAtts)

iterator = 1
while (iterator < 16):
  OpenDatabase("localhost:/media/jean-pierre/b8bdcc07-3ca1-43c3-8e9c-de60f5b460ed/PROJECT_DATA/MDS_aeroacoustic_data/MDS_Data_Hickey/unsteady_caseB/flow_" + str(iterator) + "000/flow_*.plt database", 0)
  AddPlot("Pseudocolor", "Pressure", 1, 1)
  DrawPlots()
  # MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
  SaveSession("/media/jean-pierre/b8bdcc07-3ca1-43c3-8e9c-de60f5b460ed/PROJECT_DATA/.visit/crash_recovery.session")
  # MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
  SetWindowMode("zoom")
  # Begin spontaneous state
  View3DAtts = View3DAttributes()
  View3DAtts.viewNormal = (0, 0, 1)
  View3DAtts.focus = (1, 0, -0.005)
  View3DAtts.viewUp = (0, 1, 0)
  View3DAtts.viewAngle = 30
  View3DAtts.parallelScale = 5.38517
  View3DAtts.nearPlane = -10.7703
  View3DAtts.farPlane = 10.7703
  View3DAtts.imagePan = (0.0791063, -0.000651042)
  View3DAtts.imageZoom = 11.1304
  View3DAtts.perspective = 1
  View3DAtts.eyeAngle = 2
  View3DAtts.centerOfRotationSet = 0
  View3DAtts.centerOfRotation = (1, 0, -0.005)
  View3DAtts.axis3DScaleFlag = 0
  View3DAtts.axis3DScales = (1, 1, 1)
  View3DAtts.shear = (0, 0, 1)
  View3DAtts.windowValid = 1
  SetView3D(View3DAtts)
  # End spontaneous state

  SetWindowMode("node pick")
  PickAtts = GetPickAttributes()
  PickAtts.variables = ("default")
  PickAtts.showIncidentElements = 1
  PickAtts.showNodeId = 1
  PickAtts.showNodeDomainLogicalCoords = 0
  PickAtts.showNodeBlockLogicalCoords = 0
  PickAtts.showNodePhysicalCoords = 0
  PickAtts.showZoneId = 1
  PickAtts.showZoneDomainLogicalCoords = 0
  PickAtts.showZoneBlockLogicalCoords = 0
  PickAtts.doTimeCurve = 1
  PickAtts.conciseOutput = 0
  PickAtts.showTimeStep = 1
  PickAtts.showMeshName = 1
  PickAtts.blockPieceName = "zone"
  PickAtts.groupPieceName = "group"
  PickAtts.showGlobalIds = 0
  PickAtts.showPickLetter = 1
  PickAtts.reusePickLetter = 0
  PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
  PickAtts.createSpreadsheet = 0
  PickAtts.floatFormat = "%g"
  PickAtts.timePreserveCoord = 1
  PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
  SetPickAttributes(PickAtts)
  PickAtts = GetPickAttributes()
  PickAtts.variables = ("default", "Pressure")
  PickAtts.showIncidentElements = 1
  PickAtts.showNodeId = 1
  PickAtts.showNodeDomainLogicalCoords = 0
  PickAtts.showNodeBlockLogicalCoords = 0
  PickAtts.showNodePhysicalCoords = 0
  PickAtts.showZoneId = 1
  PickAtts.showZoneDomainLogicalCoords = 0
  PickAtts.showZoneBlockLogicalCoords = 0
  PickAtts.doTimeCurve = 1
  PickAtts.conciseOutput = 0
  PickAtts.showTimeStep = 1
  PickAtts.showMeshName = 1
  PickAtts.blockPieceName = "zone"
  PickAtts.groupPieceName = "group"
  PickAtts.showGlobalIds = 0
  PickAtts.showPickLetter = 1
  PickAtts.reusePickLetter = 0
  PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
  PickAtts.createSpreadsheet = 0
  PickAtts.floatFormat = "%g"
  PickAtts.timePreserveCoord = 1
  PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
  SetPickAttributes(PickAtts)
  PickAtts = GetPickAttributes()
  PickAtts.variables = ("default", "Pressure")
  PickAtts.showIncidentElements = 1
  PickAtts.showNodeId = 1
  PickAtts.showNodeDomainLogicalCoords = 0
  PickAtts.showNodeBlockLogicalCoords = 0
  PickAtts.showNodePhysicalCoords = 0
  PickAtts.showZoneId = 1
  PickAtts.showZoneDomainLogicalCoords = 0
  PickAtts.showZoneBlockLogicalCoords = 0
  PickAtts.doTimeCurve = 1
  PickAtts.conciseOutput = 0
  PickAtts.showTimeStep = 1
  PickAtts.showMeshName = 1
  PickAtts.blockPieceName = "zone"
  PickAtts.groupPieceName = "group"
  PickAtts.showGlobalIds = 0
  PickAtts.showPickLetter = 0
  PickAtts.reusePickLetter = 1
  PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
  PickAtts.createSpreadsheet = 0
  PickAtts.floatFormat = "%g"
  PickAtts.timePreserveCoord = 1
  PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
  SetPickAttributes(PickAtts)
  NodePick(coord=(.08052, 0, 0), vars=("default", "Pressure"))
  PickAtts = GetPickAttributes()
  PickAtts.variables = ("default", "Pressure")
  PickAtts.showIncidentElements = 1
  PickAtts.showNodeId = 1
  PickAtts.showNodeDomainLogicalCoords = 0
  PickAtts.showNodeBlockLogicalCoords = 0
  PickAtts.showNodePhysicalCoords = 0
  PickAtts.showZoneId = 1
  PickAtts.showZoneDomainLogicalCoords = 0
  PickAtts.showZoneBlockLogicalCoords = 0
  PickAtts.doTimeCurve = 1
  PickAtts.conciseOutput = 0
  PickAtts.showTimeStep = 1
  PickAtts.showMeshName = 1
  PickAtts.blockPieceName = "zone"
  PickAtts.groupPieceName = "group"
  PickAtts.showGlobalIds = 0
  PickAtts.showPickLetter = 1
  PickAtts.reusePickLetter = 0
  PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
  PickAtts.createSpreadsheet = 0
  PickAtts.floatFormat = "%g"
  PickAtts.timePreserveCoord = 1
  PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
  SetPickAttributes(PickAtts)
  SetActiveWindow(1)
  DeleteActivePlots()
  iterator = iterator + 1

