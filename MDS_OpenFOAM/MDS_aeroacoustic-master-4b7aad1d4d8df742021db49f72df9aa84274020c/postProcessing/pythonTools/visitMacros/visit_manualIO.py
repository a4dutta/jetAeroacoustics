dataBasePath = raw_input("Enter path to database here: ")
# exportPath = raw_input("Enter lineout destination location: ")
exportPath = "/home/tfaruk/"
fileName = "test"
ans = raw_input("Do you want to analyze preexisting variable (1) or a new variable (2)? ")
ans = int(ans)
while ans != int(1) and ans != int(2):
    ans = raw_input("Please enter a valid answer... ")

if (str(ans) == "2") :
    variableName = raw_input("Enter name of variable you want to analyze: ")
    definition = raw_input("Enter the definition of the variable - i.e Conservative_2/Conservative_1: ")
    DefineScalarExpression(str(variableName), str(definition))
else:
    variableName = raw_input("Enter name of variable you want to analyze: ")

# path = "localhost:/home/tfaruk/unsteady/fullDomain_2D_coarse_unsteady_M04/flow.dat"
# " + str(dataBasePath)
# original path was "localhost:/home/tamim/visit/bin/home/flow.dat"
OpenDatabase(dataBasePath, 0, "Tecplot")

AddPlot("Pseudocolor", variableName, 1, 1)
DrawPlots()
SetWindowMode("zoom")
# Begin spontaneous state
View2DAtts = View2DAttributes()
View2DAtts.windowCoords = (-0.354391, 0.302589, -0.236709, 0.307595)
View2DAtts.viewportCoords = (0.2, 0.95, 0.15, 0.95)
View2DAtts.fullFrameActivationMode = View2DAtts.Auto  # On, Off, Auto
View2DAtts.fullFrameAutoThreshold = 100
View2DAtts.xScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.yScale = View2DAtts.LINEAR  # LINEAR, LOG
View2DAtts.windowValid = 1
SetView2D(View2DAtts)
# End spontaneous state

# MAINTENANCE ISSUE: SetSuppressMessagesRPC is not handled in Logging.C. Please contact a VisIt developer.
SetQueryFloatFormat("%g")
Query("Lineout", end_point=(-0.17, 0.18, 0), num_samples=200, start_point=(-0.17, 0.05, 0), use_sampling=1)

SetActiveWindow(2)
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 0
SaveWindowAtts.outputDirectory = exportPath
SaveWindowAtts.fileName = fileName
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.CURVE  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1024
SaveWindowAtts.height = 1024
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 80
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.ScreenProportions  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 0
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()
