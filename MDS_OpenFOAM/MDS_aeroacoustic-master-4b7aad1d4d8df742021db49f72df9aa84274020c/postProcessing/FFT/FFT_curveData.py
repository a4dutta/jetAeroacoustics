import matplotlib.pyplot as plt
from scipy.fftpack import fft
import numpy as np

t1,x1=np.loadtxt("step_U.curve",unpack=True)

t2,x2=np.loadtxt("firstSep_U.curve",unpack=True)


ax1 = plt.subplot(211)
plt.plot(t1, x1,lw=2)
plt.setp(ax1.get_xticklabels(), visible=False)
# share x only
ax2 = plt.subplot(212, sharex=ax1)
plt.plot(t2, x2,lw=2)
# make these tick labels invisible
plt.setp(ax2.get_xticklabels())
plt.savefig("velocitySignal.png")

#########################################
# Separation region
#########################################
mean_x1=np.mean(x1-1)
dtmax=t1[-1]-t1[0]
dt_min = t1[44]-t1[43]
plt.figure()
N=len(t1)
yf = fft(x1-mean_x1*np.ones(N))
<<<<<<< Updated upstream
=======

>>>>>>> Stashed changes
xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]),label="Step",lw=2)
plt.xscale('log')
plt.xlim(2/dtmax,200)

mean_x2=np.mean(x2-1)
dtmax=t2[-1]-t2[0]
dt_min = t2[44]-t2[43]
yf = fft(x2-mean_x2*np.ones(N))
xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]),label="Insert",lw=2)
plt.xscale('log')
plt.xlabel('frequency, Hz')
plt.ylabel('amplitude')
plt.legend()
plt.savefig("bubble.png")
plt.show()



#########################################
# Centerline pressure
#########################################
<<<<<<< Updated upstream
t1,x1=np.loadtxt("centerline_halfWay_pressure.curve",unpack=True)
t2,x2=np.loadtxt("centerline_insert_pressure.curve",unpack=True)
t3,x3=np.loadtxt("centerline_augmenter_pressure.curve",unpack=True)
N=len(t1)
=======
timeStart=1.2
t1,x1=np.loadtxt("../unsteady_caseB/pressureProfiles_new/pressure_lip.curve",unpack=True)
t2,x2=np.loadtxt("../unsteady_caseB/pressureProfiles_new/pressure_tubeIncline.curve",unpack=True)
t3,x3=np.loadtxt("../unsteady_caseB/pressureProfiles_new/pressure_halfway.curve",unpack=True)
>>>>>>> Stashed changes
mean_x1=np.mean(x1-1)
dtmax=t1[-1]-t1[0]
dt_min = t1[44]-t1[43]
plt.figure()
<<<<<<< Updated upstream
yf = fft(x1-mean_x1*np.ones(N))
=======
mask=t1>timeStart
N=len(t1[mask])
yf = fft(x1[mask]-mean_x1*np.ones(N))
>>>>>>> Stashed changes
xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
maxVal=max(2.0/N * np.abs(yf[0:N/2]))
plt.plot(xf, 2.0/N * np.abs(yf[0:N/2])/maxVal,label="lip",lw=2)
#plt.xscale('log')

mask=t2>timeStart
mean_x2=np.mean(x2[mask]-1)
dtmax=t2[-1]-t2[0]
dt_min = t2[44]-t2[43]
yf = fft(x2[mask]-mean_x2*np.ones(N))
xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
maxVal=max(2.0/N * np.abs(yf[0:N//2]))
#plt.plot(xf, 2.0/N * np.abs(yf[0:N//2])/maxVal,label="aug",lw=2)

mask=t3>timeStart
mean_x3=np.mean(x3[mask]-1)
dtmax=t3[-1]-t3[0]
dt_min = t3[44]-t3[43]
yf = fft(x3[mask]-mean_x3*np.ones(N))
xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
maxVal=max(2.0/N * np.abs(yf[0:N//2]))
#plt.plot(xf, 2.0/N * np.abs(yf[0:N//2])/maxVal,label="halfway",lw=0.5,)
#plt.xscale('log')
plt.xlim(2/dtmax,5000)
plt.xlabel('frequency, Hz')
plt.ylabel('normalized amplitude')
plt.legend()
plt.show()
plt.savefig("pressureSpectra.png")

ax1 = plt.subplot(311)
plt.plot(t1, x1,lw=2)
plt.setp(ax1.get_xticklabels(), visible=False)
# share x only
ax2 = plt.subplot(312, sharex=ax1, visible=False)
plt.plot(t2, x2,lw=2)
# make these tick labels invisible
plt.setp(ax2.get_xticklabels())

ax3 = plt.subplot(313)
plt.plot(t3, x3,lw=2)
plt.setp(ax3.get_xticklabels())
plt.xlabel('time')

plt.savefig("pressureSignal.png")
plt.show()
