import numpy as np
import matplotlib.pyplot as plt
import Tkinter

ratioResults = open("flowRateResults")
xPlot = []
yPlot = []

innerResults = open("innerResults")
innerY = []

outerResults = open("outerResults")
outerY = []


a = 0
b = 0
xaxis = []
count = 0

# Reads in the values for X and Y axis
with ratioResults as o:
    for line in o:
        a, b = line.split()
        xPlot.append(a)
        yPlot.append(b)
        xaxis.append(count)
        count += 1

with innerResults as o:
    for line in o:
        innerY.append(line)

with outerResults as o:
     for line in o:
 	 outerY.append(line)

# Plots the grid
fig = plt.figure(figsize=[10, 10])
ax = plt.subplot(111)
ax2 = ax.twinx()

ratio = ax.plot(xaxis, yPlot, linewidth=3, color='r', label="Bypass Ratio")
plt.xticks(xaxis, xPlot, rotation='horizontal')


inner = ax2.plot(xaxis, outerY, linewidth = 3, label="Outer Mass Flow Rate")
outer = ax2.plot(xaxis, innerY, linewidth = 3, label="Inner Mass Flow Rate")


# Sets the axis titles
ax.set_xlabel('Time (microseconds)', fontsize=15)
ax.set_ylabel('Bypass Ratio of Mass Flow Rate', fontsize=15)
ax2.set_ylabel('Magnitude of Mass Flow Rate', fontsize=15)

lines = ratio+inner+outer
labs = [l.get_label() for l in lines]
ax.legend(lines, labs)

ax.set_ylim(0.25, 0.5)
ax2.set_ylim(5, 16)

# plt.axhline(y=1.87, color='b', linestyle='-')
#limit = float(yPlot[0]) + 1
#ax.set_ylim(0,round(limit))
plt.savefig("bypassRatio.png")
plt.show()
