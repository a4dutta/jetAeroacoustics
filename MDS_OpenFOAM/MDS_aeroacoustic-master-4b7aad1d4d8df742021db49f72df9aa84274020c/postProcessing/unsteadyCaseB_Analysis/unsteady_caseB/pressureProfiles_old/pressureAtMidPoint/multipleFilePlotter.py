import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

def find_nearestIndex(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def interpolate(x1, x2, x3, y1, y2, y3):
    dy1 = y2 - y1
    dy2 = y3 - y1
    dx  = x3 - x1
    x2 = dx*dy1/dy2 + x1
    return x2

fig = plt.figure(figsize=[16,10])
ax = fig.add_subplot(111)

time = [5, 400, 800, 1200, 1600]

count = 0
# Need to find more efficient way to do this....
for fname in ('pressure_00005_mid.curve', 'pressure_00400_mid.curve', 'pressure_00800_mid.curve', 'pressure_01200_mid.curve', 'pressure_01600_mid.curve'):
    data=np.loadtxt(fname)
    data=data[:len(data)]
    X=data[:,0] *100 
    Y=data[:,1]
    plt.plot(X - 3.8545,Y, linewidth=3, label="t = " + str(time[count]) + " microseconds")
    count += 1
    
ax.set_ylabel('Pressure Value (Pa)', fontsize=15)
ax.set_xlabel("y, cm of radial distance", fontsize=15)
plt.xlim(4, -4)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
# plt.show() #or
plt.savefig('pressureMid.png')
