

bad_words = ['#']

with open('pressure_halfway0000.curve') as oldfile, open('pressure_halfway.curve', 'w') as newfile:
    for line in oldfile:
        if not any(bad_word in line for bad_word in bad_words):
            newfile.write(line)
