import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate
from scipy.ndimage.filters import gaussian_filter

from scipy.fftpack import fft
import math as math
import pdb


count = 0
startTime = 0.55
endTime = 1.6

listOfFiles = []
listOfLabels = []
pos="halfway"
pos="incline"
#pos="lip"
fileInfo = open("fileInfo_"+pos)

with fileInfo as o:
    for line in o:
        a, b = line.split(', ')
        listOfFiles.append(a)
        listOfLabels.append(b)
        count = count + 1
        print(listOfLabels)

t_array = []
x_array = []

count = 0
# Need to find more efficient way to do this....
for fname in listOfFiles:
    t1,x1=np.loadtxt(fname,unpack=True)
    t1 = t1[t1 <= endTime]
    x1 = x1[t1 <= endTime]
    t1 = t1[t1 >= startTime]
    x1 = x1[t1 >= startTime]
    N=len(t1)
    mean_x1=np.mean(x1)
    dtmax=t1[-1]-t1[0]
    dt_min = t1[44]-t1[43]
    yf = fft(x1-mean_x1)
    #pdb.set_trace()
    xf = np.linspace(2/dtmax, 2.0/(dt_min), N/2)
    SPL = 20*np.log10(2.0/N * np.abs(yf/(2*10**-5)))
    # maxVal=max(2.0/N * np.abs(yf[0:N/2]))
    plt.figure(1)
    plt.plot(xf, (np.abs(SPL[0:N/2])),label=str(listOfLabels[count]),lw=2,alpha=0.5)
    plt.plot(xf, gaussian_filter(np.abs(SPL[0:N/2]),sigma=5),'k',lw=2)


    if fname==listOfFiles[0]:
      plt.figure(2)
      plt.plot(xf, (np.abs(SPL[0:N/2])),label=str(listOfLabels[count]),lw=2,alpha=0.5)
      plt.plot(xf, gaussian_filter(np.abs(SPL[0:N/2]),sigma=5),'k',lw=2)
      plt.xlim(300,xf[-1])
      plt.xlabel('Frequency, Hz')
      plt.ylabel('SPL')
      plt.savefig("pressureSpectra"+pos+"_1.png")

    plt.figure(3)
    plt.plot(t1,x1-mean_x1*np.ones(N) ,label=str(listOfLabels[count]),lw=2,alpha=0.3)
    plt.plot(t1, gaussian_filter(x1-mean_x1*np.ones(N),sigma=2),'k',lw=2)
    plt.xlabel('time')
    plt.ylabel('pressure fluctuation [Pa]')
    plt.xlim(1.2,1.525)
    plt.legend(loc=3)
    if fname==listOfFiles[-1]:
      plt.savefig("pressureSignal"+pos+"_1.png")
    #plt.xscale('log')
    count += 1

plt.figure(1)

plt.xlabel('Frequency, Hz')
plt.ylabel('SPL')
plt.xlim(300,xf[-1])

#ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
plt.legend(loc=3)
plt.savefig("pressureSpectra"+pos+".png")

# ax1 = plt.subplot(111)
# plt.plot(t_array, x_array,lw=2)
# plt.setp(ax1.get_xticklabels(), visible=False)
# # share x only
# plt.xlabel('time')
#
# plt.savefig("pressureSignal.png")
# plt.show()
