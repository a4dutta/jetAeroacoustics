import numpy as np
import matplotlib.pyplot as plt
import Tkinter

ratioResults = open("ratioResults")

xPlot = []
yPlot = []

a = 0
b = 0
xaxis = []
count = 0

# Reads in the values for X and Y axis
with ratioResults as o:
    for line in o:
        a, b = line.split()
        xPlot.append(a)
        yPlot.append(b)
        xaxis.append(count)
        count += 1

# Plots the grid
fig = plt.figure(figsize=[8,6])
ax = plt.subplot(111)
lines = plt.plot(xaxis, yPlot)
plt.setp(lines, linewidth=3, color='r')
plt.xticks(xaxis, xPlot, rotation='horizontal')

# Bolds the labels for the ticks
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(12)
    tick.label1.set_fontweight('bold')
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(12)
    tick.label1.set_fontweight('bold')

# Sets the axis titles
ax.set_xlabel('Mesh Design', fontsize=15)
ax.set_ylabel('Bypass Ratio', fontsize=15)
ax.set_title('Bypass Ratios for Varying Mesh Designs', fontsize=15)

plt.axhline(y=1.87, color='b', linestyle='-')
limit = float(yPlot[0]) + 1
ax.set_ylim(0,round(limit))
plt.savefig("bypassRatio.png")
plt.show()
