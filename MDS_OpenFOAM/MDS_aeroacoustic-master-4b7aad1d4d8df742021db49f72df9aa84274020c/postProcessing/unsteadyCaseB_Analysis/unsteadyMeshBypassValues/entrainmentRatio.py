import os
import numpy as np
import math
from scipy.integrate import simps
from scipy.integrate import cumtrapz
import pdb
def file_path(fileName):
    if os.path.isfile(fileName):
        path = os.path.dirname(filename)
        return path
    else:
        print "Invalid file path or file doesn't exist"
        return None

# innerMagnitude = []
# outerMagnitude = []
#
innerRadius = []
outerRadius = []

innerValues = []
outerValues = []

#iValues = raw_input("Enter name of file containing the inner radius values: ")
iValues = "testValues"

#oValues = raw_input("Enter name file containing the outer radius values: ")
oValues = "unsteadyM9Outer.curve"

engineValuesFile = open(iValues)
outerValuesFile = open(oValues)

# placeholder for I/O
a = 0
b = 0

# read files and save values into respective arrays
print("Reading files and storing values now.")
with engineValuesFile as i:
    next(i)
    for line in i:
        a, b = line.split()
	print (b)
        phi_r = float(b)
        innerValues.append(phi_r)
        innerRadius.append(float(a))
        # innerMagnitude.append(float(b))

with outerValuesFile as o:
    next(o)
    for line in o:
        a, b = line.split()
        outerRadius.append(float(a))
        phi_r = float(a) * float(b)
        outerValues.append(phi_r)
        # outerMagnitude.append(float(b))

# convert to numpy array for mathematical operations
# innerMagnitude = np.array(innerMagnitude)
# outerMagnitude = np.array(outerMagnitude)

innerRadius = np.array(innerRadius)
outerRadius = np.array(outerRadius)

innerValues = np.array(innerValues)
outerValues = np.array(outerValues)


# integral of circumference is area
def circ(radialValue):
    return radialValue

print("Calculating the inner and outer entrainment values...")
# integrate the inner values first
findArea = circ(innerValues)
entrainmentInner = simps(innerValues, innerRadius)

# This above operation is equivalent to doing:
# integral between 0 to innerRadius of 2pi*phi_r(r)*dr
# where phi_r(r) = r * phi

# integrate the outer radius values next
findArea = circ(outerValues)
entrainmentOuter = simps(findArea, outerRadius)

ratio = entrainmentInner/entrainmentOuter
print("Inner integral value is: " + str(entrainmentInner))
print ("Outer integral value is: " + str(entrainmentOuter))
print ("Entrainment ratio is: " + str(ratio))
