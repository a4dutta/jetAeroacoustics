#  visit -nowin -cli -s
# Change these parameters
DB = "../scale_caseB_full_coarse_M09/eachSnap/flow_*.plt database"
Mesh = "Mesh"
Var = "Pressure"
side= 0.077
p0 = (0.265,0,0)
p1 = (0.265, side, 0)
nameF="pressure_radial_M09_"

#########################################################################

OpenDatabase(DB)
AddPlot("Pseudocolor", Var)
AddOperator("Slice",1)
DrawPlots()

# Do a lineout on all 4 variables to produce 4 curves.
AddOperator("Lineout",1)
LineoutAtts = LineoutAttributes()
LineoutAtts.point1 = p0
LineoutAtts.point2 = p1
LineoutAtts.interactive = 0
LineoutAtts.ignoreGlobal = 0
LineoutAtts.samplingOn = 0
LineoutAtts.numberOfSamplePoints = 50
LineoutAtts.reflineLabels = 0
SetOperatorOptions(LineoutAtts, 1)

SetActiveWindow(2)


LineoutAtts = LineoutAttributes()
LineoutAtts.point1 = p0
LineoutAtts.point2 = p1
LineoutAtts.interactive = 0
LineoutAtts.ignoreGlobal = 0
LineoutAtts.samplingOn = 0
LineoutAtts.numberOfSamplePoints = 50
LineoutAtts.reflineLabels = 0
SetOperatorOptions(LineoutAtts, 1)

SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 1
SaveWindowAtts.outputDirectory = "."
SaveWindowAtts.fileName = nameF
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.CURVE  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1024
SaveWindowAtts.height = 1024
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 80
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.ScreenProportions  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 0
SetSaveWindowAttributes(SaveWindowAtts)
for ii in range(1000):
  SaveWindow()
  TimeSliderNextState()
quit()
                                                              65,1          Bot
