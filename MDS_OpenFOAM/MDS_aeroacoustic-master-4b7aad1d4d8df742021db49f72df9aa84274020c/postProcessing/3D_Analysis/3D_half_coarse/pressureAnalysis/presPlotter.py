import numpy as np
import matplotlib.pyplot as plt
import Tkinter
import sys
import os



# file1 = raw_input("Input name of file containing simulated Mach values: ")
# file2 = raw_input("Input name of file containing experimental Mach values: ")

# machMeshResults = open(file1)
# machRealResults = open(file2)


machMeshResults = open("3D_Pressure_V1.curve")
machRealResults = open('exp1_pres')

# Will contain the data for the mesh Mach values
meshX = []
meshY = []

# Will contain the data for the real Mach values
realX = []
realY = []

# Used to read in the Mach values from .curve file
a = 0
b = 0
xaxis = []
count = 0

# Reads in the values for X and Y axis
with machMeshResults as o:
    next(o)
    for line in o:
        a, b = line.split()
        a = float(a)
	b = float(b)
        meshX.append(a)
        meshY.append(b)

with machRealResults as o:
    for line in o:
        a, b = line.split()
        realX.append(a)
        realY.append(b)

# Plots the grid
fig = plt.figure(figsize=[12,10])
ax = fig.add_subplot(111)

# plt.setp(meshDataLine, linewidth=2, color='r')
# plt.xticks(xaxis, xPlot, rotation='horizontal')

# ax2 = ax.twinx()
# ax2 = ax.twiny()

meshDataLine, = ax.plot(meshX, meshY, 'r', label="Simulated")
rawDataLine = ax.scatter(realX, realY, c='b', s = 30, label="Experimental")
# plt.setp(rawDataLine, linewidth=2, color='b')

# Sets the axis titles
ax.set_ylabel('Total Pressure Value', fontsize=15)
ax.set_xlabel("y, cm of Extracted Value", fontsize=15)

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
ax.set_xlim(0, round(float(meshX[-1])+1))
ax.set_ylim(0)
plt.show()
