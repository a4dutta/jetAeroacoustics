import numpy as np
import matplotlib.pyplot as plt
import pdb
N=100
myfile="twoPts_lowM_A.curve"
timeA, pA=np.loadtxt(myfile,unpack=True,comments='#')
myfile="twoPts_lowM_B.curve"
timeB, pB=np.loadtxt(myfile,unpack=True,comments='#')
lenT=len(timeA)
#plt.plot(timeA,pA)
pA=pA-np.mean(pA)
pB=pB-np.mean(pB)
dTrange=np.arange(N)
corr=np.zeros(N)
for tt,dt in enumerate(dTrange):
  num=0.0
  denum=0.0
  #pdb.set_trace()
  for ii in range(lenT-dt-1):
    num += (pB[ii]*pB[ii+dt])
    denum+=  (pB[ii]**2)
  corr[tt]=num/denum
plt.plot(dTrange,corr,lw=3,label="lowM")



myfile="twoPts_highM_A.curve"
timeA, pA=np.loadtxt(myfile,unpack=True,comments='#')
myfile="twoPts_highM_B.curve"
timeB, pB=np.loadtxt(myfile,unpack=True,comments='#')
lenT=len(timeA)
#plt.plot(timeA,pA)
pA=pA-np.mean(pA)
pB=pB-np.mean(pB)
dTrange=np.arange(N)
corr=np.zeros(N)
for tt,dt in enumerate(dTrange):
  num=0.0
  denum=0.0
  #pdb.set_trace()
  for ii in range(lenT-dt-1):
    num += (pB[ii]*pB[ii+dt])
    denum+= (pB[ii]**2)
  corr[tt]=num/denum
plt.plot(dTrange,corr,lw=3,label="highM")
plt.xlabel("dT")

plt.ylabel("correlation")
plt.legend(loc=0)
plt.savefig("Correlation_in_timeB.png")
plt.show()
