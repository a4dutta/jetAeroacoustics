AddPlot("Pseudocolor", "Pressure", 1, 1)
DrawPlots()
SetWindowMode("node pick")
PickAtts = GetPickAttributes()
PickAtts.variables = ("default", "Pressure")
PickAtts.showIncidentElements = 1
PickAtts.showNodeId = 1
PickAtts.showNodeDomainLogicalCoords = 0
PickAtts.showNodeBlockLogicalCoords = 0
PickAtts.showNodePhysicalCoords = 0
PickAtts.showZoneId = 1
PickAtts.showZoneDomainLogicalCoords = 0
PickAtts.showZoneBlockLogicalCoords = 0
PickAtts.doTimeCurve = 0
PickAtts.conciseOutput = 0
PickAtts.showTimeStep = 1
PickAtts.showMeshName = 1
PickAtts.blockPieceName = "zone"
PickAtts.groupPieceName = "group"
PickAtts.showGlobalIds = 0
PickAtts.showPickLetter = 1
PickAtts.reusePickLetter = 0
PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
PickAtts.createSpreadsheet = 0
PickAtts.floatFormat = "%g"
PickAtts.timePreserveCoord = 1
PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
SetPickAttributes(PickAtts)
QueryOverTimeAtts = GetQueryOverTimeAttributes()
QueryOverTimeAtts.timeType = QueryOverTimeAtts.DTime  # Cycle, DTime, Timestep
QueryOverTimeAtts.startTimeFlag = 0
QueryOverTimeAtts.startTime = 0
QueryOverTimeAtts.endTimeFlag = 0
QueryOverTimeAtts.endTime = 1
QueryOverTimeAtts.strideFlag = 0
QueryOverTimeAtts.stride = 1
QueryOverTimeAtts.createWindow = 1
QueryOverTimeAtts.windowId = 2
SetQueryOverTimeAttributes(QueryOverTimeAtts)
PickAtts = GetPickAttributes()
PickAtts.variables = ("default", "Pressure")
PickAtts.showIncidentElements = 1
PickAtts.showNodeId = 1
PickAtts.showNodeDomainLogicalCoords = 0
PickAtts.showNodeBlockLogicalCoords = 0
PickAtts.showNodePhysicalCoords = 0
PickAtts.showZoneId = 1
PickAtts.showZoneDomainLogicalCoords = 0
PickAtts.showZoneBlockLogicalCoords = 0
PickAtts.doTimeCurve = 1
PickAtts.conciseOutput = 0
PickAtts.showTimeStep = 1
PickAtts.showMeshName = 1
PickAtts.blockPieceName = "zone"
PickAtts.groupPieceName = "group"
PickAtts.showGlobalIds = 0
PickAtts.showPickLetter = 1
PickAtts.reusePickLetter = 0
PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
PickAtts.createSpreadsheet = 0
PickAtts.floatFormat = "%g"
PickAtts.timePreserveCoord = 1
PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
SetPickAttributes(PickAtts)
PickAtts = GetPickAttributes()
PickAtts.variables = ("default", "Pressure")
PickAtts.showIncidentElements = 1
PickAtts.showNodeId = 1
PickAtts.showNodeDomainLogicalCoords = 0
PickAtts.showNodeBlockLogicalCoords = 0
PickAtts.showNodePhysicalCoords = 0
PickAtts.showZoneId = 1
PickAtts.showZoneDomainLogicalCoords = 0
PickAtts.showZoneBlockLogicalCoords = 0
PickAtts.doTimeCurve = 1
PickAtts.conciseOutput = 0
PickAtts.showTimeStep = 1
PickAtts.showMeshName = 1
PickAtts.blockPieceName = "zone"
PickAtts.groupPieceName = "group"
PickAtts.showGlobalIds = 0
PickAtts.showPickLetter = 0
PickAtts.reusePickLetter = 1
PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
PickAtts.createSpreadsheet = 0
PickAtts.floatFormat = "%g"
PickAtts.timePreserveCoord = 1
PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
SetPickAttributes(PickAtts)
NodePick(coord=(0.134622, 0.00596972, 0), vars=("default", "Pressure"))
PickAtts = GetPickAttributes()
PickAtts.variables = ("default", "Pressure")
PickAtts.showIncidentElements = 1
PickAtts.showNodeId = 1
PickAtts.showNodeDomainLogicalCoords = 0
PickAtts.showNodeBlockLogicalCoords = 0
PickAtts.showNodePhysicalCoords = 0
PickAtts.showZoneId = 1
PickAtts.showZoneDomainLogicalCoords = 0
PickAtts.showZoneBlockLogicalCoords = 0
PickAtts.doTimeCurve = 1
PickAtts.conciseOutput = 0
PickAtts.showTimeStep = 1
PickAtts.showMeshName = 1
PickAtts.blockPieceName = "zone"
PickAtts.groupPieceName = "group"
PickAtts.showGlobalIds = 0
PickAtts.showPickLetter = 1
PickAtts.reusePickLetter = 0
PickAtts.meshCoordType = PickAtts.XY  # XY, RZ, ZR
PickAtts.createSpreadsheet = 0
PickAtts.floatFormat = "%g"
PickAtts.timePreserveCoord = 1
PickAtts.timeCurveType = PickAtts.Single_Y_Axis  # Single_Y_Axis, Multiple_Y_Axes
SetPickAttributes(PickAtts)
SaveSession("/home/jean-pierre/Downloads/visit0000.session")
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 1
SaveWindowAtts.outputDirectory = "."
SaveWindowAtts.fileName = "visit"
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.CURVE  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1024
SaveWindowAtts.height = 1024
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 80
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.ScreenProportions  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 0
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

SetActiveWindow(2)
SaveWindowAtts = SaveWindowAttributes()
SaveWindowAtts.outputToCurrentDirectory = 1
SaveWindowAtts.outputDirectory = "."
SaveWindowAtts.fileName = "visit"
SaveWindowAtts.family = 1
SaveWindowAtts.format = SaveWindowAtts.CURVE  # BMP, CURVE, JPEG, OBJ, PNG, POSTSCRIPT, POVRAY, PPM, RGB, STL, TIFF, ULTRA, VTK, PLY
SaveWindowAtts.width = 1024
SaveWindowAtts.height = 1024
SaveWindowAtts.screenCapture = 0
SaveWindowAtts.saveTiled = 0
SaveWindowAtts.quality = 80
SaveWindowAtts.progressive = 0
SaveWindowAtts.binary = 0
SaveWindowAtts.stereo = 0
SaveWindowAtts.compression = SaveWindowAtts.PackBits  # None, PackBits, Jpeg, Deflate
SaveWindowAtts.forceMerge = 0
SaveWindowAtts.resConstraint = SaveWindowAtts.ScreenProportions  # NoConstraint, EqualWidthHeight, ScreenProportions
SaveWindowAtts.advancedMultiWindowSave = 0
SetSaveWindowAttributes(SaveWindowAtts)
SaveWindow()

