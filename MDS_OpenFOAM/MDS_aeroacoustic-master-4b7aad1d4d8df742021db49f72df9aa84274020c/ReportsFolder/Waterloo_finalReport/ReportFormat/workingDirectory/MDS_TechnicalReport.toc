\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Background and Problem Description}{6}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Background}{6}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Timeline}{6}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Bypass ratio}{7}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}VnV}{8}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Validation on canonical test cases}{8}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Structured and Unstructured Mesh}{8}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Grid convergence study}{8}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Scaling tests}{8}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Numerical Setup}{9}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Physical Mechanism of Noise Generation }{10}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Physical background}{10}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Meshing}{10}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Low-order model }{11}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Modelling jet flow}{11}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Closure of the turbulent jet}{11}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Appendix}{13}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Mesh Generation}{13}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.1}Fully structured mesh}{13}{subsection.A.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.1.2}Hybrid or unstructured mesh}{13}{subsection.A.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Code snippets sample}{13}{section.A.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}SU2}{13}{section.A.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.3.1}Computation of the mass flow rate}{13}{subsection.A.3.1}
\contentsfinish 
