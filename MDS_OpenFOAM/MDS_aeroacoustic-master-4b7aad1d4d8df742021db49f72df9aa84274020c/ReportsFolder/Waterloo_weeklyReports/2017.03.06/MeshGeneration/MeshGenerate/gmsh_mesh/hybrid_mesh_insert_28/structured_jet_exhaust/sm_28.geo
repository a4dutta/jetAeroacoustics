// Gmsh project created on Mon Dec 12 10:21:54 2016
ls1 = 0.05;
//+
Point(1) = {0, 0, 0, ls1};
//+
Point(2) = {0.17043, 0, 0, ls1};
//+
Point(3) = {0.202798974573, 0, 0, ls1};
//+
Point(4) = {0.213778974573, 0, 0, ls1};
//+
Point(5) = {0.263698974573, 0, 0, ls1};
//+
Point(6) = {1, 0, 0, ls1};
//+
Point(7) = {0, 0.038545, 0, ls1/2};
//+
Point(8) = {0.17043, 0.038545, 0, ls1/2};
//+
Point(9) = {0.202798974573, 0.038545, 0, ls1/2};
//+
Point(10) = {0.213778974573, 0.038545, 0, ls1/2};
//+
Point(11) = {0.263698974573, 0.038545, 0, ls1/2};
//+
Point(12) = {1, 0.038545, 0, ls1};
//+
Point(13) = {0, 0.043545, 0, ls1/2};
//+
Point(14) = {0.17043, 0.043545, 0, ls1/2};
//+
Point(15) = {0.202798974573, 0.043545, 0, ls1/2};
//+
Point(16) = {0.213778974573, 0.043545, 0, ls1/2};
//+
Point(17) = {0.263698974573, 0.043545, 0, ls1/2};
//+
Point(18) = {1, 0.043545, 0, ls1};
//+
Point(19) = {0, 0.099755, 0, ls1};
//+
Point(20) = {0.17043, 0.099755, 0, ls1/3};
//+
Point(21) = {0.202798974573, 0.07709, 0, ls1/3};
//+
Point(22) = {0.213778974573, 0.06943, 0, ls1/3};
//+
Point(23) = {0.263698974573, 0.06943, 0, ls1/3};
//+
Point(24) = {0.263698974573, 0.07709, 0, ls1/3};
//+
Point(25) = {1, 0.07709, 0, ls1};
//+
Point(26) = {1, 0.06943, 0, ls1};
//+
Point(27) = {-0.4, 0, 0, ls1};
//+
Point(28) = {-0.4, 0.052, 0, ls1};
//+
Point(29) = {-0.4, 0.099755, 0, ls1};
//+
Point(30) = {-0.4, 0.1778, 0, ls1};
//+
Point(31) = {-0.2667, 0.1778, 0, ls1};
//+
Point(32) = {-0.054, 0.1778, 0, ls1};
//+
Point(33) = {0, 0.1778, 0, ls1};
//+
Point(34) = {0.17043, 0.1778, 0, ls1};
//+
Point(35) = {-0.2667, 0, 0, ls1/2};
//+
Point(36) = {-0.2667, 0.052, 0, ls1/2};
//+
Point(37) = {-0.054, 0.052, 0, ls1/2};
//+
Point(38) = {-0.2667, 0.099755, 0, ls1};
//+
Point(39) = {-0.054, 0.099755, 0, ls1};
//+
Point(40) = {-0.1778, 0, 0, ls1};
//+
Point(41) = {-0.0889, 0, 0, ls1};
//+
Point(42) = {-0.1778, 0.045, 0, ls1};
//+
Point(43) = {-0.0889, 0.045, 0, ls1};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 8};
//+
Line(3) = {8, 7};
//+
Line(4) = {7, 1};
//+
Line(5) = {2, 3};
//+
Line(6) = {3, 9};
//+
Line(7) = {9, 8};
//+
Line(8) = {3, 4};
//+
Line(9) = {4, 10};
//+
Line(10) = {10, 9};
//+
Line(11) = {4, 5};
//+
Line(12) = {5, 11};
//+
Line(13) = {11, 10};
//+
Line(14) = {8, 14};
//+
Line(15) = {14, 13};
//+
Line(16) = {13, 7};
//+
Line(17) = {9, 15};
//+
Line(18) = {15, 14};
//+
Line(19) = {10, 16};
//+
Line(20) = {16, 15};
//+
Line(21) = {11, 17};
//+
Line(22) = {17, 16};
//+
Line(23) = {14, 20};
//+
Line(24) = {20, 19};
//+
Line(25) = {19, 13};
//+
Line(26) = {15, 21};
//+
Line(27) = {21, 20};
//+
Line(28) = {16, 22};
//+
Line(29) = {22, 21};
//+
Line(30) = {17, 23};
//+
Line(31) = {23, 22};
//+
Line(32) = {23, 24};
//+
Line(33) = {5, 6};
//+
Line(34) = {6, 12};
//+
Line(35) = {12, 11};
//+
Line(36) = {12, 18};
//+
Line(37) = {18, 17};
//+
Line(38) = {18, 26};
//+
Line(39) = {26, 23};
//+
Line(40) = {26, 25};
//+
Line(41) = {25, 24};
//+
Line(42) = {20, 34};
//+
Line(43) = {34, 33};
//+
Line(44) = {33, 19};
//+
Line(45) = {39, 19};
//+
Line(46) = {33, 32};
//+
Line(47) = {32, 39};
//+
Line(48) = {13, 37};
//+
Line(49) = {7, 43};
//+
Line(50) = {37, 39};
//+
Line(51) = {32, 31};
//+
Line(52) = {31, 38};
//+
Line(53) = {38, 39};
//+
Line(54) = {37, 36};
//+
Line(55) = {36, 35};
//+
Line(56) = {42, 43};
//+
Line(57) = {42, 40};
//+
Line(58) = {40, 41};
//+
Line(59) = {41, 1};
//+
Line(60) = {41, 43};
//+
Line(61) = {38, 36};
//+
Line(62) = {31, 30};
//+
Line(63) = {30, 29};
//+
Line(64) = {29, 38};
//+
Line(65) = {28, 29};
//+
Line(66) = {28, 36};
//+
Line(67) = {27, 35};
//+
Line(68) = {27, 28};
//+
Line Loop(69) = {57, 58, 60, -56};
//+
Plane Surface(70) = {69};
//+
Line Loop(71) = {49, -60, 59, -4};
//+
Plane Surface(72) = {71};
//+
Line Loop(73) = {4, 1, 2, 3};
//+
Plane Surface(74) = {73};
//+
Line Loop(75) = {2, -7, -6, -5};
//+
Plane Surface(76) = {75};
//+
Line Loop(77) = {6, -10, -9, -8};
//+
Plane Surface(78) = {77};
//+
Line Loop(79) = {13, -9, 11, 12};
//+
Plane Surface(80) = {79};
//+
Line Loop(81) = {12, -35, -34, -33};
//+
Plane Surface(82) = {81};
//+
Line Loop(83) = {37, -21, -35, 36};
//+
Plane Surface(84) = {83};
//+
Line Loop(85) = {21, 22, -19, -13};
//+
Plane Surface(86) = {85};
//+
Line Loop(87) = {10, 17, -20, -19};
//+
Plane Surface(88) = {87};
//+
Line Loop(89) = {14, -18, -17, 7};
//+
Plane Surface(90) = {89};
//+
Line Loop(91) = {3, -16, -15, -14};
//+
Plane Surface(92) = {91};
//+
Line Loop(93) = {25, -15, 23, 24};
//+
Plane Surface(94) = {93};
//+
Line Loop(95) = {23, -27, -26, 18};
//+
Plane Surface(96) = {95};
//+
Line Loop(97) = {26, -29, -28, 20};
//+
Plane Surface(98) = {97};
//+
Line Loop(99) = {28, -31, -30, 22};
//+
Plane Surface(100) = {99};
//+
Line Loop(101) = {30, -39, -38, 37};
//+
Plane Surface(102) = {101};
//+
Line Loop(103) = {32, -41, -40, 39};
//+
Plane Surface(104) = {103};
//+
Line Loop(105) = {44, -24, 42, 43};
//+
Plane Surface(106) = {105};
//+
Line Loop(107) = {47, 45, -44, 46};
//+
Plane Surface(108) = {107};
//+
Line Loop(109) = {50, 45, 25, 48};
//+
Plane Surface(110) = {109};
//+
Line Loop(111) = {54, -61, 53, -50};
//+
Plane Surface(112) = {111};
//+
Line Loop(113) = {52, 53, -47, 51};
//+
Plane Surface(114) = {113};
//+
Line Loop(115) = {68, 66, 55, -67};
//+
Plane Surface(116) = {115};
//+
Line Loop(117) = {65, 64, 61, -66};
//+
Plane Surface(118) = {117};
//+
Line Loop(119) = {63, 64, -52, 62};
//+
Plane Surface(120) = {119};
//+
Physical Line("axis") = {67, 58, 59, 1, 5, 8, 11, 33};
//+
Physical Line("enginein") = {55};
//+
Physical Line("engineout") = {57};
//+
Physical Line("inlet") = {68, 65, 63};
//+
Physical Line("outlet") = {34, 36, 38, 40};
//+
Physical Line("walls") = {62, 51, 46, 43, 42, 27, 29, 31, 32, 41};
//+
Physical Line("engsurfin") = {56, 49, 16};
//+
Physical Line("engsurfout") = {48, 54};
//+
Physical Surface("empty") = {116, 118, 120, 112, 114, 110, 108, 70, 72, 74, 92, 94, 106, 76, 90, 96, 78, 88, 98, 80, 86, 100, 82, 84, 102, 104};
//+
Transfinite Line {57, 60} = 10 Using Progression 1;
//+
Transfinite Line {56, 58, 56} = 10 Using Progression 1;
//+
Transfinite Surface {70};
//+
Recombine Surface {70};
//+
Transfinite Line {60, 4} = 10 Using Progression 1;
//+
Transfinite Line {49, 59} = 10 Using Progression 1;
//+
Transfinite Surface {72};
//+
Recombine Surface {72};
//+
Transfinite Line {4, 2, 6, 9, 12, 34} = 10 Using Progression 1;
//+
Transfinite Line {3, 1} = 20 Using Progression 1;
//+
Transfinite Surface {74};
//+
Recombine Surface {74};
//+
Transfinite Line {7, 5} = 5 Using Progression 1;
//+
Transfinite Surface {76};
//+
Recombine Surface {76};
//+
Transfinite Line {10, 8} = 2 Using Progression 1;
//+
Transfinite Line {13, 11} = 8 Using Progression 1;
//+
Transfinite Line {35, -33} = 50 Using Progression 0.99;
//+
Transfinite Surface {78};
//+
Transfinite Surface {80};
//+
Transfinite Surface {82};
//+
Recombine Surface {78};
//+
Recombine Surface {80};
//+
Recombine Surface {82};
//+
Transfinite Line {16, 14, 17, 19, 21, 36} = 2 Using Progression 1;
//+
Transfinite Line {15} = 20 Using Progression 1;
//+
Transfinite Line {18} = 5 Using Progression 1;
//+
Transfinite Line {20} = 2 Using Progression 1;
//+
Transfinite Line {22} = 8 Using Progression 1;
//+
Transfinite Line {37} = 50 Using Progression 0.99;
//+
Transfinite Surface {92};
//+
Transfinite Surface {90};
//+
Transfinite Surface {88};
//+
Transfinite Surface {86};
//+
Transfinite Surface {84};
//+
Recombine Surface {92};
//+
Recombine Surface {90};
//+
Recombine Surface {88};
//+
Recombine Surface {86};
//+
Recombine Surface {84};
