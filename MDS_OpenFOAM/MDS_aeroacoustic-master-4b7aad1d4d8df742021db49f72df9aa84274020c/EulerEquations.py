import numpy as np
import sympy  as sp
from sympy import MatrixSymbol, Matrix
X = MatrixSymbol('X', 3, 3)
rho1, u1, p1,A1, rho2, u2, p2,A2 = sp.symbols('rho1, u1, p1,A1, rho2, u2, p2,A2')


T_inv = Matrix( [[1, 0, 0, 0],[u1, rho1, 0, 0],[c1,c2,c3]])
