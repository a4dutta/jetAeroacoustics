#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 11:38:31 2019

@author: adutta
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import pickle
import os
from natsort import natsorted

os.chdir('./pts_data_latest') #folder containing processed files
An = pickle.load(open('pts2_proc_data.p','rb')) #name of processed file i.e. output of PQRS2 or PQRS4

Qm = An[0]
Rm = An[1]
Qsm = An[2]
Rsm = An[3]
Wwm = An[4]


for qm,rm in zip(Qm,Rm):
    data = [1*qm,rm]

    sandR = np.max(np.abs(data[1]))
    sandQ = np.max(np.abs(data[0]))
    xi = np.linspace(-sandR/10,sandR/10,100)
    yi = np.linspace(-sandQ/10,sandQ/10,100)
    hst = np.histogramdd(data,bins=[xi,yi])

    xx = hst[1][0]
    yy = hst[1][1]

    xx = xx[0:99]
    yy = yy[0:99]
    [X,Y] = np.meshgrid(xx,yy)
    plt.contour(X,Y,hst[0])
    #plt.scatter(hst[0])
    plt.xlim(-1.5, 1.5)
    plt.ylim(-1.5, 1.5)
    #plt.xlim(0, 1)
    #plt.ylim(0, 1)

    plt.hold('True')

xx_alt = np.linspace(-1,1,5000)
Qsmp1 = (27/4)*(xx_alt*xx_alt)
Qsmp2 = -1*np.cbrt(Qsmp1)
#Qsmp1 = (27/4)*(xx*xx)

plt.plot(xx_alt,Qsmp2)
plt.grid('on')
#plt.colorbar()
os.chdir('../')