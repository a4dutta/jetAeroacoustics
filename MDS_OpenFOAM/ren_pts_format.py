#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 00:14:39 2018

@author: adutta
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
import re
import os

def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

def lamb_vec(filename,exp_filename,num):
    df = pd.read_csv(filename,skiprows=3,header=None,delim_whitespace=True)
    df = df[:-1]
    df.columns = ['x', 'y', 'z', 'u0', 'v0', 'w0', 'p', 'Wx', 'Wy', 'Wz', 'Wx2', 'Wy2', 'Wz2']
    
    df.drop('Wz2',axis=1,inplace=True)
    df.drop('Wy2',axis=1,inplace=True)
    df.drop('Wx2',axis=1,inplace=True)    
    
    df.to_csv(exp_filename+'_'+num+'.pts',sep=' ',header=False)
    
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<POINTS DIM="3" FIELDS="x,y,z,u0,v0,w0,p,Wx,Wy,Wz" >\n' + old) # write the new line before
         
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<NEKTAR>\n' + old) # write the new line before
         
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<?xml version="1.0" encoding="utf-8" ?>\n' + old) # write the new line before
           
    with open(exp_filename+'_'+num+'.pts', "a+") as f:
         f.write('</POINTS>\n') 
         
    with open(exp_filename+'_'+num+'.pts', "a+") as f:
         f.write('</NEKTAR>') 


files = os.listdir('./pts_folder')
os.chdir('./pts_folder')
files = natural_sorted(files)

i = 0
for f in files:
    lamb_vec(f,f+'ren',str(i))
    i = i+1

os.chdir('../')
