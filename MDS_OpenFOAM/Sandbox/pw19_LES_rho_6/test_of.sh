#!/bin/bash
#
#SBATCH --nodes 1
#SBATCH -o slurm.%N.%j.out
#SBATCH -t 8:00:00 
 

# 2014-09-26 DSP - Example batch job script for a parallel OpenFOAM run on Parallel or Lattice



# Set up the environment

module load gcc/7.3.0  
module load openmpi/3.0.1
module load openfoam/5.0



decomposePar
mpirun -np 40 rhoPimpleFoam
echo "Finished at `date`"
