#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "FieldUtils" for configuration "Release"
set_property(TARGET FieldUtils APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FieldUtils PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libFieldUtils.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libFieldUtils.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS FieldUtils )
list(APPEND _IMPORT_CHECK_FILES_FOR_FieldUtils "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libFieldUtils.so.4.4.1" )

# Import target "GlobalMapping" for configuration "Release"
set_property(TARGET GlobalMapping APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(GlobalMapping PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libGlobalMapping.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libGlobalMapping.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS GlobalMapping )
list(APPEND _IMPORT_CHECK_FILES_FOR_GlobalMapping "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libGlobalMapping.so.4.4.1" )

# Import target "LibUtilities" for configuration "Release"
set_property(TARGET LibUtilities APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(LibUtilities PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libLibUtilities.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libLibUtilities.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS LibUtilities )
list(APPEND _IMPORT_CHECK_FILES_FOR_LibUtilities "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libLibUtilities.so.4.4.1" )

# Import target "LocalRegions" for configuration "Release"
set_property(TARGET LocalRegions APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(LocalRegions PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libLocalRegions.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libLocalRegions.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS LocalRegions )
list(APPEND _IMPORT_CHECK_FILES_FOR_LocalRegions "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libLocalRegions.so.4.4.1" )

# Import target "Collections" for configuration "Release"
set_property(TARGET Collections APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(Collections PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libCollections.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libCollections.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS Collections )
list(APPEND _IMPORT_CHECK_FILES_FOR_Collections "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libCollections.so.4.4.1" )

# Import target "MultiRegions" for configuration "Release"
set_property(TARGET MultiRegions APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(MultiRegions PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libMultiRegions.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libMultiRegions.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS MultiRegions )
list(APPEND _IMPORT_CHECK_FILES_FOR_MultiRegions "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libMultiRegions.so.4.4.1" )

# Import target "SpatialDomains" for configuration "Release"
set_property(TARGET SpatialDomains APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(SpatialDomains PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libSpatialDomains.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libSpatialDomains.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS SpatialDomains )
list(APPEND _IMPORT_CHECK_FILES_FOR_SpatialDomains "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libSpatialDomains.so.4.4.1" )

# Import target "StdRegions" for configuration "Release"
set_property(TARGET StdRegions APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(StdRegions PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libStdRegions.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libStdRegions.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS StdRegions )
list(APPEND _IMPORT_CHECK_FILES_FOR_StdRegions "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libStdRegions.so.4.4.1" )

# Import target "SolverUtils" for configuration "Release"
set_property(TARGET SolverUtils APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(SolverUtils PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libSolverUtils.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libSolverUtils.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS SolverUtils )
list(APPEND _IMPORT_CHECK_FILES_FOR_SolverUtils "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libSolverUtils.so.4.4.1" )

# Import target "NekMeshUtils" for configuration "Release"
set_property(TARGET NekMeshUtils APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(NekMeshUtils PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libNekMeshUtils.so.4.4.1"
  IMPORTED_SONAME_RELEASE "libNekMeshUtils.so.4.4.1"
  )

list(APPEND _IMPORT_CHECK_TARGETS NekMeshUtils )
list(APPEND _IMPORT_CHECK_FILES_FOR_NekMeshUtils "${_IMPORT_PREFIX}/lib64/nektar++-4.4.1/libNekMeshUtils.so.4.4.1" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
