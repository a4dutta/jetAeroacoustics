# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(FieldUtils)
subdirs(GlobalMapping)
subdirs(LibUtilities)
subdirs(LocalRegions)
subdirs(Collections)
subdirs(MultiRegions)
subdirs(SpatialDomains)
subdirs(StdRegions)
subdirs(UnitTests)
subdirs(Demos)
subdirs(SolverUtils)
subdirs(NekMeshUtils)
