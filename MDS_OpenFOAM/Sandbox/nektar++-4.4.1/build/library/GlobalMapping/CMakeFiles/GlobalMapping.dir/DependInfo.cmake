# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/Deform.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/Deform.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/Mapping.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/Mapping.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingGeneral.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingGeneral.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingTranslation.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingTranslation.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingXYofXY.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingXYofXY.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingXYofZ.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingXYofZ.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingXofXZ.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingXofXZ.cpp.o"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/GlobalMapping/MappingXofZ.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/CMakeFiles/GlobalMapping.dir/MappingXofZ.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLOBAL_MAPPING_EXPORTS"
  "NEKTAR_MEMORY_POOL_ENABLED"
  "NEKTAR_USING_BLAS"
  "NEKTAR_USING_LAPACK"
  "NEKTAR_VERSION=\"4.4.1\""
  "TIXML_USE_STL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "ThirdParty/dist/include"
  "/home/adutta/anaconda3/include"
  "../"
  "../library"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/libGlobalMapping.so" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/GlobalMapping/libGlobalMapping.so.4.4.1"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions/CMakeFiles/MultiRegions.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections/CMakeFiles/Collections.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions/CMakeFiles/LocalRegions.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains/CMakeFiles/SpatialDomains.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions/CMakeFiles/StdRegions.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities/CMakeFiles/LibUtilities.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
