# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/Demos
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Demos
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(LibUtilities)
subdirs(StdRegions)
subdirs(SpatialDomains)
subdirs(LocalRegions)
subdirs(Collections)
subdirs(MultiRegions)
