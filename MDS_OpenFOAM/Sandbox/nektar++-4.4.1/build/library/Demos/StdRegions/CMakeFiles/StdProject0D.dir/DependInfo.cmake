# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/Demos/StdRegions/StdProject0D.cpp" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Demos/StdRegions/CMakeFiles/StdProject0D.dir/StdProject0D.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "NEKTAR_MEMORY_POOL_ENABLED"
  "NEKTAR_USING_BLAS"
  "NEKTAR_USING_LAPACK"
  "NEKTAR_VERSION=\"4.4.1\""
  "TIXML_USE_STL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "ThirdParty/dist/include"
  "/home/adutta/anaconda3/include"
  "../"
  "../library"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions/CMakeFiles/StdRegions.dir/DependInfo.cmake"
  "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities/CMakeFiles/LibUtilities.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
