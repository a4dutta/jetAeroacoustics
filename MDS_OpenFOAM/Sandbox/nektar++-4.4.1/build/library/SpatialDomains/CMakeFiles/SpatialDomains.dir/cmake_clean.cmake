file(REMOVE_RECURSE
  "CMakeFiles/SpatialDomains.dir/Conditions.cpp.o"
  "CMakeFiles/SpatialDomains.dir/GeomFactors.cpp.o"
  "CMakeFiles/SpatialDomains.dir/Geometry0D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/Geometry1D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/Geometry2D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/Geometry3D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/Geometry.cpp.o"
  "CMakeFiles/SpatialDomains.dir/HexGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/InterfaceComponent.cpp.o"
  "CMakeFiles/SpatialDomains.dir/MeshComponents.cpp.o"
  "CMakeFiles/SpatialDomains.dir/MeshGraph.cpp.o"
  "CMakeFiles/SpatialDomains.dir/MeshGraph1D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/MeshGraph2D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/MeshGraph3D.cpp.o"
  "CMakeFiles/SpatialDomains.dir/pchSpatialDomains.cpp.o"
  "CMakeFiles/SpatialDomains.dir/PrismGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/PyrGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/QuadGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/SegGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/PointGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/TetGeom.cpp.o"
  "CMakeFiles/SpatialDomains.dir/TriGeom.cpp.o"
  "libSpatialDomains.pdb"
  "libSpatialDomains.so"
  "libSpatialDomains.so.4.4.1"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/SpatialDomains.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
