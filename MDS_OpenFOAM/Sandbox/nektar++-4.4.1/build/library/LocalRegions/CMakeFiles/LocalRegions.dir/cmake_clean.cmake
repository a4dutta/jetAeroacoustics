file(REMOVE_RECURSE
  "CMakeFiles/LocalRegions.dir/Expansion.cpp.o"
  "CMakeFiles/LocalRegions.dir/Expansion0D.cpp.o"
  "CMakeFiles/LocalRegions.dir/Expansion1D.cpp.o"
  "CMakeFiles/LocalRegions.dir/Expansion2D.cpp.o"
  "CMakeFiles/LocalRegions.dir/Expansion3D.cpp.o"
  "CMakeFiles/LocalRegions.dir/QuadExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/HexExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/MatrixKey.cpp.o"
  "CMakeFiles/LocalRegions.dir/NodalTetExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/NodalTriExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/PointExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/PrismExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/PyrExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/SegExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/TetExp.cpp.o"
  "CMakeFiles/LocalRegions.dir/TriExp.cpp.o"
  "libLocalRegions.pdb"
  "libLocalRegions.so"
  "libLocalRegions.so.4.4.1"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/LocalRegions.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
