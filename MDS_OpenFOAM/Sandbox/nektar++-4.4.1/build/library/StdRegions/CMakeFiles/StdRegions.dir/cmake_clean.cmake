file(REMOVE_RECURSE
  "CMakeFiles/StdRegions.dir/StdExpansion.cpp.o"
  "CMakeFiles/StdRegions.dir/StdExpansion0D.cpp.o"
  "CMakeFiles/StdRegions.dir/StdExpansion1D.cpp.o"
  "CMakeFiles/StdRegions.dir/StdExpansion2D.cpp.o"
  "CMakeFiles/StdRegions.dir/StdExpansion3D.cpp.o"
  "CMakeFiles/StdRegions.dir/StdHexExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdMatrixKey.cpp.o"
  "CMakeFiles/StdRegions.dir/StdNodalPrismExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdNodalTetExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdNodalTriExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdPrismExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdPyrExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdQuadExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdSegExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdPointExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdTetExp.cpp.o"
  "CMakeFiles/StdRegions.dir/StdTriExp.cpp.o"
  "CMakeFiles/StdRegions.dir/IndexMapKey.cpp.o"
  "libStdRegions.pdb"
  "libStdRegions.so"
  "libStdRegions.so.4.4.1"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/StdRegions.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
