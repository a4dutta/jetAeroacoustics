file(REMOVE_RECURSE
  "CMakeFiles/LinearAlgebraUnitTests.dir/main.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestBandedMatrixOperations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestBandedMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestBlockMatrix.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestCanGetRawPtr.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestDgemmOptimizations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestDiagonalMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestDiagonalMatrixOperations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestFullMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestFullMatrixOperations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestLowerTriangularMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestNekVector.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestScaledBlockMatrixOperations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestScaledMatrix.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestSymmetricMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestTriangularMatrixOperations.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestUpperTriangularMatrixStoragePolicy.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/TestStandardMatrix.cpp.o"
  "CMakeFiles/LinearAlgebraUnitTests.dir/__/__/util.cpp.o"
  "LinearAlgebraUnitTests.pdb"
  "LinearAlgebraUnitTests"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/LinearAlgebraUnitTests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
