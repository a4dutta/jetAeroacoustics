file(REMOVE_RECURSE
  "CMakeFiles/UnitTests.dir/main.cpp.o"
  "CMakeFiles/UnitTests.dir/ScaledMatrixUnitTests.cpp.o"
  "CMakeFiles/UnitTests.dir/BlockMatrixUnitTests.cpp.o"
  "CMakeFiles/UnitTests.dir/testBoostUtil.cpp.o"
  "CMakeFiles/UnitTests.dir/testExpressionTemplates.cpp.o"
  "CMakeFiles/UnitTests.dir/testLinearSystem.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekLinAlgAlgorithms.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekManager.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekMatrix.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekPoint.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekVector.cpp.o"
  "CMakeFiles/UnitTests.dir/testNekSharedArray.cpp.o"
  "CMakeFiles/UnitTests.dir/Memory/TestNekMemoryManager.cpp.o"
  "CMakeFiles/UnitTests.dir/StdRegions/testStdSegExp.cpp.o"
  "CMakeFiles/UnitTests.dir/testFoundation/testFoundation.cpp.o"
  "CMakeFiles/UnitTests.dir/testFoundation/testInterpolation.cpp.o"
  "CMakeFiles/UnitTests.dir/testFoundation/testDerivation.cpp.o"
  "CMakeFiles/UnitTests.dir/util.cpp.o"
  "UnitTests.pdb"
  "UnitTests"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/UnitTests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
