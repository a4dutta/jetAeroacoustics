# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/library/UnitTests/Collections
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/UnitTests/Collections
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Collections "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/UnitTests/Collections/CollectionsUnitTests" "--detect_memory_leaks=0")
