# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build

# Include any dependencies generated for this target.
include tests/CMakeFiles/Tester.dir/depend.make

# Include the progress variables for this target.
include tests/CMakeFiles/Tester.dir/progress.make

# Include the compile flags for this target's objects.
include tests/CMakeFiles/Tester.dir/flags.make

tests/CMakeFiles/Tester.dir/Metric.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/Metric.cpp.o: ../tests/Metric.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object tests/CMakeFiles/Tester.dir/Metric.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/Metric.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/Metric.cpp

tests/CMakeFiles/Tester.dir/Metric.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/Metric.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/Metric.cpp > CMakeFiles/Tester.dir/Metric.cpp.i

tests/CMakeFiles/Tester.dir/Metric.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/Metric.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/Metric.cpp -o CMakeFiles/Tester.dir/Metric.cpp.s

tests/CMakeFiles/Tester.dir/Metric.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/Metric.cpp.o.requires

tests/CMakeFiles/Tester.dir/Metric.cpp.o.provides: tests/CMakeFiles/Tester.dir/Metric.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/Metric.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/Metric.cpp.o.provides

tests/CMakeFiles/Tester.dir/Metric.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/Metric.cpp.o


tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o: ../tests/MetricEigenvalue.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricEigenvalue.cpp

tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricEigenvalue.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricEigenvalue.cpp > CMakeFiles/Tester.dir/MetricEigenvalue.cpp.i

tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricEigenvalue.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricEigenvalue.cpp -o CMakeFiles/Tester.dir/MetricEigenvalue.cpp.s

tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o


tests/CMakeFiles/Tester.dir/MetricFile.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricFile.cpp.o: ../tests/MetricFile.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object tests/CMakeFiles/Tester.dir/MetricFile.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricFile.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricFile.cpp

tests/CMakeFiles/Tester.dir/MetricFile.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricFile.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricFile.cpp > CMakeFiles/Tester.dir/MetricFile.cpp.i

tests/CMakeFiles/Tester.dir/MetricFile.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricFile.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricFile.cpp -o CMakeFiles/Tester.dir/MetricFile.cpp.s

tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricFile.cpp.o


tests/CMakeFiles/Tester.dir/MetricL2.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricL2.cpp.o: ../tests/MetricL2.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Building CXX object tests/CMakeFiles/Tester.dir/MetricL2.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricL2.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricL2.cpp

tests/CMakeFiles/Tester.dir/MetricL2.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricL2.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricL2.cpp > CMakeFiles/Tester.dir/MetricL2.cpp.i

tests/CMakeFiles/Tester.dir/MetricL2.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricL2.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricL2.cpp -o CMakeFiles/Tester.dir/MetricL2.cpp.s

tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricL2.cpp.o


tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o: ../tests/MetricLInf.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_5) "Building CXX object tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricLInf.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricLInf.cpp

tests/CMakeFiles/Tester.dir/MetricLInf.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricLInf.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricLInf.cpp > CMakeFiles/Tester.dir/MetricLInf.cpp.i

tests/CMakeFiles/Tester.dir/MetricLInf.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricLInf.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricLInf.cpp -o CMakeFiles/Tester.dir/MetricLInf.cpp.s

tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o


tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o: ../tests/MetricPrecon.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_6) "Building CXX object tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricPrecon.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricPrecon.cpp

tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricPrecon.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricPrecon.cpp > CMakeFiles/Tester.dir/MetricPrecon.cpp.i

tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricPrecon.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricPrecon.cpp -o CMakeFiles/Tester.dir/MetricPrecon.cpp.s

tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o


tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o: ../tests/MetricRegex.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_7) "Building CXX object tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/MetricRegex.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricRegex.cpp

tests/CMakeFiles/Tester.dir/MetricRegex.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/MetricRegex.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricRegex.cpp > CMakeFiles/Tester.dir/MetricRegex.cpp.i

tests/CMakeFiles/Tester.dir/MetricRegex.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/MetricRegex.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/MetricRegex.cpp -o CMakeFiles/Tester.dir/MetricRegex.cpp.s

tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.requires

tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.provides: tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.provides

tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o


tests/CMakeFiles/Tester.dir/TestData.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/TestData.cpp.o: ../tests/TestData.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_8) "Building CXX object tests/CMakeFiles/Tester.dir/TestData.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/TestData.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/TestData.cpp

tests/CMakeFiles/Tester.dir/TestData.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/TestData.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/TestData.cpp > CMakeFiles/Tester.dir/TestData.cpp.i

tests/CMakeFiles/Tester.dir/TestData.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/TestData.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/TestData.cpp -o CMakeFiles/Tester.dir/TestData.cpp.s

tests/CMakeFiles/Tester.dir/TestData.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/TestData.cpp.o.requires

tests/CMakeFiles/Tester.dir/TestData.cpp.o.provides: tests/CMakeFiles/Tester.dir/TestData.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/TestData.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/TestData.cpp.o.provides

tests/CMakeFiles/Tester.dir/TestData.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/TestData.cpp.o


tests/CMakeFiles/Tester.dir/Tester.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/Tester.cpp.o: tests/Tester.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_9) "Building CXX object tests/CMakeFiles/Tester.dir/Tester.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/Tester.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester.cpp

tests/CMakeFiles/Tester.dir/Tester.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/Tester.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester.cpp > CMakeFiles/Tester.dir/Tester.cpp.i

tests/CMakeFiles/Tester.dir/Tester.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/Tester.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester.cpp -o CMakeFiles/Tester.dir/Tester.cpp.s

tests/CMakeFiles/Tester.dir/Tester.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/Tester.cpp.o.requires

tests/CMakeFiles/Tester.dir/Tester.cpp.o.provides: tests/CMakeFiles/Tester.dir/Tester.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/Tester.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/Tester.cpp.o.provides

tests/CMakeFiles/Tester.dir/Tester.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/Tester.cpp.o


tests/CMakeFiles/Tester.dir/sha1.cpp.o: tests/CMakeFiles/Tester.dir/flags.make
tests/CMakeFiles/Tester.dir/sha1.cpp.o: ../tests/sha1.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_10) "Building CXX object tests/CMakeFiles/Tester.dir/sha1.cpp.o"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/Tester.dir/sha1.cpp.o -c /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/sha1.cpp

tests/CMakeFiles/Tester.dir/sha1.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/Tester.dir/sha1.cpp.i"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/sha1.cpp > CMakeFiles/Tester.dir/sha1.cpp.i

tests/CMakeFiles/Tester.dir/sha1.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/Tester.dir/sha1.cpp.s"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && /home/adutta/anaconda3/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests/sha1.cpp -o CMakeFiles/Tester.dir/sha1.cpp.s

tests/CMakeFiles/Tester.dir/sha1.cpp.o.requires:

.PHONY : tests/CMakeFiles/Tester.dir/sha1.cpp.o.requires

tests/CMakeFiles/Tester.dir/sha1.cpp.o.provides: tests/CMakeFiles/Tester.dir/sha1.cpp.o.requires
	$(MAKE) -f tests/CMakeFiles/Tester.dir/build.make tests/CMakeFiles/Tester.dir/sha1.cpp.o.provides.build
.PHONY : tests/CMakeFiles/Tester.dir/sha1.cpp.o.provides

tests/CMakeFiles/Tester.dir/sha1.cpp.o.provides.build: tests/CMakeFiles/Tester.dir/sha1.cpp.o


# Object files for target Tester
Tester_OBJECTS = \
"CMakeFiles/Tester.dir/Metric.cpp.o" \
"CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o" \
"CMakeFiles/Tester.dir/MetricFile.cpp.o" \
"CMakeFiles/Tester.dir/MetricL2.cpp.o" \
"CMakeFiles/Tester.dir/MetricLInf.cpp.o" \
"CMakeFiles/Tester.dir/MetricPrecon.cpp.o" \
"CMakeFiles/Tester.dir/MetricRegex.cpp.o" \
"CMakeFiles/Tester.dir/TestData.cpp.o" \
"CMakeFiles/Tester.dir/Tester.cpp.o" \
"CMakeFiles/Tester.dir/sha1.cpp.o"

# External object files for target Tester
Tester_EXTERNAL_OBJECTS =

tests/Tester: tests/CMakeFiles/Tester.dir/Metric.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricFile.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricL2.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/TestData.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/Tester.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/sha1.cpp.o
tests/Tester: tests/CMakeFiles/Tester.dir/build.make
tests/Tester: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
tests/Tester: /usr/lib/x86_64-linux-gnu/libboost_system.so
tests/Tester: /usr/lib/x86_64-linux-gnu/libboost_regex.so
tests/Tester: /usr/lib/x86_64-linux-gnu/libboost_program_options.so
tests/Tester: /usr/lib/x86_64-linux-gnu/libboost_thread.so
tests/Tester: tests/CMakeFiles/Tester.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_11) "Linking CXX executable Tester"
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/Tester.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
tests/CMakeFiles/Tester.dir/build: tests/Tester

.PHONY : tests/CMakeFiles/Tester.dir/build

tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/Metric.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricEigenvalue.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricFile.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricL2.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricLInf.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricPrecon.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/MetricRegex.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/TestData.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/Tester.cpp.o.requires
tests/CMakeFiles/Tester.dir/requires: tests/CMakeFiles/Tester.dir/sha1.cpp.o.requires

.PHONY : tests/CMakeFiles/Tester.dir/requires

tests/CMakeFiles/Tester.dir/clean:
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests && $(CMAKE_COMMAND) -P CMakeFiles/Tester.dir/cmake_clean.cmake
.PHONY : tests/CMakeFiles/Tester.dir/clean

tests/CMakeFiles/Tester.dir/depend:
	cd /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1 /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/tests /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/CMakeFiles/Tester.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : tests/CMakeFiles/Tester.dir/depend

