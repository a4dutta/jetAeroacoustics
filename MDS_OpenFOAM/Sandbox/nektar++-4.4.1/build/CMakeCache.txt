# This is the CMakeCache file.
# For build in directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build
# It was generated by CMake: /usr/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Path to a program.
BIBTEX:FILEPATH=/usr/bin/bibtex

//The threading library used by boost-thread
BOOST_THREAD_LIBRARY:FILEPATH=/usr/lib/x86_64-linux-gnu/libpthread.so

//Boost atomic library (debug)
Boost_ATOMIC_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_atomic.so

//Boost atomic library (release)
Boost_ATOMIC_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_atomic.so

//Boost chrono library (debug)
Boost_CHRONO_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_chrono.so

//Boost chrono library (release)
Boost_CHRONO_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_chrono.so

//Boost date_time library (debug)
Boost_DATE_TIME_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_date_time.so

//Boost date_time library (release)
Boost_DATE_TIME_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_date_time.so

//Boost filesystem library (debug)
Boost_FILESYSTEM_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_filesystem.so

//Boost filesystem library (release)
Boost_FILESYSTEM_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_filesystem.so

//Path to a file.
Boost_INCLUDE_DIR:PATH=/usr/include

//Boost iostreams library (debug)
Boost_IOSTREAMS_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_iostreams.so

//Boost iostreams library (release)
Boost_IOSTREAMS_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_iostreams.so

//Boost library directory DEBUG
Boost_LIBRARY_DIR_DEBUG:PATH=/usr/lib/x86_64-linux-gnu

//Boost library directory RELEASE
Boost_LIBRARY_DIR_RELEASE:PATH=/usr/lib/x86_64-linux-gnu

//Boost program_options library (debug)
Boost_PROGRAM_OPTIONS_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_program_options.so

//Boost program_options library (release)
Boost_PROGRAM_OPTIONS_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_program_options.so

//Boost regex library (debug)
Boost_REGEX_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_regex.so

//Boost regex library (release)
Boost_REGEX_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_regex.so

//Boost system library (debug)
Boost_SYSTEM_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_system.so

//Boost system library (release)
Boost_SYSTEM_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_system.so

//Boost thread library (debug)
Boost_THREAD_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_thread.so

//Boost thread library (release)
Boost_THREAD_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_thread.so

//Boost timer library (debug)
Boost_TIMER_LIBRARY_DEBUG:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_timer.so

//Boost timer library (release)
Boost_TIMER_LIBRARY_RELEASE:FILEPATH=/usr/lib/x86_64-linux-gnu/libboost_timer.so

//Search for multithreaded boost libraries
Boost_USE_MULTITHREADED:BOOL=ON

//Path to a program.
CMAKE_AR:FILEPATH=/home/adutta/anaconda3/bin/ar

//Choose the type of build,
//\noptions are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug
// Release
//\nRelWithDebInfo MinSizeRel.
CMAKE_BUILD_TYPE:STRING=Release

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/home/adutta/anaconda3/bin/c++

//Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//C compiler
CMAKE_C_COMPILER:FILEPATH=/home/adutta/anaconda3/bin/cc

//Flags used by the compiler during all build types.
CMAKE_C_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during release builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during release builds with debug info.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

//Flags used by the linker.
CMAKE_EXE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Fortran compiler
CMAKE_Fortran_COMPILER:FILEPATH=/usr/bin/f95

//Flags for Fortran compiler.
CMAKE_Fortran_FLAGS:STRING=

//Flags used by the compiler during debug builds.
CMAKE_Fortran_FLAGS_DEBUG:STRING=-g

//Flags used by the compiler during release builds for minimum
// size.
CMAKE_Fortran_FLAGS_MINSIZEREL:STRING=-Os

//Flags used by the compiler during release builds.
CMAKE_Fortran_FLAGS_RELEASE:STRING=-O3

//Flags used by the compiler during release builds with debug info.
CMAKE_Fortran_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

CMAKE_INSTALL_PREFIX:PATH=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist

//Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/make

//Flags used by the linker during the creation of modules.
CMAKE_MODULE_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=Nektar++

//Path to a program.
CMAKE_RANLIB:FILEPATH=/home/adutta/anaconda3/bin/ranlib

//Flags used by the linker during the creation of dll's.
CMAKE_SHARED_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during debug builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during release minsize builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during release builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during Release with Debug Info builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/home/adutta/anaconda3/bin/strip

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//Path to a program.
CONVERT:FILEPATH=/usr/bin/convert

//Enable to build Debian packages
CPACK_BINARY_DEB:BOOL=OFF

//Enable to build IFW packages
CPACK_BINARY_IFW:BOOL=OFF

//Enable to build NSIS packages
CPACK_BINARY_NSIS:BOOL=OFF

//Enable to build RPM packages
CPACK_BINARY_RPM:BOOL=OFF

//Enable to build STGZ packages
CPACK_BINARY_STGZ:BOOL=ON

//Enable to build TBZ2 packages
CPACK_BINARY_TBZ2:BOOL=OFF

//Enable to build TGZ packages
CPACK_BINARY_TGZ:BOOL=ON

//Enable to build TXZ packages
CPACK_BINARY_TXZ:BOOL=OFF

//Enable to build TZ packages
CPACK_BINARY_TZ:BOOL=ON

//Enable to build TBZ2 source packages
CPACK_SOURCE_TBZ2:BOOL=ON

//Enable to build TGZ source packages
CPACK_SOURCE_TGZ:BOOL=ON

//Enable to build TXZ source packages
CPACK_SOURCE_TXZ:BOOL=ON

//Enable to build TZ source packages
CPACK_SOURCE_TZ:BOOL=ON

//Enable to build ZIP source packages
CPACK_SOURCE_ZIP:BOOL=OFF

//Dependencies for the target
Collections_LIB_DEPENDS:STATIC=general;LocalRegions;

//Dependencies for the target
FieldUtils_LIB_DEPENDS:STATIC=general;GlobalMapping;

//Dependencies for the target
GlobalMapping_LIB_DEPENDS:STATIC=general;MultiRegions;

//Path to a program.
HTLATEX:FILEPATH=/usr/bin/htlatex

LOKI_INCLUDE_DIR:PATH=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/include

//Dependencies for the target
LibUtilities_LIB_DEPENDS:STATIC=general;metis;general;/usr/lib/x86_64-linux-gnu/libboost_thread.so;general;/usr/lib/x86_64-linux-gnu/libboost_iostreams.so;general;/usr/lib/x86_64-linux-gnu/libboost_date_time.so;general;/usr/lib/x86_64-linux-gnu/libboost_program_options.so;general;/usr/lib/x86_64-linux-gnu/libboost_filesystem.so;general;/usr/lib/x86_64-linux-gnu/libboost_system.so;general;/usr/lib/x86_64-linux-gnu/libboost_timer.so;general;/usr/lib/x86_64-linux-gnu/libboost_chrono.so;debug;/home/adutta/anaconda3/lib/libz.so;optimized;/home/adutta/anaconda3/lib/libz.so;general;tinyxml;general;rt;general;/usr/lib/liblapack.so;general;/usr/lib/libblas.so;

//Dependencies for the target
LocalRegions_LIB_DEPENDS:STATIC=general;SpatialDomains;general;StdRegions;general;LibUtilities;

//Path to a program.
MAKEINDEX:FILEPATH=/usr/bin/makeindex

//METIS library
METIS_LIB:FILEPATH=metis

//Dependencies for the target
MultiRegions_LIB_DEPENDS:STATIC=general;Collections;general;metis;

//Path to a library.
NATIVE_BLAS:FILEPATH=/usr/lib/libblas.so

//Path to a library.
NATIVE_LAPACK:FILEPATH=/usr/lib/liblapack.so

//Build demonstration codes.
NEKTAR_BUILD_DEMOS:BOOL=ON

//Build source code documentation using doxygen
NEKTAR_BUILD_DOC:BOOL=OFF

//Build main Nektar++ libraries.
NEKTAR_BUILD_LIBRARY:BOOL=ON

//Build Nektar++ binary packages
NEKTAR_BUILD_PACKAGES:BOOL=OFF

//Build example solvers.
NEKTAR_BUILD_SOLVERS:BOOL=ON

//Build regression tests.
NEKTAR_BUILD_TESTS:BOOL=ON

//Build benchmark timing codes.
NEKTAR_BUILD_TIMINGS:BOOL=OFF

//Build unit tests.
NEKTAR_BUILD_UNIT_TESTS:BOOL=ON

//Build utilities.
NEKTAR_BUILD_UTILITIES:BOOL=ON

//Uses -pg compiler flag
NEKTAR_ENABLE_PROFILE:BOOL=OFF

//Enable Full Debugging.
NEKTAR_FULL_DEBUG:BOOL=OFF

//Build the ADR Solver.
NEKTAR_SOLVER_ADR:BOOL=ON

//Build the APE solver.
NEKTAR_SOLVER_APE:BOOL=ON

//Build the Cardiac electrophysiology solver.
NEKTAR_SOLVER_CARDIAC_EP:BOOL=ON

//Build the Compressible Flow Solver.
NEKTAR_SOLVER_COMPRESSIBLE_FLOW:BOOL=ON

//Build the Diffusion Solver.
NEKTAR_SOLVER_DIFFUSION:BOOL=ON

//Build the linear elasticity solver.
NEKTAR_SOLVER_ELASTICITY:BOOL=ON

//Build the Incompressible Navier-Stokes solver.
NEKTAR_SOLVER_INCNAVIERSTOKES:BOOL=ON

//Build the Pulse-wave solver.
NEKTAR_SOLVER_PULSEWAVE:BOOL=ON

//Build the Shallow Water solver.
NEKTAR_SOLVER_SHALLOW_WATER:BOOL=ON

//Include full set of regression tests to this build.
NEKTAR_TEST_ALL:BOOL=OFF

//Use a hostfile to explicitly specify number of
//\nslots.
NEKTAR_TEST_USE_HOSTFILE:BOOL=OFF

//Use the AMD Core Math Library (ACML) for BLAS and Lapack support.
NEKTAR_USE_ACML:BOOL=OFF

//Use Arpack routines for evaluating eigenvalues and eigenvectors
NEKTAR_USE_ARPACK:BOOL=OFF

//Use Blas and lapack routines.
NEKTAR_USE_BLAS_LAPACK:BOOL=ON

//CCM star i/o library is available.
NEKTAR_USE_CCM:BOOL=OFF

//Use Expression templates.
NEKTAR_USE_EXPRESSION_TEMPLATES:BOOL=OFF

//Use FFTW routines for performing the Fast Fourier Transform.
NEKTAR_USE_FFTW:BOOL=OFF

//Enable HDF5 I/O support.
NEKTAR_USE_HDF5:BOOL=OFF

//Use memory pools to accelerate memory allocation.
NEKTAR_USE_MEMORY_POOLS:BOOL=ON

//Build mesh generation utilities.
NEKTAR_USE_MESHGEN:BOOL=OFF

//Use the Intel Math Kernel Library (MKL) for BLAS and Lapack support.
NEKTAR_USE_MKL:BOOL=OFF

//Use MPI for parallelisation.
NEKTAR_USE_MPI:BOOL=OFF

//Use OpenBLAS library as a substitute to native BLAS.
NEKTAR_USE_OPENBLAS:BOOL=OFF

//Enable PETSc parallel matrix solver support.
NEKTAR_USE_PETSC:BOOL=OFF

//Use Scotch library for performing mesh partitioning.
NEKTAR_USE_SCOTCH:BOOL=OFF

//Use LibSMV library for faster small matrix-vector multiplies.
NEKTAR_USE_SMV:BOOL=OFF

//Use the system provided blas and lapack libraries
NEKTAR_USE_SYSTEM_BLAS_LAPACK:BOOL=ON

//Use STL with TinyXML library.
NEKTAR_USE_TINYXML_STL:BOOL=ON

//Use VTK library for utilities.
NEKTAR_USE_VTK:BOOL=OFF

//Dependencies for the target
NekMeshUtils_LIB_DEPENDS:STATIC=general;SpatialDomains;

//Value Computed by CMake
Nektar++_BINARY_DIR:STATIC=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build

//Value Computed by CMake
Nektar++_SOURCE_DIR:STATIC=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1

//Path to a program.
PDFLATEX:FILEPATH=/usr/bin/pdflatex

//Dependencies for the target
SolverUtils_LIB_DEPENDS:STATIC=general;FieldUtils;

//Dependencies for the target
SpatialDomains_LIB_DEPENDS:STATIC=general;StdRegions;general;LibUtilities;

//Dependencies for the target
StdRegions_LIB_DEPENDS:STATIC=general;LibUtilities;

//Build blas and lapack libraries from ThirdParty.
THIRDPARTY_BUILD_BLAS_LAPACK:BOOL=OFF

//Build Boost libraries
THIRDPARTY_BUILD_BOOST:BOOL=OFF

//Download and extract Loki library to ThirdParty.
THIRDPARTY_BUILD_LOKI:BOOL=ON

//Build LibSMV
THIRDPARTY_BUILD_SMV:BOOL=OFF

//Build TinyXML library from ThirdParty.
THIRDPARTY_BUILD_TINYXML:BOOL=ON

//Build ZLib library
THIRDPARTY_BUILD_ZLIB:BOOL=OFF

//Use secure HTTP connection to download third-party files.
THIRDPARTY_USE_SSL:BOOL=OFF

//TinyXML include
TINYXML_INCLUDE_DIR:FILEPATH=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/include

//TinyXML library
TINYXML_LIBRARY:FILEPATH=tinyxml

//Zlib include
ZLIB_INCLUDE_DIR:PATH=/home/adutta/anaconda3/include

//Zlib library
ZLIB_LIBRARY:FILEPATH=/home/adutta/anaconda3/lib/libz.so

//Zlib library
ZLIB_LIBRARY_DEBUG:FILEPATH=/home/adutta/anaconda3/lib/libz.so

//Zlib library
ZLIB_LIBRARY_RELEASE:FILEPATH=/home/adutta/anaconda3/lib/libz.so


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: BIBTEX
BIBTEX-ADVANCED:INTERNAL=1
//ADVANCED property for variable: BOOST_THREAD_LIBRARY
BOOST_THREAD_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_ATOMIC_LIBRARY_DEBUG
Boost_ATOMIC_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_ATOMIC_LIBRARY_RELEASE
Boost_ATOMIC_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_CHRONO_LIBRARY_DEBUG
Boost_CHRONO_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_CHRONO_LIBRARY_RELEASE
Boost_CHRONO_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_DATE_TIME_LIBRARY_DEBUG
Boost_DATE_TIME_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_DATE_TIME_LIBRARY_RELEASE
Boost_DATE_TIME_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_FILESYSTEM_LIBRARY_DEBUG
Boost_FILESYSTEM_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_FILESYSTEM_LIBRARY_RELEASE
Boost_FILESYSTEM_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_INCLUDE_DIR
Boost_INCLUDE_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_IOSTREAMS_LIBRARY_DEBUG
Boost_IOSTREAMS_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_IOSTREAMS_LIBRARY_RELEASE
Boost_IOSTREAMS_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_LIBRARY_DIR_DEBUG
Boost_LIBRARY_DIR_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_LIBRARY_DIR_RELEASE
Boost_LIBRARY_DIR_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_PROGRAM_OPTIONS_LIBRARY_DEBUG
Boost_PROGRAM_OPTIONS_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_PROGRAM_OPTIONS_LIBRARY_RELEASE
Boost_PROGRAM_OPTIONS_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_REGEX_LIBRARY_DEBUG
Boost_REGEX_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_REGEX_LIBRARY_RELEASE
Boost_REGEX_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_SYSTEM_LIBRARY_DEBUG
Boost_SYSTEM_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_SYSTEM_LIBRARY_RELEASE
Boost_SYSTEM_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_THREAD_LIBRARY_DEBUG
Boost_THREAD_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_THREAD_LIBRARY_RELEASE
Boost_THREAD_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_TIMER_LIBRARY_DEBUG
Boost_TIMER_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_TIMER_LIBRARY_RELEASE
Boost_TIMER_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: Boost_USE_MULTITHREADED
Boost_USE_MULTITHREADED-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=5
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=1
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/usr/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/usr/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/usr/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/usr/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//ADVANCED property for variable: CMAKE_Fortran_COMPILER
CMAKE_Fortran_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS
CMAKE_Fortran_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_DEBUG
CMAKE_Fortran_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_MINSIZEREL
CMAKE_Fortran_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELEASE
CMAKE_Fortran_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELWITHDEBINFO
CMAKE_Fortran_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Have symbol pthread_create
CMAKE_HAVE_LIBC_CREATE:INTERNAL=
//Have library pthreads
CMAKE_HAVE_PTHREADS_CREATE:INTERNAL=
//Have library pthread
CMAKE_HAVE_PTHREAD_CREATE:INTERNAL=1
//Have include pthread.h
CMAKE_HAVE_PTHREAD_H:INTERNAL=1
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=1
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=47
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/usr/share/cmake-3.5
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_DEB
CPACK_BINARY_DEB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_IFW
CPACK_BINARY_IFW-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_NSIS
CPACK_BINARY_NSIS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_RPM
CPACK_BINARY_RPM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_STGZ
CPACK_BINARY_STGZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_TBZ2
CPACK_BINARY_TBZ2-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_TGZ
CPACK_BINARY_TGZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_TXZ
CPACK_BINARY_TXZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_BINARY_TZ
CPACK_BINARY_TZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_SOURCE_TBZ2
CPACK_SOURCE_TBZ2-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_SOURCE_TGZ
CPACK_SOURCE_TGZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_SOURCE_TXZ
CPACK_SOURCE_TXZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_SOURCE_TZ
CPACK_SOURCE_TZ-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CPACK_SOURCE_ZIP
CPACK_SOURCE_ZIP-ADVANCED:INTERNAL=1
//ADVANCED property for variable: HTLATEX
HTLATEX-ADVANCED:INTERNAL=1
//ADVANCED property for variable: LOKI_INCLUDE_DIR
LOKI_INCLUDE_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: MAKEINDEX
MAKEINDEX-ADVANCED:INTERNAL=1
//ADVANCED property for variable: METIS_LIB
METIS_LIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NATIVE_BLAS
NATIVE_BLAS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NATIVE_LAPACK
NATIVE_LAPACK-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_BUILD_PACKAGES
NEKTAR_BUILD_PACKAGES-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_ENABLE_PROFILE
NEKTAR_ENABLE_PROFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_FULL_DEBUG
NEKTAR_FULL_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_USE_EXPRESSION_TEMPLATES
NEKTAR_USE_EXPRESSION_TEMPLATES-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_USE_MEMORY_POOLS
NEKTAR_USE_MEMORY_POOLS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: NEKTAR_USE_TINYXML_STL
NEKTAR_USE_TINYXML_STL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PDFLATEX
PDFLATEX-ADVANCED:INTERNAL=1
//ADVANCED property for variable: TINYXML_INCLUDE_DIR
TINYXML_INCLUDE_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: TINYXML_LIBRARY
TINYXML_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ZLIB_INCLUDE_DIR
ZLIB_INCLUDE_DIR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ZLIB_LIBRARY
ZLIB_LIBRARY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ZLIB_LIBRARY_DEBUG
ZLIB_LIBRARY_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ZLIB_LIBRARY_RELEASE
ZLIB_LIBRARY_RELEASE-ADVANCED:INTERNAL=1
//Last used BOOST_ROOT value.
_BOOST_ROOT_LAST:INTERNAL=/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist
//Components requested for this build tree.
_Boost_COMPONENTS_SEARCHED:INTERNAL=atomic;chrono;date_time;filesystem;iostreams;program_options;regex;system;thread;timer
//Last used Boost_INCLUDE_DIR value.
_Boost_INCLUDE_DIR_LAST:INTERNAL=/usr/include
//Last used Boost_LIBRARY_DIR_DEBUG value.
_Boost_LIBRARY_DIR_DEBUG_LAST:INTERNAL=/usr/lib/x86_64-linux-gnu
//Last used Boost_LIBRARY_DIR_RELEASE value.
_Boost_LIBRARY_DIR_RELEASE_LAST:INTERNAL=/usr/lib/x86_64-linux-gnu
//Last used Boost_NAMESPACE value.
_Boost_NAMESPACE_LAST:INTERNAL=boost
//Last used Boost_USE_MULTITHREADED value.
_Boost_USE_MULTITHREADED_LAST:INTERNAL=ON

