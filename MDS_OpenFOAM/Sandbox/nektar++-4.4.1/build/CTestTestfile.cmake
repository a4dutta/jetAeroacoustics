# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(library)
subdirs(solvers)
subdirs(utilities)
subdirs(tests)
subdirs(docs)
