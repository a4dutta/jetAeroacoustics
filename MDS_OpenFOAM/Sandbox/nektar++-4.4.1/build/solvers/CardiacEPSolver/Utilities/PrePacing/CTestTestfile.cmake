# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/CardiacEPSolver/Utilities/PrePacing
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/solvers/CardiacEPSolver/Utilities/PrePacing
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(PrePacing_Courtemanche "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/CardiacEPSolver/Utilities/PrePacing/Tests/Courtemanche.tst")
add_test(PrePacing_CourtemancheAF "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/CardiacEPSolver/Utilities/PrePacing/Tests/CourtemancheAF.tst")
add_test(PrePacing_FentonKarma "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/CardiacEPSolver/Utilities/PrePacing/Tests/FentonKarma.tst")
