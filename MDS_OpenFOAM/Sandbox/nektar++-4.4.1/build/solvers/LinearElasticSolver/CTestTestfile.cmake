# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/LinearElasticSolver
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/solvers/LinearElasticSolver
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(LinearElasticSolver_L-domain "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/LinearElasticSolver/Tests/L-domain.tst")
