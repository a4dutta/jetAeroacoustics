file(REMOVE_RECURSE
  "CMakeFiles/ShallowWaterSolver.dir/ShallowWaterSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/EquationSystems/ShallowWaterSystem.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/EquationSystems/LinearSWE.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/EquationSystems/NonlinearSWE.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/EquationSystems/NonlinearPeregrine.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/LinearSWESolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/NonlinearSWESolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/NoSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/LinearAverageSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/LinearHLLSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/AverageSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/LaxFriedrichsSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/HLLSolver.cpp.o"
  "CMakeFiles/ShallowWaterSolver.dir/RiemannSolvers/HLLCSolver.cpp.o"
  "ShallowWaterSolver.pdb"
  "ShallowWaterSolver"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/ShallowWaterSolver.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
