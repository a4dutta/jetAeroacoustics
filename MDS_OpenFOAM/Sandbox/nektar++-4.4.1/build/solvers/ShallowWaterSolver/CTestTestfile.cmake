# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/solvers/ShallowWaterSolver
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(ShallowWaterSolver_LinearSWE_StandingWave_WallBC_CG_P4 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_WallBC_CG_P4.tst")
add_test(ShallowWaterSolver_LinearSWE_StandingWave_WallBC_CG_P8 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_WallBC_CG_P8.tst")
add_test(ShallowWaterSolver_LinearSWE_StandingWave_PeriodicBC_CG_P8 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_PeriodicBC_CG_P8.tst")
add_test(ShallowWaterSolver_LinearSWE_StandingWave_WallBC_DG_P4 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_WallBC_DG_P4.tst")
add_test(ShallowWaterSolver_LinearSWE_StandingWave_WallBC_DG_P8 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_WallBC_DG_P8.tst")
add_test(ShallowWaterSolver_LinearSWE_StandingWave_PeriodicBC_DG_P8 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/LinearSWE_StandingWave_PeriodicBC_DG_P8.tst")
add_test(ShallowWaterSolver_NonlinearSWE_RossbyModon_CG_P9 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/NonlinearSWE_RossbyModon_CG_P9.tst")
add_test(ShallowWaterSolver_NonlinearSWE_RossbyModon_DG_P9 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/ShallowWaterSolver/Tests/NonlinearSWE_RossbyModon_DG_P9.tst")
