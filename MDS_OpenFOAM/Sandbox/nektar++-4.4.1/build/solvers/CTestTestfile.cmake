# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/solvers
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(LinearElasticSolver)
subdirs(PulseWaveSolver)
subdirs(APESolver)
subdirs(ShallowWaterSolver)
subdirs(CardiacEPSolver)
subdirs(CompressibleFlowSolver)
subdirs(ADRSolver)
subdirs(DiffusionSolver)
subdirs(IncNavierStokesSolver)
