file(REMOVE_RECURSE
  "CMakeFiles/ADRSolver.dir/ADRSolver.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/Helmholtz.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/Laplace.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/Poisson.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/Projection.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/SteadyAdvectionDiffusion.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/SteadyAdvectionDiffusionReaction.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/UnsteadyAdvection.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/UnsteadyDiffusion.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/UnsteadyAdvectionDiffusion.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/UnsteadyInviscidBurger.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/UnsteadyViscousBurgers.cpp.o"
  "CMakeFiles/ADRSolver.dir/EquationSystems/EigenValuesAdvection.cpp.o"
  "ADRSolver.pdb"
  "ADRSolver"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/ADRSolver.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
