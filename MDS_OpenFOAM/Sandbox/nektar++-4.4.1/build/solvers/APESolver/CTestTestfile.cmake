# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/solvers/APESolver
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(APESolver_APE_2DChannel_WeakDG_MODIFIED "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver/Tests/APE_2DChannel_WeakDG_MODIFIED.tst")
add_test(APESolver_APE_2DPulseAdv_WeakDG_MODIFIED "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver/Tests/APE_2DPulseAdv_WeakDG_MODIFIED.tst")
add_test(APESolver_APE_2DPulseInterp_WeakDG_MODIFIED "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver/Tests/APE_2DPulseInterp_WeakDG_MODIFIED.tst")
add_test(APESolver_APE_2DPulseWall_WeakDG_MODIFIED "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver/Tests/APE_2DPulseWall_WeakDG_MODIFIED.tst")
add_test(APESolver_APE_2DVariableC_WeakDG_MODIFIED "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/solvers/APESolver/Tests/APE_2DVariableC_WeakDG_MODIFIED.tst")
