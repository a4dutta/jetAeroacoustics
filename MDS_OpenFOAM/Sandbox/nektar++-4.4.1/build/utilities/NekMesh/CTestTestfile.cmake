# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/NekMesh
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(NekMesh_Nektar++/InvalidTetFace "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar++/InvalidTetFace.tst")
add_test(NekMesh_Nektar++/InvalidQuads "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar++/InvalidQuads.tst")
add_test(NekMesh_Nektar++/Tube45Refinement "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar++/Tube45Refinement.tst")
add_test(NekMesh_Nektar++/Tube45Refinement_extractsurf "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar++/Tube45Refinement_extractsurf.tst")
add_test(NekMesh_Nektar++/CylQuadBl "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar++/CylQuadBl.tst")
add_test(NekMesh_Gmsh/CubeAllElements "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubeAllElements.tst")
add_test(NekMesh_Gmsh/CubeHex "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubeHex.tst")
add_test(NekMesh_Gmsh/CubeHexLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubeHexLinear.tst")
add_test(NekMesh_Gmsh/CubePrism "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubePrism.tst")
add_test(NekMesh_Gmsh/CubePrismLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubePrismLinear.tst")
add_test(NekMesh_Gmsh/CubeTet "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubeTet.tst")
add_test(NekMesh_Gmsh/CubeTetLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/CubeTetLinear.tst")
add_test(NekMesh_Gmsh/Scalar "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/Scalar.tst")
add_test(NekMesh_Gmsh/SquareQuad "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/SquareQuad.tst")
add_test(NekMesh_Gmsh/SquareQuadLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/SquareQuadLinear.tst")
add_test(NekMesh_Gmsh/SquareTri "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/SquareTri.tst")
add_test(NekMesh_Gmsh/SquareTriLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Gmsh/SquareTriLinear.tst")
add_test(NekMesh_Nektar/HexLinear "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar/HexLinear.tst")
add_test(NekMesh_Nektar/Tube45 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar/Tube45.tst")
add_test(NekMesh_Nektar/UKMesh "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nektar/UKMesh.tst")
add_test(NekMesh_Nek5000/3da "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nek5000/3da.tst")
add_test(NekMesh_Nek5000/r1854a "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/Nek5000/r1854a.tst")
add_test(NekMesh_StarTec/CubePer "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/NekMesh/Tests/StarTec/CubePer.tst")
