# CMake generated Testfile for 
# Source directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert
# Build directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FieldConvert
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(FieldConvert_chan3D_tec_n10 "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_tec_n10.tst")
add_test(FieldConvert_chan3D_interppointsplane "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_interppointsplane.tst")
add_test(FieldConvert_chan3D_interppointsbox "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_interppointsbox.tst")
add_test(FieldConvert_compositeid "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/compositeid.tst")
add_test(FieldConvert_bfs_probe "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/bfs_probe.tst")
add_test(FieldConvert_bfs_tec "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/bfs_tec.tst")
add_test(FieldConvert_bfs_tec_rng "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/bfs_tec_rng.tst")
add_test(FieldConvert_bfs_vort "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/bfs_vort.tst")
add_test(FieldConvert_bfs_vort_rng "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/bfs_vort_rng.tst")
add_test(FieldConvert_pointdatatofld "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/pointdatatofld.tst")
add_test(FieldConvert_chan3DH1D_meanmode "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3DH1D_meanmode.tst")
add_test(FieldConvert_chan3DH1D_plane "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3DH1D_plane.tst")
add_test(FieldConvert_chan3DH1D_stretch "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3DH1D_stretch.tst")
add_test(FieldConvert_chan3D_probe "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_probe.tst")
add_test(FieldConvert_cube_prismhex "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/cube_prismhex.tst")
add_test(FieldConvert_cube_prismhex_range "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/cube_prismhex_range.tst")
add_test(FieldConvert_chan3D_equispacedoutput "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_equispacedoutput.tst")
add_test(FieldConvert_chan3D_isocontour "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_isocontour.tst")
add_test(FieldConvert_interpfield "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/interpfield.tst")
add_test(FieldConvert_naca0012_bnd_equispacedoutput "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/naca0012_bnd_equispacedoutput.tst")
add_test(FieldConvert_smallmesh_isocontour "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/smallmesh_isocontour.tst")
add_test(FieldConvert_chan3D_interppointdatatofld "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/tests/Tester" "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities/FieldConvert/Tests/chan3D_interppointdatatofld.tst")
