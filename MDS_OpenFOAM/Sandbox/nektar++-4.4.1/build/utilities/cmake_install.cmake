# Install script for directory: /home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/utilities

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FldAddFld")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddFld")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FldAddScalGrad")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FldAddScalGrad_elmt")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddScalGrad_elmt")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FldAddWSS")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddWSS")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FldAddMultiShear")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/FldAddMultiShear")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/MeshConvert")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/MeshConvert")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util-extra")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/ProbeFld")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/ProbeFld")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/XmlToTecplot")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplot")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/XmlToTecplotWireFrame")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToTecplotWireFrame")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "util")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk"
         RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/XmlToVtk")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk"
         OLD_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/ThirdParty/dist/lib:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/MultiRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/Collections:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LocalRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/SpatialDomains:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/StdRegions:/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/library/LibUtilities:/home/adutta/anaconda3/lib:"
         NEW_RPATH "/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/dist/lib64/nektar++-4.4.1:/home/adutta/anaconda3/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/home/adutta/anaconda3/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/XmlToVtk")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/FieldConvert/cmake_install.cmake")
  include("/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/NekMesh/cmake_install.cmake")
  include("/home/adutta/Nextcloud/MDS_OpenFOAM/Sandbox/nektar++-4.4.1/build/utilities/SimpleMeshGen/cmake_install.cmake")

endif()

