#!/bin/bash
#
#SBATCH -n 80
#SBATCH --mem-per-cpu 3000
#SBATCH -o slurm.%N.%j.out
#SBATCH -t 5:00:00 
 

# 2014-09-26 DSP - Example batch job script for a parallel OpenFOAM run on Parallel or Lattice



# Set up the environment

module load openfoam/4.1



decomposePar
mpirun rhoPimpleFoam -parallel
reconstructPar
echo "Finished at `date`"
