#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 16:57:31 2018

@author: adutta
"""
import numpy as np
import pandas as pd
import os
import re
import pickle

nprobes = 20
def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

files = os.listdir('./nektar_pp_grad')
os.chdir('./nektar_pp_grad')
files = natural_sorted(files)

As2 = []
for f in files:
    df = pd.read_csv(f,skiprows=3,header=None,delim_whitespace=True)
    df.columns = ['x', 'y', 'z', 'u', 'v', 'w', 'p', 'u_x', 'u_y', 'u_z', 'v_x', 'v_y', 'v_z', 'w_x', 'w_y', 'w_z', 'p_x', 'p_y', 'p_z']
    As = []
    for i in range(nprobes):
        A = [[df.iloc[i]['u_x'],df.iloc[i]['u_y'],df.iloc[i]['u_z']],[df.iloc[i]['v_x'],df.iloc[i]['v_y'],df.iloc[i]['v_z']],[df.iloc[i]['w_x'],df.iloc[i]['w_y'],df.iloc[i]['w_z']]]
        A = np.asarray(A)
        As.append(A)
    As = np.asarray(As)
    As2.append(As)
As2 = np.asarray(As2)    
#note the array As2 has 4 indices time_index,probe_index,3,3
ntimes = len(As2)
def select_probe(As2,p_n):
    p_data = []
    for A in As2:
        p1 = A[p_n]
        p_data.append(p1)
    p_data = np.asarray(p_data)
    return(p_data)

p_data = []
for i in range(nprobes):
    p_temp = select_probe(As2,i)
    p_data.append(p_temp)
os.chdir('./../')

pickle.dump(p_data,open( "p_data_cedar.p", "wb" ))
    