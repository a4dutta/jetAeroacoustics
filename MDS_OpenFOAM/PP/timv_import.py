#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 18:12:43 2018

@author: adutta
"""

import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

def read_nektar_file(filename):
    df = pd.read_csv(filename,sep = ' ',skiprows = 42 ) 
    df = df.dropna(axis = 1)
    cols = ['Time','u','v','w','p']
    df.columns = cols
    npoints = 42
    ntimes = df.Time.unique()
    u_array = np.zeros(len(ntimes)*npoints)
    v_array = np.zeros(len(ntimes)*npoints)
    w_array = np.zeros(len(ntimes)*npoints)
    p_array = np.zeros(len(ntimes)*npoints)
    for value,i in zip(df.u,range(len(u_array))):
        u_array[i] = value
    for value,i in zip(df.v,range(len(v_array))):
        v_array[i] = value
    for value,i in zip(df.w,range(len(w_array))):
        w_array[i] = value
    for value,i in zip(df.p,range(len(p_array))):
        p_array[i] = value
    u_array = np.reshape(u_array,[len(ntimes),npoints])
    v_array = np.reshape(v_array,[len(ntimes),npoints])
    w_array = np.reshape(w_array,[len(ntimes),npoints])   
    p_array = np.reshape(p_array,[len(ntimes),npoints]) 
    return(u_array,v_array,w_array,p_array)

def calculate_mean(any_array):
    mean = np.mean(any_array,axis=0)
    return(mean)

def calculate_fluc(any_array,mean_array):
    fluc_array = any_array - mean_array
    return(fluc_array)
    