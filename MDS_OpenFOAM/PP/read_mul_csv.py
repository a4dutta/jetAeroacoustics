#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 11:32:11 2018

@author: adutta
"""

import numpy as np
import os
import time
from natsort import natsorted

start_time = time.time()

filenames = os.listdir(os.getcwd()+'/pxs/')
filenames = natsorted(filenames)
x0 = 0.005
x1 = 0.06
y0 = -0.02
y1 = 0.02  
X = []
Y = []
U = []
V = []

for f in filenames:
    temp_x,temp_y,temp_u,temp_v = read_of_csv_data(os.getcwd()+'/pxs/'+f,x0,x1,y0,y1)
    X.append(temp_x)
    Y.append(temp_y)
    U.append(temp_u)
    V.append(temp_v)

X = np.asarray(X)
Y = np.asarray(Y)
U = np.asarray(U)
V = np.asarray(V)

X = np.transpose(X,[1,2,0])
Y = np.transpose(Y,[1,2,0])
U = np.transpose(U,[1,2,0])
V = np.transpose(V,[1,2,0])

end_time =time.time()

print(end_time-start_time)