#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 10:18:09 2018

@author: adutta
"""
import pandas as pd
import numpy as np
#from matplotlib import pyplot as plt
import re
import os
import scipy.io

def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

def read_pv_clip_data(foldername,arr_savename):
    u_t_array = []
    v_t_array = []
    fnames = os.listdir(foldername)
    fnames = natural_sorted(fnames)
    for f in fnames:
        df = pd.read_csv(foldername+f,sep=',')
        df.drop('vtkOriginalPointIds',axis=1,inplace=True)
        columns = ['p','u','v','w','x','y','z']
        df.columns = columns
        df = df[df['z']==0]
        df_sl_2 = df.sort_values(by=['y','x'],ascending=True)
        x_array = df_sl_2.x.unique()
        y_array = df_sl_2.y.unique()
        nrows = len(y_array)
        ncolumns = len(x_array)
        u_array = np.asarray(df_sl_2.u)
        v_array = np.asarray(df_sl_2.v)
        u_array = np.reshape(u_array,[nrows,ncolumns])
        v_array = np.reshape(v_array,[nrows,ncolumns])    
        [X, Y] = np.meshgrid(x_array,y_array)
        u_t_array.append(u_array)
        v_t_array.append(v_array)
    #plt.quiver(X,Y,u_array,v_array)
    u_t_array = np.asarray(u_t_array)
    v_t_array = np.asarray(v_t_array)
    u_t_array = np.transpose(u_t_array,[1,2,0])
    v_t_array = np.transpose(v_t_array,[1,2,0])
    scipy.io.savemat(arr_savename, mdict={'x': X,'y': Y,'u':u_t_array,'v':v_t_array})
    return(X,Y,u_t_array,v_t_array)