#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 14:21:07 2018

@author: adutta
"""

import numpy as np

x0 = 0.06
x1 = 0.08
y0 = -0.015
y1 = 0.015

dx = 0.0004
dy = 0.0003

x_arr = np.arange(0.06, 0.08, dx)
y_arr = np.arange(-0.015, 0.015, dy)

for i in range(0,len(x_arr)):
    for j in range(0,len(y_arr)):
        with open('testprobes.txt','a') as prb:
            prb.write('('+str(x_arr[i])+' '+str(y_arr[j])+' '+'0'+')\n')

