import numpy as np

data = []
x_array = []
y_array = []
u_array = []
v_array = []
w_array = []
p_array = []
with open('fldf.dat') as f:
    for _ in range(3):
        next(f)
    for line in f:
        cols = line.split()
        data.append(cols)

length = len(data[0])

for d in data:
    x_array.append(float(d[0]))
    y_array.append(float(d[1]))
    u_array.append(float(d[3]))
    v_array.append(float(d[4]))
    w_array.append(float(d[5]))
    p_array.append(float(d[6]))

x_array = np.asarray(x_array,dtype=float)
y_array = np.asarray(y_array,dtype=float)
u_array = np.asarray(u_array,dtype=float)
v_array = np.asarray(v_array,dtype=float)
w_array = np.asarray(w_array,dtype=float)
p_array = np.asarray(p_array,dtype=float)

x_array = np.transpose(x_array.reshape(60,300))
y_array = y_array.reshape(60,300)
u_array = u_array.reshape(60,300)
v_array = v_array.reshape(60,300)
w_array = w_array.reshape(60,300)
p_array = p_array.reshape(60,300)