#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 15:18:03 2018

@author: adutta
"""

import pandas as pd
import numpy as np
from datetime import datetime
import scipy.io


def cond_of_probecloud(filename,n_probes,n_time):
    df = pd.read_csv('U.csv',skiprows=5102,header=None,delim_whitespace=True)
    #n_probes = 15300
    #n_time = 38
    u_range = np.arange(1,n_probes-1,3)
    for u in u_range:
        df[u] = df[u].str.replace(r"\(","")
    df_remove_col = np.arange(3,15301,3)
    for d in df_remove_col:
        df.drop(d,axis=1,inplace=True)
    return(df)
'''
    arr = np.linspace(1,(n_probes*3+1),(n_probes+1))
    
    for a in arr:
        for i in range(0,(n_time)):
            df[a].iloc[i] = df[a].iloc[i][1:]
            df[a].iloc[i] = float(df[a].iloc[i])
'''


def field_of_plane(df,x_arr,y_arr,n_probes,i):
    u_range = np.arange(1,n_probes-1,3)
    u_values = []
    for u in u_range:
        u_values.append(df.iloc[i][u])
    v_range = np.arange(2,n_probes,3)
    v_values = []
    for v in v_range:
        v_values.append(df.iloc[i][v])
    u_values = np.asarray(u_values)
    v_values = np.asarray(v_values)
    u_values = np.transpose(np.reshape(u_values,[len(x_arr),len(y_arr)]))
    v_values = np.transpose(np.reshape(v_values,[len(x_arr),len(y_arr)]))
    return(u_values,v_values)
    
start_time = datetime.now()
arr_savename = 'sample_of_ts_data'
x0 = 0.04
x1 = 0.1
y0 = -0.015
y1 = 0.015

dx = 0.0004
dy = 0.0003

x_arr = np.arange(0.06, 0.08, dx)
y_arr = np.arange(-0.015, 0.015, dy)
n_time = 38
n_probes = 15300

df = cond_of_probecloud('U.csv',n_probes,n_time)


uv_ts = []
vv_ts = []
i = np.arange(0,3,1)
for a in i:
    temp_uv,temp_vv = field_of_plane(df,x_arr,y_arr,15300,a)
    uv_ts.append(temp_uv)
    vv_ts.append(temp_vv)

uv_ts = np.asarray(uv_ts,dtype=float)
vv_ts = np.asarray(vv_ts,dtype=float)
#uv_ts = np.transpose(uv_ts,[1,2,0])
#vv_ts = np.transpose(vv_ts,[1,2,0])
X,Y = np.meshgrid(x_arr,y_arr)
scipy.io.savemat(arr_savename, mdict={'x': X,'y': Y,'u':uv_ts,'v':vv_ts})
print(datetime.now()-start_time)