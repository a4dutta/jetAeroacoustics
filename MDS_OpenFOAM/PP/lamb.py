#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 11:01:53 2018

@author: adutta
"""

import numpy as np
import pandas as pd

df1 = pd.read_csv('nxs14_lambda_edge_top_data.csv')
df1_new = df1[['u', 'v', 'w','W_x','W_y','W_z']].copy()
lamb = []
for i in range(0,len(df1_new)):
    arr = df1_new.iloc[i]
    arr_u = arr[0:3]
    arr_W = arr[3:6]
    lamb.append(np.cross(arr_W,arr_u))

lamb_mag = []
for l in lamb:
    lamb_mag.append(np.linalg.norm(l))
