#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 17:41:50 2018

@author: adutta
"""

import numpy as np
import pandas as pd


def read_of_csv_data(filename,x0,x1,y0,y1):
    df = pd.read_csv(filename,sep=',')
    columns = ['p','u','v','w','x','y','z']
    df.columns = columns
    df = df[df['z']==0]
    
    df_slice = df[df['x'] > x0]
    df_slice = df_slice[df_slice['x'] < x1]
    df_slice = df_slice[df_slice['y'] < y1]
    df_slice = df_slice[df_slice['y'] > y0]
    
    df_sl_2 = df_slice.sort_values(by=['x','y'],ascending=True)
    x_array = df_sl_2.x.unique()
    y_array = df_sl_2.y.unique()
    [X , Y] = np.meshgrid(x_array,y_array)
    
    u_cat_array = np.zeros([X.shape[0],X.shape[1]])
    v_cat_array = np.zeros([X.shape[0],X.shape[1]])
    u_cat_array = u_cat_array + 990
    v_cat_array = v_cat_array + 880
    
    df_sl_shape = df_sl_2.shape
    for k in range(0,df_sl_shape[0]):
        x_val = df_sl_2.iloc[k].x
        y_val = df_sl_2.iloc[k].y
        for i,j in zip(range(0,u_cat_array.shape[1]),range(0,u_cat_array.shape[0])):
            if(x_val == X[0,i]):
                x_index = i
            if(y_val == Y[j,0]):
                y_index = j
        u_cat_array[y_index,x_index] =  df_sl_2.iloc[k].u
        v_cat_array[y_index,x_index] =  df_sl_2.iloc[k].v      
    
    u_app = []
    v_app = []
    X_new = []
    Y_new = []
    
    for i in range(0,u_cat_array.shape[0]):
        flag = 0
        for j in range(0,u_cat_array.shape[1]):
            if u_cat_array[i][j] == 990:
                flag = 1
        if flag == 0:
            u_app.append(u_cat_array[i,:])
            v_app.append(v_cat_array[i,:])
            X_new.append(X[i,:])
            Y_new.append(Y[i,:])
    
    u_app = np.asarray(u_app)
    v_app = np.asarray(v_app)
    X_new = np.asarray(X_new)
    Y_new = np.asarray(Y_new)
        
    return(X_new,Y_new,u_app,v_app)

