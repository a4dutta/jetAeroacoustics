function dm=getdm(obj,number)
dm=obj.arrdiffmatrix{number};
dm=dm(:,:,1);