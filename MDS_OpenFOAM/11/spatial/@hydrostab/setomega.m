function obj=setomega(obj,omega)
   % Set frequency.
   % omega: The frequency.
   % Note: The normal mode adopted in this code is exp(i*k*x-i*omega*t).
   if obj.intervalset==0
       error('You should call set_interval() before you call setomega().');
   end
   checkintervalset(obj);
   checkscaleset(obj);           
   checknumeric(obj,omega,'omega');
   checkscalar(obj,omega,'omega');
   checknargout(obj,nargout);                      
   obj.omega=omega;
   obj.omegaset=1;
   % Since the value of omega has been changed, the matrix is no longer
   % valid.           
   obj.matrix=[];
   obj.matrixb=[];
   obj.matrixset=0;           
   % clear the variables
   obj.arrvar={};
   obj.arrvardomain=[];
   obj.arrunknownidstart=[];
   obj.arrunknownidend=[];
   obj.arrvarxt={};
   obj.arrunknownidxt=[];          
   obj.varadded=0;         
   obj.nunknowns=0;
   % Since the value of omega has been changed, the stored eigenvalues are no
   % longer valid.
   obj.arreigenvalue=[];
   
