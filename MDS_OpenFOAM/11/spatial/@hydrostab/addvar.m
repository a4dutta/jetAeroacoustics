function obj=addvar(obj,number,varargin)
   % add variables
   % 
   % Syntax
   %
   % obj=addvar(obj,number,name1,name2,...)
   %
   % number: sequence number of the interval     
   % name1, name2, ...: names of the variables
   checknumeric(obj,number,'number');
   checkscalar(obj,number,'number');
   checknargout(obj,nargout);           
   if obj.omegaset==0
       error('You should set the value of the frequency by calling setomega() before you call addvar().');
   end
   if obj.matrixset==1
       error('You cannot call addvar() after you have called addeq().');
   end
   if number>obj.ninterval || number<1
       error('The argument ''number'' is out of range.');
   end
   if nargin<3
       error('You should provide at least one variable name.');
   end
   currentLength=length(obj.arrvar);           
   for i=1:nargin-2
       checkchar(obj,varargin{i},'name1, name2, ...');
       checknamedup(obj,varargin{i});   
       haveerror=0;
       try,
           temp1=evalin('caller',varargin{i});
       catch,
           haveerror=1;
       end
       if haveerror==0 && ~isa(temp1,'equ')
           warnmsg=sprintf('The object ''%s'' is already exist in the caller function and it is not an object of the class ''equ''. It will be replaced by an ''equ'' object created in this function.',varargin{i});
           warning(warnmsg);
       end
       % number, col-range, diffmatrix, isvar, matrixa, matrixb, omega, collocation-points
       assignin('caller',varargin{i},equ(number,obj.nunknowns+1:obj.nunknowns+obj.arrncoll(number),obj.arrdiffmatrix{number}, ...
           1,eye(obj.arrncoll(number)),zeros(obj.arrncoll(number),obj.arrncoll(number),6),obj.omega,obj.arrcollopoint{number},obj.funcvectorized));
       obj.arrvar{currentLength+1}=varargin{i}; % must be cell array, since the length of the name of the variables are not fixed
       obj.arrvardomain(currentLength+1)=number; % do not need to be cell array
       obj.arrunknownidstart(currentLength+1)=obj.nunknowns+1;
       obj.arrunknownidend(currentLength+1)=obj.nunknowns+obj.arrncoll(number);                  
       obj.nunknowns=obj.nunknowns+obj.arrncoll(number);       
       currentLength=currentLength+1;               
   end
   obj.varadded=1;
   
