function omega=eigenvalue(obj,varargin)
   % Return the eigenvalues.
   %
   % Syntax
   % 
   % omega=eigenvalue(obj)
   % omega=eigenvalue(obj,threshold)
   %
   % Description
   % omega=eigenvalue(obj) returns all the eigenvalues.
   % omega=eigenvalue(obj,threshold) only returns eigenvalues of which the 
   % absolute value is within the interval [-threshold, threshold]. 
   % The argument 'threshold' must be a scalar.
   if isempty(obj.arreigenvalue)
       error('The eigenvalue problem has not been solved yet. Please call compute() to solve the eigenvalue problem. (Note: After each call to setomega(), the stored eigenvalues are automatically cleared. Therefore, after each call to setomega(), you should call compute() re-compute the eigenvalues.)');
   end
   omega=obj.arreigenvalue;
   % ignore infinite eigenvalues
   notinf=find(abs(omega)~=inf);
   omega=omega(notinf);
   % ignore eigenvalues of which the absolute value is outside the interval [-threshold, threshold]. 
   if ~isempty(varargin)
       threshold=varargin{1};
       checknumeric(obj,threshold,'threshold');
       checkscalar(obj,threshold,'threshold');
       if threshold<=0
           error('The argument ''threshold'' must be positive.');
       end                              
       withinrange=find(abs(omega)<=threshold);
       omega=omega(withinrange);
   end
   