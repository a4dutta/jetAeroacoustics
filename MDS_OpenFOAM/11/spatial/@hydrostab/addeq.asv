function obj=addeq(obj,varargin)
   % Add equations
   %
   % Syntax
   % obj=addeq(obj,eq)
   % obj=addeq(obj,eq,option)
   % 
   % Description
   % obj=addeq(obj,eq) add the equations represented by eq to the
   % curent matrix.
   % obj=addeq(obj,eq,option) allows additional options. Avaliable
   % options are listed as follows.
   % 'full': the default option. Add all equations in eq.
   %
   % 'ignoreendpoints': Ignore the equations at the two endpoints
   % of the interval. This option can only be used on a finite interval.
   %
   % 'ignorefirstpoint': Ignore the equation at the first point of
   % the interval. This option can only be used on a finite
   % interval or a semi-infinite interval of [a,inf] type.
   %
   % 'ignorelastpoint': Ignore the equation at the last point of
   % the interval. This option can only be used on a finite
   % interval or a semi-infinite interval of [-inf,a] type.
   %
   % 'ignorepossible': Ignore endpoints whenever possible. For a
   % finite interval, this option means ignoring both endpoints. For
   % a semi-infinite interval of [a,inf] type, this options means
   % ignoring the first point. For a semi-infinite interval of
   % [-inf,a] type, this option means ignoring the last point.
   %
   % Note: These options are intended for governing equations within the interior 
   % of the interval. In this case, a number of equations are
   % added. For boundary conditions, only one equation is added,
   % and you cannot ignore anything.
   if obj.varadded==0
       error('You should add all the variables by calling addvar() and addvarxt() before you call addeq(). (Note: each time you call setomega(), all the variables are cleared. Therefore, please re-add all the variables after you call setomega().)');
   end
   if ~isempty(obj.arreigenvalue)
       error('You cannot call addeq() after you have called compute().');
   end
   checknargout(obj,nargout);                      
   if length(varargin)~=1 && length(varargin)~=2
       error('Please use one of the following two syntaxes: (1) obj=addeq(obj,eq);  (2) obj=addeq(obj,eq,option); , where ''obj'' is an object of the class hydrostab.');
   end
   if length(varargin)==1
       eq=varargin{1};
       option='full';
   else
       eq=varargin{1};
       option=varargin{2};
       checkchar(obj,option,'option');
   end
   if ~isa(eq,'equ')
       error('The argument ''eq'' must be an object of the class ''equ''. Symbolic objects, character array and numeric objects are all not supported.');
   end
   if isempty(obj.matrix)
       obj.matrix=zeros(0,obj.nunknowns); 
       obj.matrixb=zeros(0,obj.nunknowns,6); 
   end
   a=zeros(size(get(eq,'matrixa'),1),obj.nunknowns);
   b=zeros(size(get(eq,'matrixb'),1),obj.nunknowns,6);
   a(:,get(eq,'colrange'))=get(eq,'matrixa');
   b(:,get(eq,'colrange'),:)=get(eq,'matrixb');
   
   if strcmp(option,'ignorepossible')==1
       if get(eq,'intervalid')==-1
           warning('This is a boundary condition. No points are ignored.');
           option='full';
       else
           if obj.arrtype(get(eq,'intervalid'))~=1 && obj.arrtype(get(eq,'intervalid'))~=2
               warning('This is an infinite interval. No points are ignored.');
               option='full';
           elseif obj.arrtype(get(eq,'intervalid'))==1
               option='ignoreendpoints';
           elseif obj.arrtype(get(eq,'intervalid'))==2 && obj.arrinterval(1,get(eq,'intervalid'))==-inf
               option='ignorelastpoint';
           else
               option='ignorefirstpoint';                       
           end
       end        
   end
   if strcmp(option,'full')==1        
       checkinfnan(obj,a);
       checkinfnan(obj,b);
       obj.matrix=[obj.matrix;a];
       obj.matrixb=[obj.matrixb;b];               
   elseif strcmp(option,'ignorefirstpoint')==1
       if get(eq,'intervalid')==-1
           error('This is a boundary condition. You cannot ignore anything.');
       end
       if obj.arrtype(get(eq,'intervalid'))~=1 && obj.arrtype(get(eq,'intervalid'))~=2
           error('You can use the option ''ignorefirstpoint'' only on a finite interval or a semi-infinite interval.');                   
       end      
       if obj.arrtype(get(eq,'intervalid'))==2 && obj.arrinterval(1,get(eq,'intervalid'))==-inf
           error('The interval is [-inf,a] type. You should use the ''ignorelastpoint'' option.');
       end
       checkinfnan(obj,a(2:end,:));
       checkinfnan(obj,b(2:end,:));               
       obj.matrix=[obj.matrix;a(2:end,:)];
       obj.matrixb=[obj.matrixb;b(2:end,:)];               
   elseif strcmp(option,'ignorelastpoint')==1
       if get(eq,'intervalid')==-1
           error('This is a boundary condition. You cannot ignore anything.');
       end               
       if obj.arrtype(get(eq,'intervalid'))~=1 && obj.arrtype(get(eq,'intervalid'))~=2
           error('You can use the option ''ignorelastpoint'' only on a finite interval or a semi-infinite interval.');                   
       end
       if obj.arrtype(get(eq,'intervalid'))==2 && obj.arrinterval(2,get(eq,'intervalid'))==inf
           error('The interval is [a,inf] type. You should use the ''ignorefirstpoint'' option.');
       end               
       checkinfnan(obj,a(1:(end-1),:));
       checkinfnan(obj,b(1:(end-1),:));               
       obj.matrix=[obj.matrix;a(1:(end-1),:)];
       obj.matrixb=[obj.matrixb;b(1:(end-1),:)];               
   elseif strcmp(option,'ignoreendpoints')==1      
       if get(eq,'intervalid')==-1
           error('This is a boundary condition. You cannot ignore anything.');
       end               
       if obj.arrtype(get(eq,'intervalid'))~=1
           error('You can use the option ''ignoreendpoints'' only on a finite interval.');
       end
       checkinfnan(obj,a(2:(end-1),:));
       checkinfnan(obj,b(2:(end-1),:));               
       obj.matrix=[obj.matrix;a(2:(end-1),:)];
       obj.matrixb=[obj.matrixb;b(2:(end-1),:)];             
   else
       errmsg=sprintf('Unknown option: ''%s''. Valid options are: ''full'', ''ignorefirstpoint'', ''ignorelastpoint'', ''ignoreendpoints'', ''ignorepossible''.',option);
       error(errmsg);
   end           
   obj.matrixset=1;           
   
