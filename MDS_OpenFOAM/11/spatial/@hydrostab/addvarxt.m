function obj=addvarxt(obj,varargin)
   % add variables which is function of only x and t (For example, the interface displacement)
   % 
   % Syntax
   %
   % obj=addvarxt(obj,name1,name2,...)
   %
   % name1, name2, ...: names of the variables           
   checknargout(obj,nargout);                      
   if obj.omegaset==0
       error('You should set the value of the frequency by calling setomega() before you call addvarxt().');
   end
   if obj.matrixset==1
       error('You cannot call addvarxt() after you have called addeq().');
   end           
   if nargin<2
       error('You should provide at least one variable name.');
   end           
   currentLength=length(obj.arrvarxt);
   for i=1:nargin-1
       checkchar(obj,varargin{i},'name1, name2, ...');
       checknamedup(obj,varargin{i});
       % number, col-range, diffmatrix, isvar, matrixa, matrixb, omega,
       % collocation-points
       assignin('caller',varargin{i},equ(-1,obj.nunknowns+1,[],1,1,zeros(1,1,6),obj.omega,[],obj.funcvectorized));
       obj.arrvarxt{currentLength+1}=varargin{i}; % must be cell array, since the length of the name of the variables are not fixed
       obj.arrunknownidxt(currentLength+1)=obj.nunknowns+1;               
       obj.nunknowns=obj.nunknowns+1;
       currentLength=currentLength+1;               
   end
   obj.varadded=1;
   
