function obj = hydrostab(n_interval,varargin)
   % constructor
   %
   % Syntax
   %
   % obj=hydrostab(n_interval)
   % obj=hydrostab(n_interval,option)
   %
   % Description
   % obj=hydrostab(n_interval) creates a hydrostab object.
   % 'n_interval' is the number of intervals.
   % 
   % obj=hydrostab(n_interval,option) creates a hydrostab object
   % with additional options. At present, the only available option
   % is 'vectorizedfunc'. This option means that all the
   % user-supplied  functions (which are created by func()) can be
   % vectorized. To write a user-supplied function which can be
   % vectorized, you must take extra care and make sure that you
   % have used element-wise operators when needed. For example,
   % the following user-supplied function cannot be vectorized:
   %
   % U=func('1-y^2');
   %
   % while the following one can be vectorized:
   %
   % U=func('1-y.^2');
   % Using this option can enhance the performance of the code.
   
	% Note: new version of MATLAB (2008a or later) allows a new format for defining
	% classes.
      
  obj.ninterval=[]; % number of intervals
  obj.arrinterval=[]; % start points and end points of the intervals
  obj.arrncoll=[]; % number of collocation points in each interval
  obj.arrtype=[]; % type of spectral method (1: Chebyshev;  2: Laguerre; 3: Hermite)
  obj.arrscale=[]; % range of collocation points (only for Laguerre method and Hermite method)
  obj.arrdiffmatrix=[]; % store the differentiation matrices
  obj.arrcollopoint=[]; % store the collocation points
  obj.arrvar=[]; % store the name of the variables
  obj.arrvardomain=[]; % store the domain ID (sequence number of the interval) of the variables
  obj.arrunknownidstart=[];
  obj.arrunknownidend=[];      
  obj.arrvarxt=[]; % variables which is function of only x and t
  obj.arrunknownidxt=[];      
  obj.intervalset=[]; % indicates whether the function set_interval() has been called
  obj.scaleset=[]; % indicates whether the function setscale() has been called
  obj.omegaset=[]; % indicates whether omega has been set      
  obj.varadded=[]; % indicates whether the function addvar() or addvarxt() has been called
  obj.matrixset=[]; % indicates whether the matrix for computing eigenvalues has been set
  obj.arrintervalset=[]; % indicates whether each interval has been set
  obj.arrscaleset=[]; % indicates whether the 'scale' parameter of each interval has been set
  obj.nunknowns=[];
  obj.omega=[]; % frequency
  obj.matrix=[]; % the matrix A for computing eigenvalues
  obj.matrixb=[]; % the matrix B for computing eigenvalues
  obj.arreigenvalue=[]; % store the eigenvalues obtained from the latest call to compute()
  obj.arreigenvector=[]; % store the eigenvectors obtained from the latest call to compute()
  obj.funcvectorized=[]; % whether the user-supplied function or expression can be vectorized
   
   obj=class(obj,'hydrostab');
   
   checknumeric(obj,n_interval,'n_interval');
   checkscalar(obj,n_interval,'n_interval');
   obj.ninterval = n_interval;
   obj.arrinterval=zeros(2,n_interval);
   obj.arrncoll=zeros(1,n_interval);
   obj.arrtype=zeros(1,n_interval);
   obj.arrscale=zeros(1,n_interval);
   obj.arrdiffmatrix=cell(1,n_interval);
   obj.arrcollopoint=cell(1,n_interval);
   
   obj.arrvar={};
   obj.arrvardomain=[];
   obj.arrunknownidstart=[];
   obj.arrunknownidend=[];
   obj.arrvarxt={};
   obj.arrunknownidxt=[];           
   
   obj.intervalset=0;
   obj.scaleset=0;
   obj.varadded=0;
   obj.omegaset=0;
   obj.matrixset=0;
   
   obj.arrintervalset=zeros(1,n_interval);
   obj.arrscaleset=zeros(1,n_interval);
   
   obj.nunknowns=0;
   obj.omega=0;
   obj.matrix=[];
   obj.matrixb=[];
   obj.arreigenvalue=[];
   obj.arreigenvector=[];
   if isempty(varargin)
       obj.funcvectorized=0;
   else
       option=varargin{1};
       checkchar(obj,option,'option');
       if strcmp(option,'vectorizedfunc')==1
           obj.funcvectorized=1;
       else
           errmsg=sprintf('Unknown option: ''%s''. Valid options are: ''vectorizedfunc''.',option);
           error(errmsg);
       end
   end
   
