function checkinfnan(obj,a)
   if sum(sum(isinf(a)))~=0 
       error('Your equation contains Inf (Infinity). Maybe there is a divide-by-zero error.');
   end
   if sum(sum(isnan(a)))~=0
       error('Your equation contains NaN (Not a number).');
   end   
   
