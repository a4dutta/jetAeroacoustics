function checknamedup(obj,name)
   % check for duplication of variable names
   for i=1:length(obj.arrvar)
       if strcmp(obj.arrvar{i},name)==1
           errmsg=sprintf('Name duplication: %s',name);
           error(errmsg);
       end
   end
   for i=1:length(obj.arrvarxt)
       if strcmp(obj.arrvarxt{i},name)==1
           errmsg=sprintf('Name duplication: %s',name);
           error(errmsg);
       end               
   end           
   
