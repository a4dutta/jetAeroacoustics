function obj=setscale(obj,number,scale)
   checknumeric(obj,number,'number');
   checkscalar(obj,number,'number');
   checknumeric(obj,scale,'scale');
   checkscalar(obj,scale,'scale');
   if obj.intervalset==0
       error('You should call set_interval() before you call setscale().');
   end
   checkintervalset(obj);
   if obj.omegaset==1
       error('You cannot call setscale() after you have call setomega().');
   end
   checknargout(obj,nargout);
   if number>obj.ninterval || number<1
       error('The argument ''number'' is out of range.');
   end
   if obj.arrtype(number)~=2 && obj.arrtype(number)~=3
       error('This parameter is only applied to Laguerre method or Hermite method.');
   end
   obj.arrscale(number)=scale;
   if obj.arrtype(number)==2 % Laguerre method
       
       % compute differentiation matrices and collocation points
       [collopoint, DM] = lagdif(obj.arrncoll(number), min(obj.arrncoll(number)-1,6), 1);
       
       if obj.arrinterval(1,number)==-inf % [-inf,a]
           % map [0,collopoint(end)] to [a-scale,a]      
           scaling=scale/collopoint(end);                   
           collopoint_new=obj.arrinterval(2,number)-collopoint*scaling;
           collopoint_new=collopoint_new(end:-1:1);
           E1=eye(obj.arrncoll(number));
           E1r=E1(:,obj.arrncoll(number):-1:1);
           DMnew=DM;
           for i=1:size(DM,3)
               DMnew(:,:,i)=(-1)^i*E1r*DMnew(:,:,i)*E1r;                                          
               DMnew(:,:,i)=DMnew(:,:,i)/(scaling^i);
           end
           % test
%                    yy=exp(2*collopoint_new);
%                    dyy=DMnew(:,:,1)*yy;
%                    d2yy=DMnew(:,:,2)*yy;
%                    d3yy=DMnew(:,:,3)*yy;
%                    d4yy=DMnew(:,:,4)*yy;
           obj.arrdiffmatrix{number}=DMnew;
           obj.arrcollopoint{number}=collopoint_new;                   
       else % [a,inf]
           % map [0,collopoint(end)] to [a,a+scale]
           scaling=scale/collopoint(end);
           collopoint_new=obj.arrinterval(1,number)+collopoint*scaling;   
           DMnew=DM;
           for i=1:size(DM,3)
               DMnew(:,:,i)=DMnew(:,:,i)/(scaling^i);
           end
           % test
%                    yy=exp(-2*collopoint_new);
%                    dyy=DMnew(:,:,1)*yy;
%                    d2yy=DMnew(:,:,2)*yy;
%                    d3yy=DMnew(:,:,3)*yy;
%                    d4yy=DMnew(:,:,4)*yy;
           obj.arrdiffmatrix{number}=DMnew;
           obj.arrcollopoint{number}=collopoint_new;                   
       end
   elseif obj.arrtype(number)==3 % Hermite method
       
       % compute differentiation matrices and collocation points
       [collopoint, DM] = herdif(obj.arrncoll(number), min(obj.arrncoll(number)-1,6), 1);
       
       % map [collopoint(1),collopoint(end)] to [-scale,scale]
       scaling=scale/collopoint(end);
       collopoint_new=collopoint*scaling;
       DMnew=DM;
       for i=1:size(DM,3)
           DMnew(:,:,i)=DMnew(:,:,i)/(scaling^i);
       end
       % test
%                yy=exp(-collopoint_new.^2);
%                dyy=DMnew(:,:,1)*yy;
%                dyytrue=-2*collopoint_new.*exp(-collopoint_new.^2);
%                d2yy=DMnew(:,:,2)*yy;
%                d2yytrue=4*collopoint_new.^2.*exp(-collopoint_new.^2) - 2*exp(-collopoint_new.^2);
%                d3yy=DMnew(:,:,3)*yy;
%                d3yytrue=12*collopoint_new.*exp(-collopoint_new.^2) - 8*collopoint_new.^3.*exp(-collopoint_new.^2);
%                d4yy=DMnew(:,:,4)*yy;
%                d4yytrue=12*exp(-collopoint_new.^2) - 48*collopoint_new.^2.*exp(-collopoint_new.^2) + 16*collopoint_new.^4.*exp(-collopoint_new.^2);
           obj.arrdiffmatrix{number}=DMnew;
           obj.arrcollopoint{number}=collopoint_new;                   
   else
       % never happen
   end
   obj.scaleset=1;
   obj.arrscaleset(number)=1;
   
