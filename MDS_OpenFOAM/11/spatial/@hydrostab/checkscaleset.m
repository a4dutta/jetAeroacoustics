function checkscaleset(obj)
   for i=1:obj.ninterval
       if (obj.arrtype(i)==2 || obj.arrtype(i)==3) && obj.arrscaleset(i)==0
           errmsg=sprintf('You have not set the scale for Interval %d yet. You must call setscale() for an interval if Laguerre method or Hermite method is applied to that interval.',i);
           error(errmsg);
       end
   end           
   
