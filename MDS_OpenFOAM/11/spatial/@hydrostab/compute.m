function obj=compute(obj)
   % Solve the eigenvalue problem.
   %
   % Syntax
   % 
   % obj=compute(obj)
   checknargout(obj,nargout);                                 
   if obj.matrixset==0
       error('The matrix for computing eigenvalues has not been set. Please use addeq() to add equations. (Note: After each call to setomega(), the matrix is automatically cleared. Therefore, after each call to setomega(), you should reset all the equations.)');
   end
   if size(obj.matrix,1)<obj.nunknowns
       errmsg=sprintf('Number of equation=%d, number of unknown=%d, number of equation is less than number of unknown. Please use addeq() to add extra equations.', ...
           size(obj.matrix,1),obj.nunknowns);
       error(errmsg);
   end
   if size(obj.matrix,1)>obj.nunknowns
       errmsg=sprintf('Number of equation=%d, number of unknown=%d, number of equation is greater than number of unknown. Please ignore some equations at some endpoints by using the ''option'' argument of the function addeq().', ...
           size(obj.matrix,1),obj.nunknowns);
       error(errmsg);
   end           
%    if sum(sum(sum(obj.matrixb==0)))==size(obj.matrixb,1)*size(obj.matrixb,2)*size(obj.matrixb,3)
%        error('Your problem contains no spatial derivative. This is not allowed.');
%    end
   % find maximum order of derivative with respect to x
   max_order=0;
   for i=1:6
       if sum(sum(obj.matrixb(:,:,i)==0))~=size(obj.matrixb(:,:,i),1)*size(obj.matrixb(:,:,i),2)
           max_order=i;
       end
   end
   if max_order==0
       error('Your problem contains no spatial derivative. This is not allowed.');
   end
   if max_order==1
       [eigenvec,D]=eig(obj.matrix,-obj.matrixb(:,:,1)); % Note the negative sign
   else % max_order>=2
       newmatrix=zeros(size(obj.matrix,1)*max_order,size(obj.matrix,1)*max_order);
       newmatrixb=zeros(size(obj.matrix,1)*max_order,size(obj.matrix,1)*max_order);
       newmatrix(1:size(obj.matrix,1),1:size(obj.matrix,1))=obj.matrix;
       newmatrixb(1:size(obj.matrix,1),1:size(obj.matrix,1))=obj.matrixb(:,:,1);
       for i=2:max_order
           newmatrix(size(obj.matrix,1)*(i-1)+1:size(obj.matrix,1)*i,size(obj.matrix,1)*(i-1)+1:size(obj.matrix,1)*i)=-eye(size(obj.matrix,1));
           newmatrixb(size(obj.matrix,1)*(i-1)+1:size(obj.matrix,1)*i,size(obj.matrix,1)*(i-2)+1:size(obj.matrix,1)*(i-1))=eye(size(obj.matrix,1));
           newmatrixb(1:size(obj.matrix,1),size(obj.matrix,1)*(i-1)+1:size(obj.matrix,1)*i)=obj.matrixb(:,:,i);
       end
%        findzero=1;
%        while findzero==1
%            findzero=0;
%            sz=size(obj.matrix,1);
%            for i=sz+1:size(newmatrixb,1)
%                if sum(newmatrixb(:,i)==0)==length(newmatrixb(:,i))
%                    newmatrix=[newmatrix(1:i-1,1:i-1),newmatrix(1:i-1,i+1:end);newmatrix(i+1:end,1:i-1),newmatrix(i+1:end,i+1:end)];               
%                    newmatrixb=[newmatrixb(1:i-1,1:i-1),newmatrixb(1:i-1,i+1:end);newmatrixb(i+1:end,1:i-1),newmatrixb(i+1:end,i+1:end)]; 
%                    findzero=1;
%                    break
%                end
%            end           
%        end
       [eigenvec,D]=eig(newmatrix,-newmatrixb); % Note the negative sign
   end
   
   omega=diag(D);     
   % store the eigenvalues and eigenvectors
   obj.arreigenvalue=omega;
   obj.arreigenvector=eigenvec;
   
