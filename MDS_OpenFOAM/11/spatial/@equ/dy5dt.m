       function obj=dy5dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,5);
           obj.diffmatrix=[];
           obj.omega=[];
