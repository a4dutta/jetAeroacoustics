function display(obj)
   fprintf('-------------object of class ''equ''-------------------\n');
   if obj.intervalid==-1
       fprintf('The object is defined on boundary.\n');
   else
       fprintf('The object is defined on Interval %d.\n',obj.intervalid);
   end
   if obj.isvar==1
       fprintf('The object is a variable created by addvar() or addvarxt().\n');               
   else
       fprintf('The object is not a variable created by addvar() or addvarxt().\n');                              
   end
   
