       function obj=dydt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,1);
           obj.diffmatrix=[];
           obj.omega=[];
