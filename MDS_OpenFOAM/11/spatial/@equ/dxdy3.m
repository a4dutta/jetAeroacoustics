       function obj=dxdy3(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixb=zeros(length(obj.colrange),length(obj.colrange),6);
           obj.matrixb(:,:,1)=(1i)^1*1*obj.diffmatrix(:,:,3);
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.omega=[];
