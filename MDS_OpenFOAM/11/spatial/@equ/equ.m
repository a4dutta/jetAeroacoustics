function obj=equ(number,colrange,diffmatrix,isvar,matrixa,matrixb,omega,collopoint,funcvectorized)
   obj.intervalid=number; % sequence number of the interval (if the object is defined at a single point, then intervalid=-1)
   obj.colrange=colrange; % range of column numbers in the matrix  
   obj.diffmatrix=diffmatrix;
   % if isvar==1, then the object represents a variable created by hydrostab::addvar() or hydrostab::addvarxt(). Otherwise,
   % the object is the result of some operations.   
   obj.isvar=isvar;
   obj.matrixa=matrixa; 
   obj.matrixb=matrixb;
   obj.omega=omega; % frequency
   obj.collopoint=collopoint; % collocation points
   obj.funcvectorized=funcvectorized; % whether the user-supplied function or expression can be vectorized
   obj=class(obj,'equ');
    

