       function obj=dy3dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,3);
           obj.diffmatrix=[];
           obj.omega=[];
