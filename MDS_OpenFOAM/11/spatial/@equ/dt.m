       function obj=dt(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.omega=[];
