       function obj=dy2dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,2);
           obj.diffmatrix=[];
           obj.omega=[];
