       function obj=dx2dy4(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixb=zeros(length(obj.colrange),length(obj.colrange),6);
           obj.matrixb(:,:,2)=(1i)^2*1*obj.diffmatrix(:,:,4);
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.omega=[];
