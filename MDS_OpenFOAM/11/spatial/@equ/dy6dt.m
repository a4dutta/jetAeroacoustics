       function obj=dy6dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,6);
           obj.diffmatrix=[];
           obj.omega=[];
