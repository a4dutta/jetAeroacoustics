       function obj=dx5dydt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixb=zeros(length(obj.colrange),length(obj.colrange),6);
           obj.matrixb(:,:,5)=(1i)^5*(-1i*obj.omega)*obj.diffmatrix(:,:,1);
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.omega=[];
