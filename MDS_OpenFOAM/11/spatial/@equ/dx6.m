       function obj=dx6(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixb=zeros(length(obj.colrange),length(obj.colrange),6);
           obj.matrixb(:,:,6)=(1i)^6*1*eye(length(obj.colrange));
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.omega=[];
