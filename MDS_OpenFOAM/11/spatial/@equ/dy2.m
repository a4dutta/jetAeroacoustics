       function obj=dy2(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=1*obj.diffmatrix(:,:,2);
           obj.diffmatrix=[];
           obj.omega=[];
