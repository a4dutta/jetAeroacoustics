       function obj=dy3(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=1*obj.diffmatrix(:,:,3);
           obj.diffmatrix=[];
           obj.omega=[];
