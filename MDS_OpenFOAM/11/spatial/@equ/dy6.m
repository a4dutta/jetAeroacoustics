       function obj=dy6(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=1*obj.diffmatrix(:,:,6);
           obj.diffmatrix=[];
           obj.omega=[];
