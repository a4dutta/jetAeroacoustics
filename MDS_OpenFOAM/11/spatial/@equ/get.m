function val=get(obj,name)
switch name
    case 'matrixa'
        val=obj.matrixa;
    case 'matrixb'
        val=obj.matrixb;
    case 'colrange'
        val=obj.colrange;
    case 'intervalid'
        val=obj.intervalid;
    case 'isvar'
        val=obj.isvar;
    otherwise
        error([name ,'Is not a valid equ property']);
end