       function obj=dy4dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(-1i*obj.omega)*obj.diffmatrix(:,:,4);
           obj.diffmatrix=[];
           obj.omega=[];
