function obj=mtimes(obj1,obj2)
   if isa(obj1,'func')&&isa(obj2,'equ')
       obj=obj2*obj1;
   elseif isa(obj1,'func')&&isa(obj2,'func')
       obj=obj1;       
       obj.arrhandle=cat(2,obj.arrhandle,obj2.arrhandle);
       obj.arrargument=cat(2,obj.arrargument,obj2.arrargument);
       obj.arrhandleden=cat(2,obj.arrhandleden,obj2.arrhandleden);
       obj.arrargumentden=cat(2,obj.arrargumentden,obj2.arrargumentden);    
       obj.coeff=obj.coeff*obj2.coeff;
   elseif isa(obj1,'func')&&isnumeric(obj2)
        siz=size(obj2);
        if siz(1)~=1||siz(2)~=1
           error('If the second operand of the multiplication is numeric, it must be a scalar.');
       end
       obj=obj1;       
       obj.coeff=obj.coeff*obj2;
   elseif isa(obj2,'func')&&isnumeric(obj1)
        siz=size(obj1);
        if siz(1)~=1||siz(2)~=1
           error('If the first operand of the multiplication is numeric, it must be a scalar.');
       end
       obj=obj2;     
       obj.coeff=obj.coeff*obj1;       
   else
       error(['Cannot perform multiplication. For multiplication obj1*obj2, only the following seven situations are allowed: ' ...
           '(1) obj1 is a numeric scalar, obj2 is an ''equ'' object; (2) obj1 is an ''equ'' object, obj2 is a numeric scalar; ' ...
           '(3) obj1 is a ''func'' object, obj2 is an ''equ'' object; (4) obj1 is an ''equ'' object, obj2 is a ''func'' object; ' ...
           '(5) obj1 is a ''func'' object, obj2 is a ''func'' object; (6) obj1 is a ''func'' object, obj2 is a numeric scalar; ' ...
           '(7) obj1 is a numeric scalar, obj2 is a ''func'' object.  The situation that both obj1 and obj2 are ''equ'' object ' ...
           'is not allowed because this program only accepts linearized equations.']);
   end
   
