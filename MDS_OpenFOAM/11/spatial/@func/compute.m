function fval=compute(obj,collopoint,vectorized)
       ncollopoint=length(collopoint);       
       fval=ones(ncollopoint,1)*obj.coeff;   
       for j=1:length(obj.arrhandle)
           if vectorized==1
               fval=fval.*feval(obj.arrhandle{j},collopoint,obj.arrargument{j}{:});
           else
               for i=1:ncollopoint
                   fval(i)=fval(i)*feval(obj.arrhandle{j},collopoint(i),obj.arrargument{j}{:});
               end                   
           end           
       end
       for j=1:length(obj.arrhandleden)
           if vectorized==1
               fval=fval./feval(obj.arrhandleden{j},collopoint,obj.arrargumentden{j}{:});
           else
               for i=1:ncollopoint
                   fval(i)=fval(i)/feval(obj.arrhandleden{j},collopoint(i),obj.arrargumentden{j}{:});
               end                   
           end                      
       end       
