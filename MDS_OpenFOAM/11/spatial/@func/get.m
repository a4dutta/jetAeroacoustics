function val=get(obj,name)
switch name
    case 'coeff'
        val=obj.coeff;
    case 'arrhandle'
        val=obj.arrhandle;
    case 'arrargument'
        val=obj.arrargument;
    case 'arrhandleden'
        val=obj.arrhandleden;
    case 'arrargumentden'
        val=obj.arrargumentden;        
    otherwise
        error([name ,'Is not a valid equ property']);
end
