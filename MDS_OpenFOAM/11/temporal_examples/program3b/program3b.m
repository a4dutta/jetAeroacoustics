clear
Re=5772.22; 
hs=hydrostab(1);
hs=set_interval(hs,1,-1,1,50);
nk=100;
arrk=linspace(0,1.7,nk);
U=func(@velocity);
dU=func(@dvelocity);

for i=1:nk
    hs=setk(hs,arrk(i)); 
    hs=addvar(hs,1,'u','v','p');    
    hs=addeq(hs,dt(u)+U*dx(u)+v*dU==-dx(p)+1/Re*(dx2(u)+dy2(u)),'ignoreendpoints');
    hs=addeq(hs,dt(v)+U*dx(v)==-dy(p)+1/Re*(dx2(v)+dy2(v)),'ignoreendpoints');
    hs=addeq(hs,dx(u)+dy(v)==0);
    hs=addeq(hs,at(u,1)==0);
    hs=addeq(hs,at(v,1)==0);
    hs=addeq(hs,at(u,-1)==0);
    hs=addeq(hs,at(v,-1)==0);      
    hs=compute(hs);
    eige=eigenvalue(hs,1e8);        
    plot(arrk(i)*ones(length(eige),1),imag(eige),'k+');
    hold on
end
axis([0 1.7 -0.06 0.02]);
grid on
