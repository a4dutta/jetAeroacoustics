clear
Pr=7;
nk=20;
arrk=linspace(0.1,8,nk);
nRa=20;
arrRa=linspace(0,8000,nRa);
[gridk,gridRa]=meshgrid(arrk,arrRa); 
gridgrowth=zeros(nRa,nk);
hs=hydrostab(1);
hs=set_interval(hs,1,0,1,40);
for i=1:nk
    fprintf('%d of %d\n',i,nk);
    for j=1:nRa
        hs=setk(hs,arrk(i));
        hs=addvar(hs,1,'u','v','p','theta');
        hs=addeq(hs,dx(u)+dy(v)==0);
        hs=addeq(hs,dt(u)==-dx(p)+Pr*(dx2(u)+dy2(u)),'ignoreendpoints');
        hs=addeq(hs,dt(v)==-dy(p)+Pr*(dx2(v)+dy2(v))+Pr*arrRa(j)*theta,'ignoreendpoints');
        hs=addeq(hs,dt(theta)-v==dx2(theta)+dy2(theta),'ignoreendpoints');
        hs=addeq(hs,at(u,1)==0);
        hs=addeq(hs,at(v,1)==0);
        hs=addeq(hs,at(u,0)==0);
        hs=addeq(hs,at(v,0)==0);
        hs=addeq(hs,at(theta,1)==0);
        hs=addeq(hs,at(theta,0)==0);
        hs=compute(hs);
        eige=eigenvalue(hs,1e8);
        gridgrowth(j,i)=max(imag(eige));
    end
end
contour(gridk,gridRa,gridgrowth,[0 0],'k');
