clear
Re=20;
U=func('tanh(y)');
dU=func('1 - tanh(y).^2');
nscale=4;
arrscale=logspace(0.7,1.9,nscale);
arrcolor={'r','g','b','m'};
nk=50;
arrk=linspace(0,1,nk);
arrgrowthrate=zeros(1,nk);
for j=1:nscale
    fprintf('%d of %d\n',j,nscale);
    hs=hydrostab(1,'vectorizedfunc');
    hs=set_interval(hs,1,-inf,inf,70);
    hs=setscale(hs,1,arrscale(j));        
    for i=1:nk
        hs=setk(hs,arrk(i)); 
        hs=addvar(hs,1,'u','v','p');
        hs=addeq(hs,dt(u)+U*dx(u)+v*dU==-dx(p)+1/Re*(dx2(u)+dy2(u)));
        hs=addeq(hs,dt(v)+U*dx(v)==-dy(p)+1/Re*(dx2(v)+dy2(v)));
        hs=addeq(hs,dx(u)+dy(v)==0);
        hs=compute(hs);
        eige=eigenvalue(hs,1e8);   
        arrgrowthrate(i)=max(imag(eige));
    end
    plot(arrk,arrgrowthrate,arrcolor{j});
    hold on
end
