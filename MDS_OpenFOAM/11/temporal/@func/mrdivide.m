function obj=mrdivide(obj1,obj2)
    if isnumeric(obj1)&&isa(obj2,'func')
        siz=size(obj1);
        if siz(1)~=1||siz(2)~=1
           error('If the first operand is numeric, it must be a scalar.');                    
        end
       obj=obj2;       
       obj.arrhandle=obj2.arrhandleden;
       obj.arrargument=obj2.arrargumentden;
       obj.arrhandleden=obj2.arrhandle;
       obj.arrargumentden=obj2.arrargument;
       obj.coeff=obj1/obj.coeff;              
    elseif isa(obj1,'func')&&isnumeric(obj2)
        siz=size(obj2);
        if siz(1)~=1||siz(2)~=1
           error('If the second operand is numeric, it must be a scalar.');                    
        end
       obj=obj1;     
       obj.coeff=obj.coeff/obj2;       
    elseif isa(obj1,'func')&&isa(obj2,'func')
       obj=obj1;       
       obj.arrhandle=cat(2,obj.arrhandle,obj2.arrhandleden);
       obj.arrargument=cat(2,obj.arrargument,obj2.arrargumentden);
       obj.arrhandleden=cat(2,obj.arrhandleden,obj2.arrhandle);
       obj.arrargumentden=cat(2,obj.arrargumentden,obj2.arrargument);  
       obj.coeff=obj.coeff/obj2.coeff;
    else
       error(['Cannot perform division. For division obj1/obj2, only the following five situations are allowed: ' ...
           '(1) obj1 is an ''equ'' object, obj2 is a numeric scalar; ' ...
           '(2) obj1 is an ''equ'' object, obj2 is a ''func'' object; ' ...
           '(3) obj1 is a ''func'' object, obj2 is a ''func'' object; (4) obj1 is a ''func'' object, obj2 is a numeric scalar; ' ...
           '(5) obj1 is a numeric scalar, obj2 is a ''func'' object.  The divisor cannot be an ''equ'' object ' ...
           'because this program only accepts linearized equations.']);                               
    end
    
