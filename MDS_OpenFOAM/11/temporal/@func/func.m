function obj=func(function_,varargin)
    obj.coeff=1;
    obj.arrhandle=cell(0,0);
    obj.arrargument=cell(0,0);  
    obj.arrhandleden=cell(0,0);
    obj.arrargumentden=cell(0,0);        
    if isa(function_,'function_handle')
        obj.arrhandle{1}=function_;
        obj.arrargument{1}=varargin;
    elseif ischar(function_)
        obj.arrhandle{1}=inline(function_);
        if nargin(obj.arrhandle{1})~=1
            error('If your function includes parameters, please use function handle, not character array.');
        end
        obj.arrargument{1}=cell(0,0);
    else
        error('The argument ''function_'' should be a function handle or a character array.');
    end    
    obj=class(obj,'func');
    
