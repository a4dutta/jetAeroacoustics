function [arg1,arg2]=retrieve(obj,variable,eigenval)
   % Retrieve the results for the variables defined by addvar() and
   % addvarxt().
   %
   % Syntax
   %
   % value=retrieve(obj,variable,eigenval)
   % [arrvalue,arry]=retrieve(obj,variable,eigenval)
   %
   % Description
   % value=retrieve(obj,variable,eigenval) retrieves the result for a
   % variable defined by addvarxt(). Since the variable does not
   % depend on y, the returned value is a scalar. 'eigenval' is one of the eigenvalues obtained
   % by calling eigenvalue().
   % [arrvalue,arry]=retrieve(obj,variable,eigenval) retrieves the result for a
   % variable defined by addvar(). Since the variable depends on y,
   % the returned value is a vector. There is also a second
   % returned value 'arry' containing the collocation points.
   checknumeric(obj,eigenval,'eigenval');
   checkscalar(obj,eigenval,'eigenval');
   if isempty(obj.arreigenvalue)
       error('The eigenvalue problem has not been solved yet. Please call compute() to solve the eigenvalue problem. (Note: After each call to setk(), the stored eigenvalues are automatically cleared. Therefore, after each call to setk(), you should call compute() re-compute the eigenvalues.)');
   end
   col_id=find(obj.arreigenvalue==eigenval);
   if isempty(col_id)
       errmsg=sprintf('%.20e is not a eigenvalue of the present problem',eigenval);
       error(errmsg);
   end
   if length(col_id)>1
       col_id=col_id(1);
   end           
   if ~isa(variable,'equ')
       error('The argument ''variable'' should be an object of the class ''equ''. If you are using a character array, please remove the single quotes.');
   end
   if get(variable,'isvar')==0
       error('The argument ''variable'' should be a variable created by the function addvar() or addvarxt().');
   end
   if get(variable,'intervalid')~=-1
       arg1=obj.arreigenvector(get(variable,'colrange'),col_id);
       arg2=obj.arrcollopoint{get(variable,'intervalid')};
   else
       if nargout>1
           error('The variable is a function of only x and t. Therefore there should be only one output argument.');
       end                   
       arg1=obj.arreigenvector(get(variable,'colrange'),col_id);
   end
   
