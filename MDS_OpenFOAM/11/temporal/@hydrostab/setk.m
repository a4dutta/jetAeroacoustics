function obj=setk(obj,k)
   % Set wavenumber.
   % k: The wavenumber.
   % Note: The normal mode adopted in this code is exp(i*k*x-i*omega*t).
   if obj.intervalset==0
       error('You should call set_interval() before you call setk().');
   end
   checkintervalset(obj);
   checkscaleset(obj);           
   checknumeric(obj,k,'k');
   checkscalar(obj,k,'k');
   checknargout(obj,nargout);                      
   obj.k=k;
   obj.kset=1;
   % Since the value of k has been changed, the matrix is no longer
   % valid.           
   obj.matrix=[];
   obj.matrixb=[];
   obj.matrixset=0;           
   % clear the variables
   obj.arrvar={};
   obj.arrvardomain=[];
   obj.arrunknownidstart=[];
   obj.arrunknownidend=[];
   obj.arrvarxt={};
   obj.arrunknownidxt=[];          
   obj.varadded=0;         
   obj.nunknowns=0;
   % Since the value of k has been changed, the stored eigenvalues are no
   % longer valid.
   obj.arreigenvalue=[];
   
