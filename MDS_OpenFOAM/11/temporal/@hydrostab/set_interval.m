function obj=set_interval(obj,number,start,terminate,n_collocation)
   % set the properties of each interval.
   % number: sequence number of the interval
   % start: start point
   % terminate: end point
   % n_collocation: number of collocation points
   checknumeric(obj,number,'number');
   checkscalar(obj,number,'number');
   checknumeric(obj,start,'start');
   checkscalar(obj,start,'start');
   checknumeric(obj,terminate,'terminate');
   checkscalar(obj,terminate,'terminate');
   checknumeric(obj,n_collocation,'n_collocation');
   checkscalar(obj,n_collocation,'n_collocation');
   if obj.scaleset==1
       error('You cannot call set_interval() after you have called setscale().');
   end
   if obj.kset==1
       error('You cannot call set_interval() after you have called setk().');               
   end
   checknargout(obj,nargout);
   if number>obj.ninterval || number<1
       error('The argument ''number'' is out of range.');
   end
   if start>=terminate
       error('''start'' must be less than ''terminate''.');
   end
   obj.arrinterval(1,number)=start;
   obj.arrinterval(2,number)=terminate;
   if round(n_collocation)~=n_collocation
       errmsg=sprintf('n_collocation=%.20e is not an integer. Already moved to n_collocation=%d',n_collocation,round(n_collocation));
       n_collocation=round(n_collocation);
       warning(errmsg);
   end
   if n_collocation<2
       error('The number of collocation points must be greater than or equal to 2');
   end
   switch n_collocation
       case 2
           warnmsg=sprintf('Only two collocation points for Interval %d. You are not allowed to compute sixth-order, fifth-order, fourth-order, third-order and second-order derivative with respect to y in this interval.',number);
           warning(warnmsg);                                                                            
       case 3
           warnmsg=sprintf('Only three collocation points for Interval %d. You are not allowed to compute sixth-order, fifth-order, fourth-order and third-order derivative with respect to y in this interval.',number);
           warning(warnmsg);                                                         
       case 4
           warnmsg=sprintf('Only four collocation points for Interval %d. You are not allowed to compute sixth-order, fifth-order and fourth-order derivative with respect to y in this interval.',number);
           warning(warnmsg);                      
       case 5
           warnmsg=sprintf('Only five collocation points for Interval %d. You are not allowed to compute sixth-order and fifth-order derivative with respect to y in this interval.',number);
           warning(warnmsg);                                      
       case 6
           warnmsg=sprintf('Only six collocation points for Interval %d. You are not allowed to compute sixth-order derivative with respect to y in this interval.',number);
           warning(warnmsg);                                                         
       otherwise
           % do nothing
   end
   obj.arrncoll(number)=n_collocation;           
   if abs(start)==inf && abs(terminate)==inf
       obj.arrtype(number)=3; % infinite interval, use Hermite method
   elseif abs(start)==inf || abs(terminate)==inf
       obj.arrtype(number)=2; % semi-infinite interval, use Laguerre method
   else
       obj.arrtype(number)=1;  % Chebyshev method
       % compute differentiation matrices and collocation points
       [collopoint, DM] = chebdif(n_collocation, min(n_collocation-1,6)); % maximum: 6th order differentiation
       collopoint_new=(start+terminate)/2-collopoint*(terminate-start)/2;
       DMnew=DM;
       for i=1:size(DM,3)
           DMnew(:,:,i)=DMnew(:,:,i)/(-(terminate-start)/2)^i;
       end
       % test
%                yy=exp(2*collopoint_new);
%                dyy=DMnew(:,:,1)*yy;
%                d2yy=DMnew(:,:,2)*yy;
%                d3yy=DMnew(:,:,3)*yy;
%                d4yy=DMnew(:,:,4)*yy;               
       obj.arrdiffmatrix{number}=DMnew;
       obj.arrcollopoint{number}=collopoint_new;
   end
   obj.intervalset=1;
   obj.arrintervalset(number)=1;
   
