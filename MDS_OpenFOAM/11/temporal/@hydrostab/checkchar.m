function checkchar(obj,variable,name)
   if ~ischar(variable)
       errmsg=sprintf('The argument ''%s'' must be a character array (a string).',name);               
       error(errmsg);
   end                      
   
