function display(obj)
   % display the properties of the object in the command window
   fprintf('-------------object of class ''hydrostab''-------------------\n');           
   fprintf('Number of intervals: %d\n',obj.ninterval);
   fprintf('%2s  %41s  %16s  %10s  %19s\n','#','Interval','Number of points','Method','Scale');           
   for i=1:obj.ninterval
       if obj.arrtype(i)==1
           method='Chebyshev';
           scalestr='N/A';
       elseif obj.arrtype(i)==2
           method='Laguerre';
           scalestr=sprintf('%e',obj.arrscale(i));
       elseif obj.arrtype(i)==3
           method='Hermite';
           scalestr=sprintf('%e',obj.arrscale(i));                   
       else
           method='Unknown';
           scalestr='N/A';
       end          
       fprintf('%2d  [%19e,%19e]  %16d  %10s  %19s\n',i,obj.arrinterval(1,i),obj.arrinterval(2,i),obj.arrncoll(i),method,scalestr);
   end
   fprintf('Number of variables: %d\n',length(obj.arrvar)+length(obj.arrvarxt));
   fprintf('Number of unknowns: %d\n',obj.nunknowns);           
   fprintf('%2s  %10s  %41s  %13s\n','#','Name','Interval','Unknown ID');
   for i=1:length(obj.arrvar)
       fprintf('%2d  %10s  [%19e,%19e]  %6d-%6d\n',i,obj.arrvar{i},obj.arrinterval(1,obj.arrvardomain(i)),obj.arrinterval(2,obj.arrvardomain(i)), ...
           obj.arrunknownidstart(i),obj.arrunknownidend(i));
   end
   for i=1:length(obj.arrvarxt)
       fprintf('%2d  %10s  %41s  %13d\n',i+length(obj.arrvar),obj.arrvarxt{i},'N/A',obj.arrunknownidxt(i));               
   end           
   fprintf('Wavenumber: k=%e\n',obj.k);
   fprintf('Number of equations: %d\n',size(obj.matrix,1));           
   
