function obj=compute(obj)
   % Solve the eigenvalue problem.
   %
   % Syntax
   % 
   % obj=compute(obj)
   checknargout(obj,nargout);                                 
   if obj.matrixset==0
       error('The matrix for computing eigenvalues has not been set. Please use addeq() to add equations. (Note: After each call to setk(), the matrix is automatically cleared. Therefore, after each call to setk(), you should reset all the equations.)');
   end
   if size(obj.matrix,1)<obj.nunknowns
       errmsg=sprintf('Number of equation=%d, number of unknown=%d, number of equation is less than number of unknown. Please use addeq() to add extra equations.', ...
           size(obj.matrix,1),obj.nunknowns);
       error(errmsg);
   end
   if size(obj.matrix,1)>obj.nunknowns
       errmsg=sprintf('Number of equation=%d, number of unknown=%d, number of equation is greater than number of unknown. Please ignore some equations at some endpoints by using the ''option'' argument of the function addeq().', ...
           size(obj.matrix,1),obj.nunknowns);
       error(errmsg);
   end           
   if sum(sum(obj.matrixb==0))==size(obj.matrixb,1)*size(obj.matrixb,2)
       error('Your problem contains no time derivative. This is not allowed.');
   end
   [eigenvec,D]=eig(obj.matrix,obj.matrixb);
   omega=diag(D);     
   % store the eigenvalues and eigenvectors
   obj.arreigenvalue=omega;
   obj.arreigenvector=eigenvec;
   
