function obj3=eq(obj1,obj2)
    if isa(obj1,'equ')&&isa(obj2,'equ')
        obj3=obj1-obj2;
    elseif isa(obj1,'equ')&&isnumeric(obj2)&&obj2==0
        obj3=obj1;                
    else
        error('The operand on the left side of ''=='' must be an ''equ'' object. The operand on the right side of ''=='' must be an ''equ'' object or zero.');
    end
    
