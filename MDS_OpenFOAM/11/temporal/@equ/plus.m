function obj3=plus(obj1,obj2)
   if (~isa(obj1,'equ'))||(~isa(obj2,'equ'))
       error('Cannot perform addition. Both operands should be ''equ'' objects. Make sure that your equation has been linearized and simplified.');
   end
   if obj1.intervalid~=obj2.intervalid
       error(['Cannot perform addition becasue the two ''equ'' objects are defined in different intervals, ' ...
           'or, one of them is defined at boundary but another is defined in an interval.']);
   end
   maxcolid=max(max(obj1.colrange),max(obj2.colrange));
   nrow=size(obj1.matrixa,1); % number of rows
   m1=zeros(nrow,maxcolid);
   m1(:,obj1.colrange)=obj1.matrixa;
   m1(:,obj2.colrange)=m1(:,obj2.colrange)+obj2.matrixa;
   m2=zeros(nrow,maxcolid);
   m2(:,obj1.colrange)=obj1.matrixb;
   m2(:,obj2.colrange)=m2(:,obj2.colrange)+obj2.matrixb;
   obj3=equ(obj1.intervalid,1:maxcolid,[],0,m1,m2,[],obj1.collopoint,obj1.funcvectorized);
   
