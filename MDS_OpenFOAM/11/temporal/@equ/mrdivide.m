function obj=mrdivide(obj,a)
   if isa(obj,'equ')&&isnumeric(a)     
       siz=size(a);
       if siz(1)~=1||siz(2)~=1
           error('If the second operand is numeric, it must be a scalar.');
       end                                         
       obj.matrixa=obj.matrixa/a;
       obj.matrixb=obj.matrixb/a;   
   elseif isa(obj,'equ')&&isa(a,'func')   
       fval=compute(a,obj.collopoint,obj.funcvectorized);       
       m1=diag(1./fval);
       obj.matrixa=m1*obj.matrixa;
       obj.matrixb=m1*obj.matrixb;
   else
       error(['Cannot perform division. For division obj1/obj2, only the following five situations are allowed: ' ...
           '(1) obj1 is an ''equ'' object, obj2 is a numeric scalar; ' ...
           '(2) obj1 is an ''equ'' object, obj2 is a ''func'' object; ' ...
           '(3) obj1 is a ''func'' object, obj2 is a ''func'' object; (4) obj1 is a ''func'' object, obj2 is a numeric scalar; ' ...
           '(5) obj1 is a numeric scalar, obj2 is a ''func'' object.  The divisor cannot be an ''equ'' object ' ...
           'because this program only accepts linearized equations.']);               
   end
   obj.diffmatrix=[];
   obj.isvar=0;
   obj.k=[];           
   
