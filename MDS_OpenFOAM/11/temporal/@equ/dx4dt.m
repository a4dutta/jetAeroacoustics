       function obj=dx4dt(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixb=1i*(1i*obj.k)^4*eye(length(obj.colrange));
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
