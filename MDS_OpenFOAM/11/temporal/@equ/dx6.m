       function obj=dx6(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^6*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
