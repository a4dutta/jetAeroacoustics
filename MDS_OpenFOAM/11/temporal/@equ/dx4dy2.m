       function obj=dx4dy2(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^4*obj.diffmatrix(:,:,2);
           obj.diffmatrix=[];
           obj.k=[];
