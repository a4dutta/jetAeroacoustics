       function obj=dx2dy2(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^2*obj.diffmatrix(:,:,2);
           obj.diffmatrix=[];
           obj.k=[];
