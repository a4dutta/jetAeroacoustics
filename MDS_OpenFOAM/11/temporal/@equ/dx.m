       function obj=dx(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^1*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
