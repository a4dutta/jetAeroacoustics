       function obj=dx4(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^4*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
