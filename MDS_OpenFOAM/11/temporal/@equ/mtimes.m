function obj=mtimes(obj1,obj2)
   if isnumeric(obj1)&&isa(obj2,'equ')
       siz=size(obj1);
       if siz(1)~=1||siz(2)~=1
           error('If the first operand of the multiplication is numeric, it must be a scalar.');
       end               
       obj=obj2;
       obj.matrixa=obj.matrixa*obj1;  
       obj.matrixb=obj.matrixb*obj1;                          
   elseif isnumeric(obj2)&&isa(obj1,'equ')
       siz=size(obj2);
       if siz(1)~=1||siz(2)~=1
           error('If the second operand of the multiplication is numeric, it must be a scalar.');
       end               
       obj=obj1;
       obj.matrixa=obj.matrixa*obj2;
       obj.matrixb=obj.matrixb*obj2; 
   elseif isa(obj2,'func')&&isa(obj1,'equ')
       fval=compute(obj2,obj1.collopoint,obj1.funcvectorized);
       m1=diag(fval);
       obj=obj1;
       obj.matrixa=m1*obj.matrixa;
       obj.matrixb=m1*obj.matrixb;
   else
       error(['Cannot perform multiplication. For multiplication obj1*obj2, only the following seven situations are allowed: ' ...
           '(1) obj1 is a numeric scalar, obj2 is an ''equ'' object; (2) obj1 is an ''equ'' object, obj2 is a numeric scalar; ' ...
           '(3) obj1 is a ''func'' object, obj2 is an ''equ'' object; (4) obj1 is an ''equ'' object, obj2 is a ''func'' object; ' ...
           '(5) obj1 is a ''func'' object, obj2 is a ''func'' object; (6) obj1 is a ''func'' object, obj2 is a numeric scalar; ' ...
           '(7) obj1 is a numeric scalar, obj2 is a ''func'' object.  The situation that both obj1 and obj2 are ''equ'' object ' ...
           'is not allowed because this program only accepts linearized equations.']);
   end
   obj.diffmatrix=[];
   obj.isvar=0;
   obj.k=[];
   
