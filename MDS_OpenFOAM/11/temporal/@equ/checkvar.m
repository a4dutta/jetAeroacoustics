function checkvar(obj)
  if obj.isvar==0
       error(['Cannot compute derivative since the object is not a variable created by addvar() or addvarxt(). ' ...
           'You cannot compute the derivative of an object obtained by some other operations. ' ...
           'For example, the following expression is invalid: dx(u+p). The solution is to rewrite the expression as: dx(u)+dx(p).']);
  end           
  
