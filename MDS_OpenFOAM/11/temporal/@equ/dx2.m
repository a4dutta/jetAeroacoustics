       function obj=dx2(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^2*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
