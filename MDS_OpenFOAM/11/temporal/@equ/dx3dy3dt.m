       function obj=dx3dy3dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixb=1i*(1i*obj.k)^3*obj.diffmatrix(:,:,3);
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
