       function obj=dx3(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^3*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
