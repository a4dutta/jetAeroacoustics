       function obj=dy(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=1*obj.diffmatrix(:,:,1);
           obj.diffmatrix=[];
           obj.k=[];
