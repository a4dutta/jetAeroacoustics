function checknumeric(obj,variable,name)
   if ~isnumeric(variable)
       errmsg=sprintf('The argument ''%s'' must be numeric.',name);
       error(errmsg);
   end           
   
