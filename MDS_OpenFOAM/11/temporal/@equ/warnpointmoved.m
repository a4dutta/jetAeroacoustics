function warnpointmoved(obj,y,ymoved)
   if y~=ymoved
       warnmsg=sprintf('y=%.20e is not equal to any collocation points. It has been moved to the nearest collocation point, y=%.20e',y,ymoved);
       warning(warnmsg);
   end
   
