       function obj=dy4(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=1*obj.diffmatrix(:,:,4);
           obj.diffmatrix=[];
           obj.k=[];
