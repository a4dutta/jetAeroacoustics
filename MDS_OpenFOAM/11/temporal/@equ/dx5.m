       function obj=dx5(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^5*eye(length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
