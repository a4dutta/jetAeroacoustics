       function obj=dx2dy4(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^2*obj.diffmatrix(:,:,4);
           obj.diffmatrix=[];
           obj.k=[];
