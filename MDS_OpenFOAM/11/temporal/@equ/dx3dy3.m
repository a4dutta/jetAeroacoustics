       function obj=dx3dy3(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^3*obj.diffmatrix(:,:,3);
           obj.diffmatrix=[];
           obj.k=[];
