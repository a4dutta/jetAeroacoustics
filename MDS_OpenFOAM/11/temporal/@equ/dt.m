       function obj=dt(obj)
           checkvar(obj);
           obj.isvar=0;
           obj.matrixb=1i*1*eye(length(obj.colrange));
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
