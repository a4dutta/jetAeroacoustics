       function obj=dx5dy(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixa=(1i*obj.k)^5*obj.diffmatrix(:,:,1);
           obj.diffmatrix=[];
           obj.k=[];
