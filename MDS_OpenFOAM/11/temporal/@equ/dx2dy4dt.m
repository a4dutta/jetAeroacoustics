       function obj=dx2dy4dt(obj)
           checkvar(obj);
           checkintervalid(obj);
           obj.isvar=0;
           obj.matrixb=1i*(1i*obj.k)^2*obj.diffmatrix(:,:,4);
           obj.matrixa=zeros(length(obj.colrange),length(obj.colrange));
           obj.diffmatrix=[];
           obj.k=[];
