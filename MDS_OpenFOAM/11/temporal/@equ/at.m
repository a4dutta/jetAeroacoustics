function obj = at(obj,y)
  checknumeric(obj,y,'y');
  checkscalar(obj,y,'y');
   if obj.intervalid==-1
       error('''obj'' is defined at boundary. It is function of only x and t.');
   end                     
  [minval,minid]=min(abs(obj.collopoint-y)); % find the nearest collocation point
  warnpointmoved(obj,y,obj.collopoint(minid));
  if minid~=1 && minid~=length(obj.collopoint)
      error('y must located at the boundary of the interval.');
  end
  obj.intervalid=-1;
  obj.diffmatrix=[];
  obj.isvar=0;
  obj.k=[];
  obj.collopoint=[];
  obj.matrixa=obj.matrixa(minid,:);
  obj.matrixb=obj.matrixb(minid,:);          
  
