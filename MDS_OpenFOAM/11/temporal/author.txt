% Hydrostab: a universal code for solving hydrodynamic stability problems
% Author: Hanyu Ye
% Beihang University (Beijing University of Aeronautics and
% Astronautics)
% January 2016
%
% This is a universal code for solving hydrodynamic stability problems.
%
% The normal mode adopted in this code is exp(i*k*x-i*omega*t).
%
% This software relies on the 'MATLAB Differentiation Matrix Suite'
% developed by Weideman and Reddy. You can find that software on MATLAB
% Central.