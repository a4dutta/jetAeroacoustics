clear
Re=1000;
We=400;
rho=0.001;
hs=hydrostab(3);
hs=set_interval(hs,1,-inf,-1,20);
hs=set_interval(hs,2,-1,1,20);
hs=set_interval(hs,3,1,inf,20);
hs=setscale(hs,1,100);
hs=setscale(hs,3,100);
nomega=50;
arromega=linspace(0,0.5,nomega);
for i=1:nomega
    hs=setomega(hs,arromega(i)); % frequency
    hs=addvar(hs,2,'u','v','p');
    hs=addvar(hs,3,'phig1');
    hs=addvar(hs,1,'phig2');
    hs=addvarxt(hs,'eta1','eta2');       
    % liquid phase
    hs=addeq(hs,dt(u)+dx(u)==-dx(p)+1/Re*(dx2(u)+dy2(u)),'ignorepossible');       
    hs=addeq(hs,dt(v)+dx(v)==-dy(p)+1/Re*(dx2(v)+dy2(v)),'ignorepossible');    
    hs=addeq(hs,dx(u)+dy(v)==0);
    % gas phase
    hs=addeq(hs,dx2(phig1)+dy2(phig1)==0,'ignorepossible');
    hs=addeq(hs,dx2(phig2)+dy2(phig2)==0,'ignorepossible');
    % boundary conditions
    hs=addeq(hs,at(v,1)==dt(eta1)+dx(eta1));
    hs=addeq(hs,at(v,-1)==dt(eta2)+dx(eta2));
    hs=addeq(hs,dyat(phig1,1)==dt(eta1));
    hs=addeq(hs,dyat(phig2,-1)==dt(eta2));
    hs=addeq(hs,-at(p,1)+2/Re*dyat(v,1)-1/We*dx2(eta1)-rho*dtat(phig1,1)==0);
    hs=addeq(hs,-at(p,-1)+2/Re*dyat(v,-1)+1/We*dx2(eta2)-rho*dtat(phig2,-1)==0);
    hs=addeq(hs,dyat(u,1)+dxat(v,1)==0);
    hs=addeq(hs,dyat(u,-1)+dxat(v,-1)==0);
    % compute eigenvalues
    hs=compute(hs);
    eige=eigenvalue(hs,1e8);   
    for j=1:length(eige)
        if real(retrieve(hs,eta1,eige(j))/retrieve(hs,eta2,eige(j)))>0 % sinuous
            plot(arromega(i),-imag(eige(j)),'r+');
        else % varicose
            plot(arromega(i),-imag(eige(j)),'b+');
        end
        hold on
    end
end
axis([0 0.5 0 0.011]);