#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 10:27:49 2018

@author: adutta
"""

import pandas as pd
import numpy as np
import re
import os

def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

def lamb_vec(filename,exp_filename,num):
    df = pd.read_csv(filename,skiprows=3,header=None,delim_whitespace=True)
    df = df[:-1]
    df.columns = ['x', 'y', 'z', 'u', 'v', 'w', 'p', 'Wx', 'Wy', 'Wz', 'Wx2', 'Wy2', 'Wz2']
    
    df.drop('Wz2',axis=1,inplace=True)
    df.drop('Wy2',axis=1,inplace=True)
    df.drop('Wx2',axis=1,inplace=True)
    
    length = len(df)
    L = []
    for i in range(length):
        V = [df.iloc[i]['u'], df.iloc[i]['v'], df.iloc[i]['w']]
        W = [df.iloc[i]['Wx'], df.iloc[i]['Wy'], df.iloc[i]['Wz']]
        L.append(np.cross(W,V))
    
    Lx = []
    Ly = []
    Lz = []
    
    for i in range(len(L)):
        Lx.append(L[i][0])
        Ly.append(L[i][1])
        Lz.append(L[i][2])    

    df['Lx'] = Lx
    df['Ly'] = Ly
    df['Lz'] = Lz
    
    df.drop('Wz',axis=1,inplace=True)
    df.drop('Wy',axis=1,inplace=True)
    df.drop('Wx',axis=1,inplace=True)
    df.drop('u',axis=1,inplace=True)
    df.drop('v',axis=1,inplace=True)
    df.drop('w',axis=1,inplace=True)
    
    df.to_csv(exp_filename+'_'+num+'.pts',sep=' ',header=False)
    
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<POINTS DIM="3" FIELDS="x,y,z,L_x,L_y,L_z" >\n' + old) # write the new line before
         
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<NEKTAR>\n' + old) # write the new line before
         
    with open(exp_filename+'_'+num+'.pts', "r+") as f:
         old = f.read() # read everything in the file
         f.seek(0) # rewind
         f.write('<?xml version="1.0" encoding="utf-8" ?>\n' + old) # write the new line before
           
    with open(exp_filename+'_'+num+'.pts', "a+") as f:
         f.write('</POINTS>\n') 
         
    with open(exp_filename+'_'+num+'.pts', "a+") as f:
         f.write('</NEKTAR>') 


files = os.listdir('./pts_folder')
os.chdir('./pts_folder')
files = natural_sorted(files)

i = 0
for f in files:
    lamb_vec(f,f+'lamb',str(i))
    i = i+1

os.chdir('../')
