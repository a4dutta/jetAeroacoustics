#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 17:58:45 2019

@author: adutta
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import pickle
import os
from natsort import natsorted

os.chdir('./pts_data_latest')
Qm = []
Rm = []
Qsm = []
Rsm = []
Wwm = []
probes = [0,1,2,3,4,5,6,7,8,9,10]
for probe in probes:
#filewritename = "nxs14_line1.p"
    As3 = pickle.load(open("p_data_pts2.p","rb"))
    #probe = 0
    As4 = As3[probe]
    ntime = len(As4)
#A = [[dudx,dudy,dudz],[dvdx,dvdy,dvdz],[dwdx,dwdy,dwdz]]
    A = As4
    A = np.asarray(A)
    A2 = []
    for a in A:
        A2.append(np.dot(a,a))
    A2 = np.asarray(A2)
    A3 = []
    for (a2,a) in zip(A2,A):
        A3.append(np.dot(a2,a))
    A3 = np.asarray(A3)
    At = []
    for i in range(ntime):
        At.append(np.transpose(A[i,:,:]))
    At = np.asarray(At)
    S = 0.5*(A+At)
    S2 = []
    for s in S:
        S2.append(np.dot(s,s))
    S2 = np.asarray(S2)
    S3 = []
    for (s2,s) in zip(S2,S):
        S3.append(np.dot(s2,s))
    S3 = np.asarray(S3)
    W = 0.5*(A-At)
    W2 = []
    for w in W:
        W2.append(np.dot(w,w))
    W2 = np.asarray(W2)

    Q = []
    R = []
    Qs = []
    Rs = []
    Ww = []
    St = []

    for a2 in A2:
        Q.append(-0.5*np.trace(a2))
    Q = np.asarray(Q)
    for a3 in A3:
        R.append(-0.33*np.trace(a3))
    R = np.asarray(R)
    for s2 in S2:
        Qs.append(-0.5*np.trace(s2))
    Qs = np.asarray(Qs)
    for s3 in S3:
        Rs.append(-0.33*np.trace(s3))
    Rs = np.asarray(Rs)
    for w2 in W2:
        Ww.append(-0.5*np.trace(w2))
    Ww = np.asarray(Ww)

    for s2 in S2:
        St.append(np.trace(s2))
    St = np.asarray(St)
    Stm = np.mean(St)


        
    Qm.append(Q/Stm)
    Rm.append(R/np.power(Stm,1.5))
    Qsm.append(Qs/Stm)
    Rsm.append(Rs/np.power(Stm,1.5))
    Wwm.append(Ww/Stm)

pickle.dump([Qm,Rm,Qsm,Rsm,Wwm],open('pts2_proc_data.p', "wb" ))
os.chdir('../')