#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 09:06:01 2018

@author: adutta
"""

import numpy as np
import pandas as pd
import os
import re

def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

def nektar_grad(f_name):
    df = pd.read_csv(f_name,skiprows=3,header=None,delim_whitespace=True)
    df.columns = ['x','y','z','u','v','w','p']
    x0 = 6; y0 = 0.5; z0 = 0.5
    dx = 0.01; dy = 0.01; dz = 0.01
    x1 = x0 - dx ; x2 = x0 + dx;
    y1 = y0 - dy ; y2 = y0 + dy;
    z1 = z0 - dz ; z2 = z0 + dz;
    dx_values = []
    dy_values = []
    dw_values = []
    for i in df.index:
        if df.iloc[i].x==x0 and df.iloc[i].y==y1 and df.iloc[i].z==z0:
            dy_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])
    for i in df.index:
        if df.iloc[i].x==x0 and df.iloc[i].y==y2 and df.iloc[i].z==z0:
            dy_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])    
    dudy = (dy_values[1][0] - dy_values[0][0])/(2*dy)
    dvdy = (dy_values[1][1] - dy_values[0][1])/(2*dy)
    dwdy = (dy_values[1][2] - dy_values[0][2])/(2*dy)
    
    for i in df.index:
        if df.iloc[i].x==x1 and df.iloc[i].y==y0 and df.iloc[i].z==z0:
            dx_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])
    for i in df.index:
        if df.iloc[i].x==x2 and df.iloc[i].y==y0 and df.iloc[i].z==z0:
            dx_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])    
    dudx = (dx_values[1][0] - dx_values[0][0])/(2*dx)
    dvdx = (dx_values[1][1] - dx_values[0][1])/(2*dx)
    dwdx = (dx_values[1][2] - dx_values[0][2])/(2*dx)
    
    for i in df.index:
        if df.iloc[i].x==x0 and df.iloc[i].y==y0 and df.iloc[i].z==z1:
            dw_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])
    for i in df.index:
        if df.iloc[i].x==x0 and df.iloc[i].y==y0 and df.iloc[i].z==z2:
            dw_values.append([df.iloc[i].u,df.iloc[i].v,df.iloc[i].w,df.iloc[i].p])    
    dudz = (dw_values[1][0] - dw_values[0][0])/(2*dz)
    dvdz = (dw_values[1][1] - dw_values[0][1])/(2*dz)
    dwdz = (dw_values[1][2] - dw_values[0][2])/(2*dz)
    dudx = np.asarray(dudx); dudy = np.asarray(dudy); dudz = np.asarray(dudz)
    dvdx = np.asarray(dvdx); dvdy = np.asarray(dvdy); dvdz = np.asarray(dvdz)
    dwdx = np.asarray(dwdx); dwdy = np.asarray(dwdy); dwdz = np.asarray(dwdz)
    A = [[dudx,dudy,dudz],[dvdx,dvdy,dvdz],[dwdx,dwdy,dwdz]]
    A = np.asarray(A)
    return(A)

files = os.listdir('./nektar_pp_k/')
files = natural_sorted(files)
As = []
for f in files:
    A = nektar_grad('./nektar_pp_k/'+f)
    As.append(A)
#A = np.asarray(As)
  