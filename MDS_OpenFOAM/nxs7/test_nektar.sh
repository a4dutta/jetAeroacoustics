#!/bin/bash
#SBATCH --nodes=4
#SBATCH --ntasks=160              # number of MPI processes
#SBATCH --time=0-04:00           # time (DD-HH:MM)
 



# Set up the environment


export PATH=$HOME/nektar++-4.4.1v02/nektar++-4.4.1/build/dist/bin:$PATH

module load gcc

module load openmpi

module load cmake

module load boost

module load mkl


mpirun -np 160 IncNavierStokesSolver nxs7.xml > a.out

echo "Finished at `date`"
