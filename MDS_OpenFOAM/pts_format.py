#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 22 13:54:16 2018

@author: adutta
"""


import os
import re

def natural_sorted(iterable, key=None, reverse=False):
    prog = re.compile(r"(\d+)")

    def alphanum_key(element):
        """Split given key in list of strings and digits"""
        return [int(c) if c.isdigit() else c for c in prog.split(key(element)
                if key else element)]

    return sorted(iterable, key=alphanum_key, reverse=reverse)

files = os.listdir('./nektar_pp_grad')
os.chdir('./nektar_pp_grad')
files = natural_sorted(files)
os.rename(files[0],'nxs12_0.00000000E+00.pts')

files2 = files[1:]

for f,i in zip(files2,range(1,len(files2))):
    os.rename(f,'nxs12_'+str(i)+'.00000000E-05.pts')    
os.chdir('../')