# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 19:54:06 2018

@author: AvD
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import pickle

As3 = pickle.load(open("p_data_line1.p","rb"))
probe = 3
As4 = As3[probe]
ntime = len(As4)
#A = [[dudx,dudy,dudz],[dvdx,dvdy,dvdz],[dwdx,dwdy,dwdz]]
A = As4
A = np.asarray(A)
A2 = []
for a in A:
    A2.append(np.dot(a,a))
A2 = np.asarray(A2)
A3 = []
for (a2,a) in zip(A2,A):
    A3.append(np.dot(a2,a))
A3 = np.asarray(A3)
At = []
for i in range(ntime):
    At.append(np.transpose(A[i,:,:]))
At = np.asarray(At)
S = 0.5*(A+At)
S2 = []
for s in S:
    S2.append(np.dot(s,s))
S2 = np.asarray(S2)
S3 = []
for (s2,s) in zip(S2,S):
    S3.append(np.dot(s2,s))
S3 = np.asarray(S3)
W = 0.5*(A-At)
W2 = []
for w in W:
    W2.append(np.dot(w,w))
W2 = np.asarray(W2)

Q = []
R = []
Qs = []
Rs = []
Ww = []
St = []

for a2 in A2:
    Q.append(-0.5*np.trace(a2))
Q = np.asarray(Q)
for a3 in A3:
    R.append(-0.33*np.trace(a3))
R = np.asarray(R)
for s2 in S2:
    Qs.append(-0.5*np.trace(s2))
Qs = np.asarray(Qs)
for s3 in S3:
    Rs.append(-0.33*np.trace(s3))
Rs = np.asarray(Rs)
for w2 in W2:
    Ww.append(-0.5*np.trace(w2))
Ww = np.asarray(Ww)

for s2 in S2:
    St.append(np.trace(s2))
St = np.asarray(St)
Stm = np.mean(St)

Qm = Q/Stm
Rm = R/np.power(Stm,1.5)
Qsm = Qs/Stm
Rsm = Rs/np.power(Stm,1.5)
Wwm = Ww/Stm

pickle.dump([Q,R,Qs,Rs,Ww],open( "nxs14_line1.p", "wb" ))
#Qs_ls = Qs[0:ntime:5]
#Rs_ls = Rs[0:ntime:5]
#Ww_ls = Ww[0:ntime:5]

data = [-1*Qm,Rm]

sandR = np.max(np.abs(data[1]))
sandQ = np.max(np.abs(data[0]))
xi = np.linspace(-sandR/10,sandR/10,100)
yi = np.linspace(-sandQ/10,sandQ/10,100)
hst = np.histogramdd(data,bins=[xi,yi])

xx = hst[1][0]
yy = hst[1][1]

xx = xx[0:99]
yy = yy[0:99]
[X,Y] = np.meshgrid(xx,yy)
plt.contour(X,Y,hst[0])

plt.hold('True')
Qsmp1 = (27/4)*(xx*xx)
Qsmp2 = -1*np.cbrt(Qsmp1)
plt.plot(xx,Qsmp2)