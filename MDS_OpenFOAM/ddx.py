#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 18:34:06 2018

@author: adutta
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import pickle

As3 = pickle.load(open("p_data.p","rb"))
dudx_mean = []
dudy_mean = []
dvdx_mean = []
dvdy_mean = []
for A in As3:
    dd1 = []
    for As in A:
        dd1.append(As[0][0])
    dd1 = np.asarray(dd1)
    d_mean = np.mean(dd1)
    dudx_mean.append(d_mean)

for A in As3:
    dd2 = []
    for As in A:
        dd2.append(As[0][1])
    dd2 = np.asarray(dd2)
    d_mean = np.mean(dd2)
    dudy_mean.append(d_mean)

for A in As3:
    dd1 = []
    for As in A:
        dd1.append(As[1][0])
    dd1 = np.asarray(dd1)
    d_mean = np.mean(dd1)
    dvdx_mean.append(d_mean)
    
for A in As3:
    dd1 = []
    for As in A:
        dd1.append(As[1][1])
    dd1 = np.asarray(dd1)
    d_mean = np.mean(dd1)
    dvdy_mean.append(d_mean)

dvdy_mean = np.asarray(dvdy_mean)

x = np.linspace(0,9,10)
plt.scatter(x,(dvdy_mean*0.01/10))
plt.ylabel('Normalised Streamnormal Gradient')
#plt.ylim(-0.0025, 0.0025)