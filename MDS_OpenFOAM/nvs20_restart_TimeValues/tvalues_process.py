#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 11:07:49 2019

@author: adutta
"""
from scipy import stats as st

data_n = 1
p_data_u = np.hstack((data_1[data_n],data_2[data_n],data_3[data_n]))
p_data_u_mean = np.mean(p_data_u)
p_data_u_fluc = p_data_u - p_data_u_mean



p_mav_u = []
window = 2000
for i in range(0,int(len(cent_p_data_u[1])-window+1)):
    p_temp = cent_p_data_u[1][int(i):int(i+window)]
    #p_mav_u.append(np.mean(p_temp))
    p_mav_u.append(st.kurtosis(p_temp))
p_mav_u = np.asarray(p_mav_u)

#p_plot_mean = p_data_u_mean*np.ones(len(p_mav_u))
p_plot_mean = st.kurtosis(cent_p_data_u[1])*np.ones(len(p_mav_u))

plt.plot(p_mav_u)
plt.hold('on')
plt.plot(p_plot_mean)