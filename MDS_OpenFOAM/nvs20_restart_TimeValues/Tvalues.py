#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 15:56:34 2019

@author: adutta
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

def read_Timevalues(n_probes,n_probe_number,filename):
    #n_probes = 53
    pd_1 =  pd.read_table(filename,sep=' ',skiprows=np.linspace(1,n_probes,n_probes))
    pd_1 = pd_1.dropna(axis=1)
    pd_1.columns = ['Time','u','v','w','p']

    p_data_p = []
    p_data_u = []
    p_data_v = []
    p_data_w = []

    #n_probe_number = 4
    for i in range(n_probe_number,int(len(pd_1)),n_probes):
        p_data_u.append(pd_1.iloc[i].u)
        p_data_v.append(pd_1.iloc[i].v)
        p_data_w.append(pd_1.iloc[i].w)
        p_data_p.append(pd_1.iloc[i].p)

    p_data_p = np.asarray(p_data_p)
    p_data_u = np.asarray(p_data_u)
    p_data_v = np.asarray(p_data_v)
    p_data_w = np.asarray(p_data_w)
    return(p_data_p,p_data_u,p_data_v,p_data_w)

cent_p_data_u = []
for probe_number in range(0,10):
    data_1 = read_Timevalues(53,probe_number,'TimeValues2.his')
    data_2 = read_Timevalues(53,probe_number,'TimeValues3.his')
    data_3 = read_Timevalues(53,probe_number,'TimeValues.his')
    data_n = 1
    p_data_u_temp = np.hstack((data_1[data_n],data_2[data_n],data_3[data_n]))
    cent_p_data_u.append(p_data_u_temp)
cent_p_data_u = np.asarray(cent_p_data_u)
