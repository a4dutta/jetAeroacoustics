function [x2shift,y2shift,corr_u,corr_v] = stitchoverlap(x1,y1,u1,v1,x2,y2,u2,v2,delx,window)
% INPUTS
% window: [left, right, down, up], shifts the current x and y  coordinates
% within this window [m]
% delx: resolution of shift (usually use your vector pitch) [m]
% 
% OUTPUTS
% once finished, add xshift and yshift to x1 and y1 to correct your
% coordinate system for misalignment, inspect corr_u and corr_v and ensure
% you have captured the correlation peak
% 
% 
% Create 2D space for stitched image---------------------------------------
for i = 1:floor((window(2) - window(1))/delx)
    for j = 1:floor((window(4) - window(3))/delx)
        disp((i-1)/floor((window(2) - window(1))/delx) + (j-1)/floor((window(4) - window(3))/delx)/floor((window(2) - window(1))/delx))
        % amount second camera coords are shifted
        xshift =  window(1) + delx*(i - 1);      
        yshift =  window(3) + delx*(j - 1);  
        x2shifted = x2 - xshift;
        y2shifted = y2 - yshift;
        
        % spatial resolution match to most coarse
        dx = max( abs([x1(1, 2) - x1(1, 1) x2shifted(1, 2) - x2shifted(1, 1)]) );
        dy = max( abs([y1(2, 1) - y1(1, 1) y2shifted(2, 1) - y2shifted(1, 1)]) );

        xmin = min( [min(x1(:)) min(x2shifted(:))] );
        xmax = max( [max(x1(:)) max(x2shifted(:))] );
        xmax = xmax - dx*((xmax-xmin)/dx - floor((xmax-xmin)/dx));
        ymin = min( [min(y1(:)) min(y2shifted(:))] );
        ymax = max( [max(y1(:)) max(y2shifted(:))] );
        ymax = ymax - dy*((ymax-ymin)/dy - floor((ymax-ymin)/dy));

        [x, y] = meshgrid( linspace(xmin, xmax, round(abs(xmax-xmin)/dx+1)),...
                   linspace(ymin, ymax, round(abs(ymax-ymin)/dy+1)));
                              
         % Region of overlap--------------------------------------------------------
        Ab = x(1, :) - min( [max(x1(:)) max(x2shifted(:))] );
        Ab(Ab > +1e-10) = Inf;
        [~, xov2] = min(abs(Ab));
        Ac = x(1, :) - max( [min(x1(:)) min(x2shifted(:))] );
        Ac(Ac < -1e-10) = Inf;
        [~, xov1] = min(abs(Ac));
        Ad = y(:, 1) - max( [min(y1(:)) min(y2shifted(:))] );
        Ad(Ad < -1e-10) = Inf;
        [~, yov1] = min(abs(Ad));
        Ae = y(:, 1) - min( [max(y1(:)) max(y2shifted(:))] );
        Ae(Ae > +1e-10) = Inf;
        [~, yov2] = min(abs(Ae));

        % hard coded stuff -------------------------------------
        for k = 1:size(u1,3)
        %    Mask1 = cylinder_mask(x1, y1, [0 y_encoder_filt(k)*1000], [50 -4], 19.05/2);
        %    Mask2 = cylinder_mask(x2shifted, y2shifted, [0 y_encoder_filt(k)*1000], [-50 -4], 19.05/2);
        %    Mask2(x2 < 0) = 1; Mask2(x2shifted.^2 + (y2shifted - y_encoder_filt(k)*1000).^2 <= (19.05/2)^2) = 0;
        %    Mask1(x1 >= 0) = 1; Mask1(x1.^2 + (y1 - y_encoder_filt(k)*1000).^2 <= (19.05/2)^2) = 0;
        
            uov1(:,:,k) = interp2(x1, y1, u1(:,:,k), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'linear');
            vov1(:,:,k) = interp2(x1, y1, v1(:,:,k), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'linear');
            %Mask1ov(:,:,k) = interp2(x1, y1, single(Mask1), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'nearest',1);

            uov2(:,:,k) = interp2(x2shifted, y2shifted, u2(:,:,k), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'linear');
            vov2(:,:,k) = interp2(x2shifted, y2shifted, v2(:,:,k), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'linear');
            %Mask2ov(:,:,k) = interp2(x2shifted, y2shifted, single(Mask2), x(yov1:yov2, xov1:xov2), y(yov1:yov2, xov1:xov2), 'nearest',1);
            %disp(k)
        end
        
        %uov1(Mask1ov ~= 1 | Mask2ov ~= 1) = nan; vov1(Mask1ov ~= 1 | Mask2ov ~= 1) = nan;
        %uov2(Mask1ov ~= 1 | Mask2ov ~= 1) = nan; vov2(Mask1ov ~= 1 | Mask2ov ~= 1) = nan;
        
        scaled_uov1 = (uov1(:) - nanmean(uov1(:))) / nanstd(uov1(:));
        scaled_vov1 = (vov1(:) - nanmean(vov1(:))) / nanstd(vov1(:));
        scaled_uov2 = (uov2(:) - nanmean(uov2(:))) / nanstd(uov2(:));
        scaled_vov2 = (vov2(:) - nanmean(vov2(:))) / nanstd(vov2(:));
        
        scaled_uov1(isnan(scaled_uov1)) = 0;
        scaled_vov1(isnan(scaled_vov1)) = 0;
        scaled_uov2(isnan(scaled_uov2)) = 0;
        scaled_vov2(isnan(scaled_vov2)) = 0;
        
        corr_u(j,i) = xcorr(scaled_uov1(:),scaled_uov2(:),0,'coeff');
        corr_v(j,i) = xcorr(scaled_vov1(:),scaled_vov2(:),0,'coeff');
        
        %clear uov1 vov1 Mask1ov uov2 vov2 Mask2ov
        clear uov1 vov1 uov2 vov2
    end
end
[~,I] = max(corr_u(:).^2 + corr_v(:).^2);
[I1,I2] = ind2sub(size(corr_u),I);

x2shift = window(1) + delx*(I2 - 1); 
y2shift = window(3) + delx*(I1 - 1);

end

