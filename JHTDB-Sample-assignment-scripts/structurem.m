% Matlab script for analysis on turbulent structures
% using Johns Hopkins Turbulence DataBase (JHTDB) channel flow
%
% Written by:
%
% Jin LEE
% Johns Hopkins University
% Department of Mechanical Engineering
%

clear all;
close all;

authkey = 'edu.jhu.meneveau-hiSmxkae'; % request authkey by emailing turbulence@jhu.edu
dataset = 'channel';

% ---- Temporal Interpolation Options ----
NoTInt   = 'None' ; % No temporal interpolation
PCHIPInt = 'PCHIP'; % Piecewise cubic Hermit interpolation in time

% ---- Spatial Interpolation Flags for getVelocity & getVelocityAndPressure ----
NoSInt = 'None'; % No spatial interpolation
Lag4   = 'Lag4'; % 4th order Lagrangian interpolation in space
Lag6   = 'Lag6'; % 6th order Lagrangian interpolation in space
Lag8   = 'Lag8'; % 8th order Lagrangian interpolation in space

% ---- Spatial Differentiation & Interpolation Flags for getVelocityGradient & getPressureGradient ----
FD4NoInt = 'None_Fd4' ; % 4th order finite differential scheme for grid values, no spatial interpolation
FD6NoInt = 'None_Fd6' ; % 6th order finite differential scheme for grid values, no spatial interpolation
FD8NoInt = 'None_Fd8' ; % 8th order finite differential scheme for grid values, no spatial interpolation
FD4Lag4  = 'Fd4Lag4'  ; % 4th order finite differential scheme for grid values, 4th order Lagrangian interpolation in space

% ---- Spline interpolation and differentiation Flags for getVelocity,
% getPressure, getVelocityGradient, getPressureGradient,
% getVelocityHessian, getPressureHessian
M1Q4   = 'M1Q4'; % Splines with smoothness 1 (3rd order) over 4 data points. Not applicable for Hessian.
M2Q8   = 'M2Q8'; % Splines with smoothness 2 (5th order) over 8 data points.
M2Q14   = 'M2Q14'; % Splines with smoothness 2 (5th order) over 14 data points.




% ///////////////////////////////////////////////////////////
% ////////////// GENERATE A SIMPLE CONTOUR PLOT IN XZ /////////////
%////////////////////////////////////////////////////////////

Uvar = 1;                           % 1 = Stream, 2 = wall-normal, and 3 = spanwise velocity
time = 0;
spacing_x = 8.0*pi/2048; % x-spacing
spacing_z = 3.0*pi/1536; % z-spacing

% Set domain size and position
x_min = 17.0;
x_max = 21.0;
z_min = 1.0;
z_max = 2.0;
nx = int32( (x_max - x_min) / spacing_x);
nz = int32( (z_max - z_min) / spacing_z);
yoff = -9.85e-01; % y+ = 15
npoints = nx*nz;


% Create surface
clear x; clear z; clear X; clear Z;
x = linspace(0, double(nx-1)*spacing_x, nx) + x_min;
z = linspace(0, double(nz-1)*spacing_z, nz) + z_min;
[X, Z] = meshgrid(x, z);
clear points;
points(1,:) = X(:)';
points(3,:) = Z(:)';
points(2,:) = yoff;

% Get the velocity at each point
fprintf('\nRequesting velocity at (%ix%i) points for velocity contour plot...\n',nx, nz);
clear result3;
result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);

% Calculate velocity
clear result1;
clear result_2D;
result1 = result3(Uvar,:);
result_2D = reshape(result1, nz, nx);

% Plot x-velocity contours
figure;
contourf(X, Z, result_2D, 30, 'LineStyle', 'none');
daspect([1,1,1])
colormap(jet);
title('x-velocity', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
colorbar('FontSize', 12);
axis([min(x) max(x) min(z) max(z)]);





% ///////////////////////////////////////////////////////////
% ////////////// GENERATE A SIMPLE CONTOUR PLOT IN XY /////////////
%////////////////////////////////////////////////////////////

Uvar = 1;                           % 1 = Stream, 2 = wall-normal, and 3 = spanwise velocity
time = 0;
spacing_x = 8.0*pi/2048;
spacing_y = 0.025;
spacing_z = 3.0*pi/1536;

% Set domain size and position
x_min = 17.0;
x_max = 21.0;
y_min = -1.0;
y_max = 0.0;
nx = int32( (x_max - x_min) / spacing_x);
ny = int32( (y_max - y_min) / spacing_y);
zoff = 0.0;
npoints = nx*ny;

% Create surface
clear x; clear y; clear X; clear Y;
x = linspace(0, double(nx-1)*spacing_x, nx) + x_min;
y = linspace(0, double(ny-1)*spacing_y, ny) + y_min;
[X, Y] = meshgrid(x, y);
clear points;
points(1,:) = X(:)';
points(2,:) = Y(:)';
points(3,:) = zoff;

% Get the velocity at each point
fprintf('\nRequesting velocity at (%ix%i) points for velocity contour plot...\n',nx, ny);
result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);

% Calculate velocity
clear result1;
clear result_2D;
result1 = result3(Uvar,:);
result_2D = reshape(result1, ny, nx);

% Plot x-velocity contours
figure;
contourf(X, Y, result_2D, 30, 'LineStyle', 'none');
daspect([1,1,1])
colormap(jet);
title('x-velocity', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
colorbar('FontSize', 12);
axis([min(x) max(x) min(y) max(y)]);





% ///////////////////////////////////////////////////////////
% ////////////// GENERATE A SIMPLE ISOSURFACE /////////////
%////////////////////////////////////////////////////////////

Uvar = 1;                           % 1 = Stream, 2 = wall-normal, and 3 = spanwise velocity
time = 0;
nx = 96;
ny = 16;
nz = 48;

% Set domain size and position
x_min = 0.0;
x_max = 6.0;
y_min = -1.0;
y_max = 0.0;
z_min = 0.0;
z_max = 2.0;
spacing_x = (x_max-x_min)/(nx-1);
spacing_y = (y_max-y_min)/(ny-1);
spacing_z = (z_max-z_min)/(nz-1);

npoints = nx*nz;

clear points;
clear result_2D;
clear U;
clear X;
clear Z;

% Create surface
x = linspace(0, (nx-1)*spacing_x, nx) + x_min;
z = linspace(0, (nz-1)*spacing_z, nz) + z_min;
[X, Z] = meshgrid(x, z);
points(1,:) = X(:)';
points(3,:) = Z(:)';

for j = 1 : ny;
    y_slice = (j-1)*spacing_y + y_min;
    points(2,:) = y_slice;
    
    fprintf('\nRequesting velocity at (%ix%i) points for 3-D isosurface plot at j=%i...', nx, nz, j);
    result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);           % Basically, request 3-D field is possible. But request in 2-D is more stable.
    
    % Calculate velocity
    result1 = result3(Uvar,:);
    result_2D = reshape(result1, nz, nx);
    for i = 1 : nx;
        for k = 1 : nz;
            U(j, i, k) = result_2D(k, i);
        end     % k-loop
    end         % i-loop
end

clear X;
clear Y;
clear Z;
y = linspace(0, (ny-1)*spacing_y, ny) + yoff;
[X, Y, Z] = meshgrid(x, y, z);

% Plot velocity isosurfaces
figure;
p = patch(isosurface(X, Y, Z, U, 0.95));
isonormals(X, Y, Z, U, p)
p.FaceColor = 'blue';
p.EdgeColor = 'none';
daspect([1,1,1])
view(3); axis tight
camlight
lighting gouraud
title('Velocity isosurface', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
zlabel('z', 'FontSize', 12, 'FontWeight', 'bold');






% ///////////////////////////////////////////////////////////
% ////////////// SIMPLE AVERAGE /////////////
%////////////////////////////////////////////////////////////

% Set domain size and position
nx = 64;
ny = 48;
nz = 32;
time_start = 10;
time_interval = 10;
time_end = 10;

x_min = 0.0;
x_max = 8.0*pi;
y_min = -1.0;
y_max = 0.0;
z_min = 0.0;
z_max = 3.0*pi;
spacing_x = (x_max-x_min)/(nx-1);
spacing_y = (y_max-y_min)/(ny-1);
spacing_z = (z_max-z_min)/(nz-1);

npoints = nx*nz;

% Create surface
x = linspace(0, double(nx-1)*spacing_x, nx) + x_min;
y = linspace(0, double(ny-1)*spacing_y, ny) + y_min;
z = linspace(0, double(nz-1)*spacing_z, nz) + z_min;
[X, Z] = meshgrid(x, z);
clear points;
points(1,:) = X(:)';
points(3,:) = Z(:)';

% Get DNS data from JHTDB
clear result_2D_u; clear result_2D_v; clear result_2D_w;
clear u_fluct; clear v_fluct; clear w_fluct;
clear U_xz_ins; clear V_xz_ins;
clear uu_xz_ins; clear vv_xz_ins; clear ww_xz_ins;
clear uv_xz_ins; clear vw_xz_ins; clear uw_xz_ins;
clear U_xz; clear V_xz;
clear uu_xz; clear vv_xz; clear ww_xz;
clear uv_xz; clear vw_xz; clear uw_xz;

% Initialization
U_xz(1:ny) = 0; V_xz(1:ny) = 0;
uu_xz(1:ny) = 0; vv_xz(1:ny) = 0; ww_xz(1:ny) = 0;
uv_xz(1:ny) = 0; vw_xz(1:ny) = 0; uw_xz(1:ny) = 0;
u_fluct_xz(1:nz, 1:nx) = 0;
v_fluct_xz(1:nz, 1:nx) = 0;
w_fluct_xz(1:nz, 1:nx) = 0;

ntime = 0;

for time = time_start : time_interval : time_end;
    ntime = ntime + 1;
    
    for j = 1 : ny;
        points(2,:) = y(j);
        
        fprintf('\nRequesting velocity at (%ix%i) points for the quadrant analysis at j=%i and ntime=%i...', nx, nz, j, ntime);
        result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);           % Basically, request 3-D field is possible. But request in 2-D is more stable.
        
        % Calculate velocity
        result1 = result3(1,:);               % 1=Streamwise velocity
        result_2D_u = reshape(result1, nz, nx);
        result1 = result3(2,:);               % 2=Wall-normal velocity
        result_2D_v = reshape(result1, nz, nx);
        result1 = result3(3,:);               % 3=Spanwise velocity
        result_2D_w = reshape(result1, nz, nx);
        
        % First-order statistics (mean velocity)
        U_xz_ins = sum( sum( result_2D_u ) ) / ( nx * nz );                 % Spatial average of total velocity
        V_xz_ins = sum( sum( result_2D_v ) ) / ( nx * nz );
        
        U_xz(j) = ( U_xz(j) * (ntime-1) + U_xz_ins ) / ntime;       % Temporal-average
        V_xz(j) = ( V_xz(j) * (ntime-1) + V_xz_ins ) / ntime;
        
        % Second-order statistics (Reynolds stress)
        uu_xz_ins = sum( sum( result_2D_u.*result_2D_u ) ) / ( nx * nz );   % Spatial average of squared total velocity
        vv_xz_ins = sum( sum( result_2D_v.*result_2D_v ) ) / ( nx * nz );
        ww_xz_ins = sum( sum( result_2D_w.*result_2D_w ) ) / ( nx * nz );
        
        u_fluct = result_2D_u - U_xz(j);             % Streamwise velocity fluctuation
        v_fluct = result_2D_v - V_xz(j);             % Wall-normal velocity fluctuation
        w_fluct = result_2D_w;             % Spanwise velocity fluctuation. W_xz is expected to be zero.
        
        uv_xz_ins = sum( sum( u_fluct.*v_fluct ) ) / ( nx * nz );
        vw_xz_ins = sum( sum( v_fluct.*w_fluct ) ) / ( nx * nz );
        uw_xz_ins = sum( sum( u_fluct.*w_fluct ) ) / ( nx * nz );
        
        
        uu_xz(j) = ( uu_xz(j) * (ntime-1) + uu_xz_ins ) / ntime;
        vv_xz(j) = ( vv_xz(j) * (ntime-1) + vv_xz_ins ) / ntime;
        ww_xz(j) = ( ww_xz(j) * (ntime-1) + ww_xz_ins ) / ntime;
        uv_xz(j) = ( uv_xz(j) * (ntime-1) + uv_xz_ins ) / ntime;
        vw_xz(j) = ( vw_xz(j) * (ntime-1) + vw_xz_ins ) / ntime;
        uw_xz(j) = ( uw_xz(j) * (ntime-1) + uw_xz_ins ) / ntime;
        
        u_rms_xz(j) = sqrt( uu_xz(j) - U_xz(j).*U_xz(j) );
        v_rms_xz(j) = sqrt( vv_xz(j) - V_xz(j).*V_xz(j) );
        w_rms_xz(j) = sqrt( ww_xz(j) );
        
    end     % J-loop
    
end     % time-loop

% Plot results
figure;
subplot(2, 2, 1);
plot( y, U_xz, '-', y, V_xz, ':');
title('Mean velocity profiles', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('y', 'FontSize', 12, 'FontWeight', 'bold');
legend ('U', 'V', 'Location', 'southeast');

subplot(2, 2, 2);
plot( y, u_rms_xz, '-', y, v_rms_xz, ':', y, w_rms_xz, '-.');
title('Normal stress profiles', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('y', 'FontSize', 12, 'FontWeight', 'bold');
legend ('u_{rms}', 'v_{rms}', 'w_{rms}', 'Location', 'northeast');

subplot(2, 2, 4);
plot( y, uv_xz, '-', y, vw_xz, ':', y, uw_xz, '-.');
title('Shear stress profiles', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('y', 'FontSize', 12, 'FontWeight', 'bold');
legend ('uv', 'vw', 'uw', 'Location', 'southeast');






% ///////////////////////////////////////////////////////////
% ////////////// TWO-POINT CORRELATION (2-D) by 2-D FFT /////////////
%////////////////////////////////////////////////////////////
% Two-point correlation to estimate domain size and large-scale velocity
% coherence.

% Parameters set-up
y_ref = -0.80;                  % Reference wall-normal position of Ruu
Uvar = 1;
nx = 256;
nz = 128;
time_start = 1;
time_interval = 0.5;
time_end = 1;

% Meshing for GetVelocity from JHTDB
spacing_x = 8.0*pi / nx;
spacing_z = 3.0*pi / nz;
x = linspace(0, (nx-1)*spacing_x, nx); % + xoff;
z = linspace(0, (nz-1)*spacing_z, nz); % + zoff;
[X, Z] = meshgrid(x, z);
clear points;
npoints = nx*nz;
points(1,:) = X(:)';
points(3,:) = Z(:)';
points(2,:) = y_ref;

% Meshing for the visualization of the correlation coefficient
delta_x = x - x( nx/2 + 1);
delta_z = z - z( nz/2 + 1);
[X, Z] = meshgrid(delta_x, delta_z);

% Initialization of accumulated quanities before time-loop
clear fft_coef_xz;
clear convol_xz;
clear ifft_coef_xz;
clear Ruu_xz;
clear Ruu_xz_xrotate;
clear Ruu_xz_plot;
U_xz = 0;
uu_xz = 0;
ntime = 0;
Ruu_xz = 0;

for time = time_start : time_interval : time_end;
    ntime = ntime + 1;
    
    % Get the velocity at each point
    fprintf('\nRequesting velocity at (%ix%i) points for two-point correlation at time=%f...',nx,nz,time);
    result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);
    
    % Calculate velocity
    result1 = result3(Uvar, :);                         % Extract one velocity component
    result_2D = reshape(result1, nz, nx);       % Reshape
    U_xz = ( U_xz * (ntime-1) + sum( sum( result_2D ) ) / ( nx * nz ) ) / ntime;       % Spatial average in xz-plane
    uu_xz = ( uu_xz * (ntime-1) + sum( sum( result_2D.*result_2D ) ) / ( nx * nz ) ) / ntime;     % Average of squared total streamwise velocity
    u_rms_xz = sqrt( uu_xz - U_xz^2 );
    
    % Compute correlation using FFT
    result_2D = result_2D - U_xz;           % Fluctuating velocity
    fft_coef_xz = fft2(result_2D, nz, nx);      % 2-D FFT (forward)
    convol_xz = real( fft_coef_xz.*conj(fft_coef_xz) ) / ( nx * nz );       % Convolution
    ifft_coef_xz = ifft2( convol_xz, nz, nx );        % 2-D FFT (inverse)
    
    % Temporal average
    Ruu_xz = ( Ruu_xz * (ntime-1) + ifft_coef_xz ) / ntime;
    
end     % time-loop

% Plot results
figure;
Ruu_xz_xrotate(:, nx/2+1:nx) = Ruu_xz(:, 1:nx/2);
Ruu_xz_xrotate(:, 1:nx/2) = Ruu_xz(:, nx/2+1:nx);
Ruu_xz_plot(nz/2+1:nz, :) = Ruu_xz_xrotate(1:nz/2, :);
Ruu_xz_plot(1:nz/2, :) = Ruu_xz_xrotate(nz/2+1:nz, :);

Ruu_xz_plot = Ruu_xz_plot / (u_rms_xz * u_rms_xz);                % Consideration of denominator in the Ruu

% 2-D contour
subplot(2, 2, [1, 2]);
contourf(X, Z, Ruu_xz_plot, 30, 'LineStyle', 'none');
colormap(jet);
title(['Two-point correlation at y_{ref} = ', num2str(y_ref)], 'FontSize', 13, 'FontWeight', 'bold');
xlabel('\Delta x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('\Delta z', 'FontSize', 12, 'FontWeight', 'bold');
colorbar('FontSize', 12);
axis([min(delta_x) max(delta_x) min(delta_z) max(delta_z)]);
daspect([1,1,1]);

% 1-D profile in \Delta{x}
subplot(2, 2, 3);
plot(X(nz/2+1, :), Ruu_xz_plot(nz/2+1, :));
xlabel('\Delta x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('R_{uu} at \Delta{z}=0', 'FontSize', 12, 'FontWeight', 'bold');

% 1-D profile in \Delta{z}
subplot(2, 2, 4);
plot(Z(:, nx/2+1), Ruu_xz_plot(:, nx/2+1));
xlabel('\Delta{z}', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('R_{uu} at \Delta{x}=0', 'FontSize', 12, 'FontWeight', 'bold');




% ///////////////////////////////////////////////////////////
% ////////////// DECOMPOSITION OF INSTANTANEOUS FLOW FIELD BY WAVELENGTH /////////////
%////////////////////////////////////////////////////////////

% Parameters set-up
lambda_x_critical = 1.0;                            % Streamwise wavenumber; unit in delta (channel half-height)
lambda_z_critical = 1.0;                            % Streamwise wavenumber; unit in delta (channel half-height)
y_ref = -0.80;                  % Reference wall-normal position of Ruu
Uvar = 1;
nx = 256;
nz = 128;
time = 1;

% Meshing for GetVelocity from JHTDB
spacing_x = 8.0*pi / nx;
spacing_z = 3.0*pi / nz;
x = linspace(0, (nx-1)*spacing_x, nx); % + xoff;
z = linspace(0, (nz-1)*spacing_z, nz); % + zoff;
[X, Z] = meshgrid(x, z);
clear points;
npoints = nx*nz;
points(1,:) = X(:)';
points(3,:) = Z(:)';
points(2,:) = y_ref;

% Initialization of accumulated quanities before time-loop
clear result_2D;
clear result_x; clear fft_coef_x;
clear result_z; clear fft_coef_z;
U_xz = 0;
uu_xz = 0;

% Get the velocity at each point
fprintf('\nRequesting velocity at (%ix%i) points for wave-decomposition at time=%f...',nx,nz,time);
result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);

% Calculate velocity
result1 = result3(Uvar, :);                         % Extract one velocity component
result_2D = reshape(result1, nz, nx);       % Reshape

% Compute correlation using FFT
result_2D = result_2D - U_xz;           % Fluctuating velocity

% Low-pass filter in the streamwise direction
clear result_x_line;
clear fft_coef_x;
clear filtered_vel1;
k_x_critical = int32( ( max(x) - min(x) ) / lambda_x_critical ) + 1;
result_x_line(1:nx) = 0.0;
filtered_vel1(1:nz, 1:nx) = 0;
for k = 1 : nz;
    result_x_line(:) = result_2D(k, :);                   % Extract one line
    fft_coef_x = fft(result_x_line, nx);                    % FFT in x
    fft_coef_x(k_x_critical+1: nx-k_x_critical+1) = 0.0;       % Elimination of short wavelength
    filtered_vel1(k, :) = real( ifft( fft_coef_x, nx ) );             % Inverse FFT in x
end     % k-loop


% Low-pass filter in the spanwise direction
clear result_z_line;
clear fft_coef_z;
clear filtered_vel2;
k_z_critical = int32( ( max(z) - min(z) ) / lambda_z_critical ) + 1;
result_z_line(1:nz) = 0.0;
filtered_vel2(1:nz, 1:nx) = 0;
for i = 1 : nx;
    result_z_line(:) = filtered_vel1(:, i);                   % Extract one line
    fft_coef_z = fft(result_z_line, nz);                    % FFT in z
    fft_coef_z(k_z_critical+1: nz-k_z_critical+1) = 0.0;       % Elimination of short wavelength
    filtered_vel2(:, i) = real( ifft( fft_coef_z, nz ) );             % Inverse FFT in z
end     % k-loop

% Plot results
figure;
subplot(3, 2, 1);
contourf(X, Z, result_2D, 30, 'LineStyle', 'none');
title('Raw velocity field', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
daspect([1,1,1])
colorbar('FontSize', 12);

subplot(3, 2, 3);
contourf(X, Z, filtered_vel1, 30, 'LineStyle', 'none');
title('Low-pass-filtered field in x', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
daspect([1,1,1])
colorbar('FontSize', 12);

subplot(3, 2, 4);
contourf(X, Z, result_2D - filtered_vel1, 30, 'LineStyle', 'none');
title('High-pass-filtered field in x', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
daspect([1,1,1])
colorbar('FontSize', 12);

subplot(3, 2, 5);
contourf(X, Z, filtered_vel2, 30, 'LineStyle', 'none');
title('Low-pass-filtered field in x and z', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
daspect([1,1,1])
colorbar('FontSize', 12);

subplot(3, 2, 6);
contourf(X, Z, result_2D - filtered_vel2, 30, 'LineStyle', 'none');
title('High-pass-filtered field in x and z', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('z', 'FontSize', 12, 'FontWeight', 'bold');
daspect([1,1,1])
colorbar('FontSize', 12);
colormap(jet);




% ///////////////////////////////////////////////////////////
% ////////////// ENERGY SPECTRUM by 1-D FFT /////////////
%////////////////////////////////////////////////////////////

% Parameters set-up
y_ref = -0.80;                  % Reference wall-normal position of Ruu
Uvar = 1;
nx = 256;
nz = 128;
time_start = 1;
time_interval = 0.5;
time_end = 1;

% Meshing for GetVelocity from JHTDB
spacing_x = 8.0*pi / nx;
spacing_z = 3.0*pi / nz;
x = linspace(0, (nx-1)*spacing_x, nx); % + xoff;
z = linspace(0, (nz-1)*spacing_z, nz); % + zoff;
[X, Z] = meshgrid(x, z);
clear points;
npoints = nx*nz;
points(1,:) = X(:)';
points(3,:) = Z(:)';
points(2,:) = y_ref;

% Meshing for the visualization of the correlation coefficient
delta_x = x - x( nx/2 + 1);
delta_z = z - z( nz/2 + 1);
clear X; clear Z;
[X, Z] = meshgrid(delta_x, delta_z);

% Initialization of accumulated quanities before time-loop
clear result_x; clear fft_coef_x;
clear spec_x; clear spec_x_ins;
clear result_z; clear fft_coef_z;
clear spec_z; clear spec_z_ins;
% clear spec_plot;
U_xz = 0;
uu_xz = 0;
ntime = 0;
spec_x(1:nx) = 0;
spec_x_ins(1:nx) = 0;
spec_z(1:nz) = 0;
spec_z_ins(1:nz) = 0;
% spec_plot(1:nz/2, 1:nx/2) = 0;

for time = time_start : time_interval : time_end;
    ntime = ntime + 1;
    
    % Get the velocity at each point
    fprintf('\nRequesting velocity at (%ix%i) points for energy spectrum at time=%f...',nx,nz,time);
    result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);
    
    % Calculate velocity
    result1 = result3(Uvar, :);                         % Extract one velocity component
    result_2D = reshape(result1, nz, nx);       % Reshape
    U_xz = ( U_xz * (ntime-1) + sum( sum( result_2D ) ) / ( nx * nz ) ) / ntime;       % Spatial average in xz-plane
    uu_xz = ( uu_xz * (ntime-1) + sum( sum( result_2D.*result_2D ) ) / ( nx * nz ) ) / ntime;     % Average of squared total streamwise velocity
    u_rms_xz = sqrt( uu_xz - U_xz^2 );
    
    % Compute correlation using FFT
    result_2D = result_2D - U_xz;           % Fluctuating velocity
    
    spec_x_ins = 0;
    for k = 1 : nz;
        for i = 1 : nx;
            result_x(i) = result_2D(k, i);
        end
        fft_coef_x = fft(result_x, nx) / nx;      % 1-D FFT (forward)
        spec_x_ins = spec_x_ins + real( fft_coef_x.*conj(fft_coef_x) ) / nz;
    end
    
    spec_z_ins = 0;
    for i = 1 : nx;
        for k = 1 : nz;
            result_z(k) = result_2D(k, i);
        end
        fft_coef_z = fft(result_z, nz) / nz;      % 1-D FFT (forward)
        spec_z_ins = spec_z_ins + real( fft_coef_z.*conj(fft_coef_z) ) / nx;
    end
    
    % Temporal average
    spec_x = ( spec_x * (ntime-1) + spec_x_ins ) / ntime;
    spec_z = ( spec_z * (ntime-1) + spec_z_ins ) / ntime;
    
end     % time-loop

% Plot results
figure;
subplot(1, 2, 1);
plot( [0:nx/2], log10( spec_x(1:nx/2+1) ));           % real part
xlabel('k_{x} = L_{x} / \lambda_{x}', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('spectrum (log10)', 'FontSize', 12, 'FontWeight', 'bold');
subplot(1, 2, 2);
plot( [0:nz/2], log10( spec_z(1:nz/2+1) ));           % real part
xlabel('k_{z} = L_{z} / \lambda_{z}', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('spectrum (log10)', 'FontSize', 12, 'FontWeight', 'bold');





% ///////////////////////////////////////////////////////////
% ////////////// QUADRANT ANALYSIS AND CONDITIONALLY-AVERAGED VELOCITY FIELD /////////////
%////////////////////////////////////////////////////////////

% Set domain size and position
j_ref = 6;
avg_width_x = 8;
avg_width_z = 10;

% Set domain size and position
nx = 40;
ny = 20;
nz = 50;
time_start = 1;
time_interval = 1;
time_end = 1;

x_min = 0.0;
x_max = 8.0*pi;
y_min = -1.0;
y_max = -0.8;
z_min = 0.0;
z_max = 1.0;
spacing_x = (x_max-x_min)/(nx-1);
spacing_y = (y_max-y_min)/(ny-1);
spacing_z = (z_max-z_min)/(nz-1);

npoints = nx*nz;

% Create surface
x = linspace(0, (nx-1)*spacing_x, nx) + x_min;
y = linspace(0, (ny-1)*spacing_y, ny) + y_min;
z = linspace(0, (nz-1)*spacing_z, nz) + z_min;
[X, Z] = meshgrid(x, z);
clear points;
points(1,:) = X(:)';
points(3,:) = Z(:)';

% Get DNS data from JHTDB
clear result_2D_u; clear result_2D_v; clear result_2D_w;
clear U_xz; clear V_xz;
clear U; clear V; clear W;
clear cond_avg_u; clear cond_avg_v; clear cond_avg_w;
clear Q_uv; clear Q_uv_ins; clear uv_total;

% Initialization
U_xz(1:ny) = 0;
V_xz(1:ny) = 0;
U(1:ny, 1:nx, 1:nz) = 0;
V(1:ny, 1:nx, 1:nz) = 0;
W(1:ny, 1:nx, 1:nz) = 0;
cond_avg_u(1:ny, 2*avg_width_x+1, 2*avg_width_z+1) = 0;
cond_avg_v(1:ny, 2*avg_width_x+1, 2*avg_width_z+1) = 0;
cond_avg_w(1:ny, 2*avg_width_x+1, 2*avg_width_z+1) = 0;
Q_uv(1:ny, 1:4) = 0;
uv_total(1:ny) = 0;

ntime = 0;
n_cond_event = 0;

for time = time_start : time_interval : time_end;
    ntime = ntime + 1;
    
    Q_uv_ins(1:ny, 1:4) = 0;
    
    for j = 1 : ny;
        points(2,:) = y(j);
        
        fprintf('\nRequesting velocity at (%ix%i) points for the conditional average at j=%i and ntime=%i...', nx, nz, j, ntime);
        result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);           % Basically, request 3-D field is possible. But request in 2-D is more stable.
        
        % Calculate velocity
        result1 = result3(1,:);               % 1=Streamwise velocity
        result_2D_u = reshape(result1, nz, nx);
        result1 = result3(2,:);               % 2=Wall-normal velocity
        result_2D_v = reshape(result1, nz, nx);
        result1 = result3(3,:);               % 3=Spanwise velocity
        result_2D_w = reshape(result1, nz, nx);
        
        U_xz(j) = ( U_xz(j) * (ntime-1) + sum( sum( result_2D_u ) ) / ( nx * nz ) ) / ntime;       % Spatial- and temporal-average
        V_xz(j) = ( V_xz(j) * (ntime-1) + sum( sum( result_2D_v ) ) / ( nx * nz ) ) / ntime;       % Spatial- and temporal-average
        
        for i = 1 : nx;
            for k = 1 : nz;
                U(j, i, k) = result_2D_u(k, i) - U_xz(j);          % Streamwise velocity fluctuation
                V(j, i, k) = result_2D_v(k, i) - V_xz(j);           % Wall-normal velocity fluctuation
                W(j, i, k) = result_2D_w(k, i);                     % Spanwise velocity fluctuation (c.f. <W> = 0 )
            end     % k-loop
        end         % i-loop
        
        % Quadrant analysis
        for k = 1 : nz;
            for i = 1 : nx;
                u_fluct = U(j, i, k);
                v_fluct = V(j, i, k);
                if( u_fluct > 0 && v_fluct > 0 )
                    Q_uv_ins(j, 1) = Q_uv_ins(j, 1) + u_fluct * v_fluct;
                elseif( u_fluct < 0 && v_fluct > 0 )
                    Q_uv_ins(j, 2) = Q_uv_ins(j, 2) + u_fluct * v_fluct;
                elseif( u_fluct < 0 && v_fluct < 0 )
                    Q_uv_ins(j, 3) = Q_uv_ins(j, 3) + u_fluct * v_fluct;
                elseif( u_fluct > 0 && v_fluct < 0 )
                    Q_uv_ins(j, 4) = Q_uv_ins(j, 4) + u_fluct * v_fluct;
                end
            end     % i-loop
        end     % k-loop
    end     % J-loop
    
    
    % Conditional average for the ejection event
    for k = 1+avg_width_z : nz-avg_width_z;
        for i = 1+avg_width_x : nx-avg_width_x;
            if( U(j_ref, i, k) < 0 && V(j_ref, i, k) > 0 )      % If ejection event is located at the reference position,
                n_cond_event = n_cond_event + 1;
                cond_avg_u(:, :, :) = ( cond_avg_u(:, :, :) * (n_cond_event - 1) + U(:, i-avg_width_x : i+avg_width_x, k-avg_width_z : k+avg_width_z) ) / n_cond_event;
                cond_avg_v(:, :, :) = ( cond_avg_v(:, :, :) * (n_cond_event - 1) + V(:, i-avg_width_x : i+avg_width_x, k-avg_width_z : k+avg_width_z) ) / n_cond_event;
                cond_avg_w(:, :, :) = ( cond_avg_w(:, :, :) * (n_cond_event - 1) + W(:, i-avg_width_x : i+avg_width_x, k-avg_width_z : k+avg_width_z) ) / n_cond_event;
            end     % end if
        end     % i-loop
    end     % k-loop
    
    Q_uv(:, :) = ( Q_uv(:, :) * (ntime - 1) + ( Q_uv_ins(:, :) / (nx*nz) ) ) / ntime;
    
end     % time-loop

% Plot results - Quadrant analysis
for j = 1 : ny;
    uv_total(j) = Q_uv(j, 1) + Q_uv(j, 2) + Q_uv(j, 3) + Q_uv(j, 4);
end
figure;
plot( y, Q_uv(:, 1), ':', y, Q_uv(:, 2), '-', y, Q_uv(:, 3), '-.', y, Q_uv(:, 4), '--', y, uv_total, 'O' );
title('Quadrant analysis of Reynolds shear stress', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('y', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('<uv |_{Q_i}>', 'FontSize', 12, 'FontWeight', 'bold');
legend ('Q1', 'Q2', 'Q3', 'Q4', 'uv', 'Location', 'southeast');

% Plot results - Conditional average
fprintf('\nTotal number of conditional event=%i...', n_cond_event);
figure;
clear x; clear z;
x = linspace(-avg_width_x*spacing_x, avg_width_x*spacing_x, 2*avg_width_x+1);
z = linspace(-avg_width_z*spacing_z, avg_width_z*spacing_z, 2*avg_width_z+1);
% Plot isosurfaces
% side view
subplot( 2, 1, 1)
clear X; clear Y; clear Z;
clear result_2D_u; clear result_2D_v; clear result_2D_w;
[X, Y] = meshgrid(x, y);
for j = 1 : ny;
    for i = 1 : 2*avg_width_x + 1;
        result_2D_u( j, i) = cond_avg_u(j, i, avg_width_z + 1);
        result_2D_v( j, i) = cond_avg_v(j, i, avg_width_z + 1);
        result_2D_w( j, i) = cond_avg_w(j, i, avg_width_z + 1);
    end     % k-loop
end     % j-loop
quiver( X,  Y, result_2D_u, result_2D_v )
xlabel('\Delta{x}', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
hold off

% Cross-flow cut view
subplot( 2, 1, 2)
clear X; clear Y; clear Z;
clear result_2D_u; clear result_2D_v; clear result_2D_w;
[Z, Y] = meshgrid(z, y);
for j = 1 : ny;
    for k = 1 : 2*avg_width_z + 1;
        result_2D_u( j, k) = cond_avg_u(j, avg_width_x + 1, k);
        result_2D_v( j, k) = cond_avg_v(j, avg_width_x + 1, k);
        result_2D_w( j, k) = cond_avg_w(j, avg_width_x + 1, k);
    end     % k-loop
end     % j-loop
contour( Z, Y, result_2D_u )
hold on
quiver( Z,  Y, result_2D_w, result_2D_v )
xlabel('\Delta{z}', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
hold off







% ///////////////////////////////////////////////////////////
% ////////////// STREAK DETECTION /////////////
%////////////////////////////////////////////////////////////
% The current streak detection is a simplified introductory version.
% The details of the algorithm can be found in:
%
%   Nolan & Zaki (J. Fluid Mech., Vol 728, pp. 306-339, 2013)
%   Lee et al., (J. Fluid Mech., Vol., 749, pp. 818-840, 2014)
%

% Parameters
U_threshold = 0.1;

% Set domain size and position
nx = 64;
ny = 48;
nz = 48;
time = 12;

x_min = 0.0;
x_max = 5;
y_min = -1.0;
y_max = 0.0;
z_min = 0.8;
z_max = 1.8;
spacing_x = (x_max-x_min)/(nx-1);
spacing_y = (y_max-y_min)/(ny-1);
spacing_z = (z_max-z_min)/(nz-1);

npoints = nx*nz;

% Create surface
x = linspace(0, double(nx-1)*spacing_x, nx) + x_min;
y = linspace(0, double(ny-1)*spacing_y, ny) + y_min;
z = linspace(0, double(nz-1)*spacing_z, nz) + z_min;
[X, Z] = meshgrid(x, z);
clear points;
points(1,:) = X(:)';
points(3,:) = Z(:)';

% Get DNS data from JHTDB
clear result_2D;
clear U;
for j = 1 : ny;
    y_slice = (j-1)*spacing_y + y_min;
    points(2,:) = y_slice;
    % Get the velocity at each point
    fprintf('\nRequesting velocity at (%ix%i) points for streak detection at j=%i...', nx, nz, j);
    result3 = getVelocity(authkey, dataset, time, NoSInt, NoTInt, npoints, points);
    % Basically, request 3-D field is possible. But request in 2-D is more stable.
    
    % Calculate x-velocity
    result1 = result3(1, :);
    result_2D = reshape(result1, nz, nx);
    for i = 1 : nx;
        for k = 1 : nz;
            U_xz = sum( sum( result_2D ) ) / ( nx * nz );       % Spatial average in xz-plane
            U(j, i, k) = result_2D(k, i) - U_xz;
        end     % k-loop
    end         % i-loop
end


% Gaussian filter in the cross-flow plane
sigma_gaussian = 0.1;           % Unit in delta (channel half-height); 
my_filter = fspecial('gaussian', [41, 41], sigma_gaussian / spacing_z);
clear filtered_vel1;
filtered_vel1(1:ny, 1:nx, 1:nz) = 0;
for i = 1 : nx;
    filtered_vel1(:, i, :) = imfilter(U(:, i, :), my_filter);
end     % i-loop


% Low-pass filter in the streamwise direction
clear result_x_line;
clear fft_coef_x;
clear filtered_vel2;
lambda_x_critical = 1.0;                            % Streamwise wavenumber; unit in delta (channel half-height)
k_x_critical = int32( ( max(x) - min(x) ) / lambda_x_critical );
result_x_line(1:nx) = 0.0;
filtered_vel2(1:ny, 1:nx, 1:nz) = 0;
for j = 1 : ny;
    for k = 1 : nz;
        result_x_line(:) = filtered_vel1(j, :, k);                      % Extract one line
        fft_coef_x = fft(result_x_line, nx);                            % FFT in x
        fft_coef_x(k_x_critical+1: nx-k_x_critical+1) = 0.0;       % Elimination of short wavelength
        filtered_vel2(j, :, k) = real( ifft( fft_coef_x, nx ) );        % Inverse FFT in x
    end     % k-loop
end     % j-loop

% Threshold correction after elimination of small-scales
clear U_filter_threshold;
for j = 1 : ny;
    uu_xz = 0;
    uu_filter_xz = 0;
    for k = 1 : nz;
        for i = 1 : nx;
            uu_xz = uu_xz + U(j, i, k) * U(j, i, k) / ( nx * nz );
            uu_filter_xz = uu_filter_xz + U(j, i, k) * filtered_vel2(j, i, k) / ( nx * nz );
        end
    end
    U_filter_threshold(j) = ( uu_filter_xz / uu_xz ) * U_threshold;
end

% Local extrema in the cross-flow plane
clear local_max;
clear local_min;
clear streak_detect;
clear sub_array;
streak_detect(1:ny, 1:nx, 1:nz) = 0;
for i = 1 : nx;
    for j = 2 : ny-1;
        for k = 2 : nz-1;
            sub_array = filtered_vel2( j-1:j+1, i, k-1:k+1 );
            local_max = max( sub_array(:) );
            local_min = min( sub_array(:) );
            if( filtered_vel2( j, i, k) == local_max && abs( local_max ) > U_filter_threshold(j))
                streak_detect(j, i, k) = 1.0;
            end
            if( filtered_vel2( j, i, k) == local_min && abs( local_min ) > U_filter_threshold(j))
                streak_detect(j, i, k) = -1.0;
            end
        end     % k-loop
    end         % j-loop
end             % i-loop



% Plot results
clear X;
clear Y;
clear Z;
y = linspace(0, (ny-1)*spacing_y, ny) + y_min;
[X, Y, Z] = meshgrid(x, y, z);

figure;
subplot(2, 2, 1);
p = patch(isosurface(X, Y, Z, U, -1.0*U_threshold));
isonormals(X, Y, Z, U, p)
p.FaceColor = 'blue';
p.EdgeColor = 'none';
daspect([1,1,1])
view(176, -58);
axis tight
camlight
lighting gouraud
title('Velocity isosurface (raw)', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
zlabel('z', 'FontSize', 12, 'FontWeight', 'bold');

for j = 1 : ny;
    for k = 1 : nz;
        for i = 1 : nx;
            filtered_vel2(j, i, k) = -filtered_vel2(j, i, k) / U_filter_threshold(j);
        end
    end
end

subplot(2, 2, 2);
p = patch(isosurface(X, Y, Z, filtered_vel2, 1.0));
isonormals(X, Y, Z, filtered_vel2, p)
p.FaceColor = 'blue';
p.EdgeColor = 'none';
daspect([1,1,1])
view(176, -58);
axis tight
camlight
lighting gouraud
title('Velocity isosurface (filtered)', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
zlabel('z', 'FontSize', 12, 'FontWeight', 'bold');

subplot(2, 2, 4);
p = patch(isosurface(X, Y, Z, streak_detect, -0.5));
isonormals(X, Y, Z, streak_detect, p)
p.FaceColor = 'red';
p.EdgeColor = 'none';
daspect([1,1,1])
view(176, -58);
axis tight
camlight
lighting gouraud
title('Detected core', 'FontSize', 13, 'FontWeight', 'bold');
xlabel('x', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('y', 'FontSize', 12, 'FontWeight', 'bold');
zlabel('z', 'FontSize', 12, 'FontWeight', 'bold');


