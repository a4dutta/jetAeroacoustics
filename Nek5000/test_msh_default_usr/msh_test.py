import sys
sys.path.append('../gmsh2nek')
from mshconvert import *

convert('msh_test.msh', \
        bcs={20:'v', 21:'W', 22:'W', 23:'O'},
        start_rea='start.rea', end_rea='end.rea')
