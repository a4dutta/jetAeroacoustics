// Gmsh project created on Mon Nov 13 09:20:15 2017
Point(1) = {0, 0, 0, 0.05};
Point(2) = {10, 0, 0, 0.05};
Point(3) = {10, 10, 0, 0.05};
Point(4) = {0, 10, 0, 0.05};
Line(1) = {4, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Line(4) = {3, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Transfinite Line {1, 2, 3, 4} = 50 Using Progression 1;
Transfinite Surface {6};
Recombine Surface {6};

Physical Line(20) = {1};
Physical Line(21) = {2};
Physical Line(22) = {4};
Physical Line(23) = {3};
Physical Surface(24) = {6};
