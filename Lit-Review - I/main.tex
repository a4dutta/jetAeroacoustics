\documentclass[titlepage, headings=large]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{bm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{setspace}


\doublespacing

\title{Literature Review - Plane and Confined Jets}
\author{Amitvikram Dutta}
\date{January 2018}



\begin{document}

\maketitle

\section{Introduction}
This review focuses on the work done on characterizing both plane and confined jets over the past 40 years. As such, it does not go into the full details of defining the exact topological details of the flows and their applications etc. These will be included in a separate introductory chapter or even in this section at a later stage.

\section{Properties of plane jets}

\subsection{Mean quantities}

The earliest studies of plane jets were carried out by Schlichting \cite{schlichting1933} and Bickley \cite{bickley1937}. Since then, the evolution and characteristics of a jet produced from a rectangular slot of \textit{w} x \textit{h} where \textit{w} $>>$ \textit{h} have attracted significant attention from the academic community \cite{miller1957,van1958,sato1960,heskestad1965}.

Subsequent to \cite{schlichting1933} and \cite{bickley1937}, a series of studies \cite{bradbury1965,gutmark1976,krothapalli1981,thomas1986} provided a complete picture of the flow properties of the plane jet. 
Their results showed an initial region of undisturbed flow which would extend upto 4 - 6 \textit{h} downstream before the symmetrically growing shear-layers met at the centreline. For the subsequent 14 - 16 jet diameters, the jet underwent a transition region where scaled values of the centreline velocity and jet half-width, decayed and grew with the normalized streamwise co-ordinate x/h. Subsequent to this region, in the far - field, these values asymptotically approached those of a round jet and turbulent statistics tended towards isotropy.

The exact nature of the evolutionary relationships were formally derived from experimental data by George \cite{george1989}. The centreline jet velocity is seen to vary as $U_{c} \propto x^{0.5}$ while the jet spread rate, characterized by the half-width of the jet is seen to grow as $y_{0.5} \propto x$. Table 1 provides a few indicative values of previous experiments conducted on plane jets at various jet exit Reynolds numbers and the constants of proportionality that were obtained for the aforementioned centreline velocity decay and jet spread - rate relationships.
\\
\begin{table}[h!]
    \centering
    \begin{tabular}{c c c c}
    Source & Reynolds Number & Decay Rate & Spread Rate\\
    \hline
    Kothrapalli et. al. \cite{krothapalli1981} & 12,000 & -- & 0.109\\
    Bradbury \cite{bradbury1965} & 30,000 & 0.16 & 0.11\\
    Gutmark and Wygnaski \cite{gutmark1976} & 30,000 & 0.17 & 0.10\\
    Browne et. al. \cite{browne1984} & 7,700 & 0.147 & 0.112\\
    Deo et. al. \cite{deo2008} & 7,000 & 0.2 & 0.14\\
    Namer and Otugen \cite{namer1988} & 1,000 & 0.239 & 0.179
    \end{tabular}
    \caption{Jet characteristics showing the jet-exit Reynolds number, far-field decay rate and jet-spread rate}
    \label{tab:my_label}
\end{table}

%Starting from the jet nozzle exit and moving further %downstream (where \textit{x} is the streamwise %direction), the jet flow has three distinct regions %which may be listed as follows:

%\begin{itemize}
%    \item 0 $<$ x/H $<$ 4 - 6 : The potential core
%    \item 4 - 6 $<$ x/H $<$ 20 : The transition %region
%    \item x/H $>$ 20 : The far field
%\end{itemize}

\subsection{Instability and turbulence in the plane jet}

The boundary between the potential core of a jet and the surrounding fluid is inviscidly unstable via the Kelvin-Helmholtz (K-H) mechanism \cite{ho1984}. The instability grows, giving rise to K-H vortices. The shear-layers then widen due to vortex merging \cite{shim2013} until they meet at the end of the potential core.

One of the first indications of large scale structures in the jet flow was the flapping motion of the planar turbulent jet as observed by Goldschmit and Bradshaw \cite{goldschmidt1981}.
The large scale structures formed prior to the end of the potential core in a jet emerging from a smooth contraction nozzle are symmetrical around the centre-line of the jet \cite{thomas1986}. Thereafter, as the shear layers merge the symmetrical vortices are gradually displaced relative to one another \cite{shim2013} and develop into an anti-symmetric pattern \cite{deo2008,sato1960}. This asymmetrical array of vortices propagate at 60 percent of the local centreline mean velocity before they evolve into the self-similar region downstream \cite{stanley2002}. The flapping motion of the jet was also explained on the basis of this asymmetrical array of vortices by \cite{oler1982}.

Shimm \cite{shim2013} determined that the initial symmetrical structures are formed as a result of the first vortex pairing prior to the end of the potential core and as they propagate downstream, non-linear interactions give rise to new structures that still maintain their symmetry but have a different propagation frequency. The same study also determined that the stream-normal locations of the peak turbulence intensity move closer to the centreline with increase in streamwise coordinate, thereby indicating further interactions of structures beyond the end of the potential core.

The turbulence in the jet flow field can also be charecterized by the turbulent intensity ($u^*$), defined as the ratio of the fluctuating velocity to that of the jet exit centreline velocity i.e. $u^* = u^{'}/U_c$. Kuhlman \cite{kuhlman1987} showed that this property had a significant role in determining jet conditions along the streamwise direction. Moving from the jet exit along the streamwise direction, $u^*$ rises from the exit value to the local maximum, as shown in Figure 1, before converging to an asymptotic value in the far-field \cite{namer1988,klein2003,gutmark1976}.
\begin{figure}
    \centering
    \includegraphics[scale=0.65]{uc.eps}
    \caption{Streamwise variation of centreline turbulence intensity, adapted from \cite{deo2008}. Note:  HC77 - Hussain and Clark \cite{hussein1994},
GW76 - Gutmark and Wygnanski \cite{gutmark1976}, B67 - Bradbury \cite{bradbury1965}, H65 - Heskestad \cite{heskestad1965},
BARC82 - Browne et al. \cite{browne1984}, NO88 - Namar and Otugen \cite{namer1988} and TG86 - Thomas
and Goldschmidt \cite{thomas1986}}
    \label{fig:my_label}
\end{figure}
The reason for this peak is resides in the characteristics of the boundary layers at the nozzle exit as postulated by \cite{browne1984}. In particular, the laminar boundary layers issuing from a smooth contraction jet exit profile result in the symmetrical vortex structures, whose interaction at the end of the potential core region gives rise to the peak in turbulent intensity \cite{shim2013}.

\subsection{Initial and Boundary Conditions}

While the establishing experiments \cite{bradbury1965,gutmark1976,krothapalli1981,thomas1986} agreed in general on the trends of centreline decay and spread - rate, their data showed a large scatter in the rates of evolution of the mean flow field and turbulent statistics.

This scatter was explained by George \cite{george1989}, who showed that since the initial entrainment of the surrounding flow by the jet provides the overwhelming majority of the mass flow rate that in turn determines jet behavior further downstream, the differences in rates of evolution observed, was due to differing initial and boundary conditions.

The primary initial and boundary conditions that were found to govern the behavior of the jet were detailed by \cite{deo2005thesis} and may be listed as follows :

\begin{itemize}
    \item The jet exit Reynolds number based on bulk mean exit velocity
    \item The aspect ratio of the jet nozzle exit
    \item The nozzle geometry or the inner-wall nozzle contraction profile
    \item The presence or absence of sidewalls
\end{itemize}

This review will elaborate on two of the aforementioned conditions, namely, the jet exit Reynolds numbers and the effects of sidewalls.

\subsubsection{Jet Exit Reynolds number}

The jet exit Reynolds number is defined on the basis of the area-averaged exit velocity and the jet slot height $h$.

\begin{equation}
    Re = U_{c}h/\nu
\end{equation}

The Reynolds number affects the development of the jet through the boundary layer at the nozzle exit. This was first categorized by Zaman \cite{zaman1985}. It was also noted, that for round jets, the centreline velocity decay and the spread rates are almost constant at high Reynolds numbers \cite{panchapakesan1993,hussein1994}. Deo et. al. \cite{deo2008}, conducted experiments with plane jets with Re upto 25,000. It was observed that an increase in Re causes the length of the potential core to decrease and a consequent increase in the near-field spreading rate. On the other hand, the far-field rates show the the opposite dependence on \textit{Re}. They decrease asymptotically with increase in \textit{Re}, with a comparable rate of convergence to that of the near field.

The fluctuating velocity field is also highly dependent on Re $<$ 25,000 range. Mi, Nathan and Nobes \cite{mi2001} were the first to show that the local maxima of the magnitude of turbulent intensity in the near field depends on initial conditions of the jet flow. Deo et. al. \cite{deo2008} determined that both the magnitude of the initial turbulent intensity peak and its downstream location decreased with increase from 1500 $<$ Re $<$ 16,500 from which it was deduced that this local maxima was asociated with the primary vortex formation. However, the asymptotic value of the far-field intensity shows an opposite dependence and increases with increase in Re.


The variation of the fluctuating velocity field is intimately related to the variation in the large scale structures that were introduced in Section 2.2.
Namer and Otugen \cite{namer1988} were among the first to study the effects of these structures with variations in Re ($1,000 < Re < 7,000$). Their study showed that higher Reynolds numbers broadened the turbulent spectrum, leading to a similar amount of energy being distributed among a larger range of eddy scales. This decreased the energy available to the large scale structures and consequently decreased the centreline velocity decay and jet spreading rates. 

Deo et. al.\cite{deo2008} also observed that while both symmetric and anti-symmetric structures were present in the near field, the symmetric structures tended to dominate with increase in Re thereby implying that they were more efficient at near field entrainment than anti-symmetric vortex arrays.

An additional Reynolds number can be defined for a jet. The local Reynolds number $Re_{local}$ is defined as 

\begin{equation}
    Re_{local} = 2(y_{0.5})(U_{c})/\nu
\end{equation}

George [2005] analytically showed that for all plane jets $Re_{local} ~ x^{0.5}$, which implied all plane eventually transition to turbulence in the far-field.

\subsection{Sidewalls}

A plane jet is formally defined as a statistically two dimensional flow, with a dominant velocity in the streamwise direction. In order enhance this two-dimensionality over a greater part of the jet's evolution downstream, several authors \cite{browne1984,heskestad1965,lemieux1985} have stressed the importance of having sidewalls.

The first study to compare characteristics of plane jets having sidewalls to those without was by Hitchman \cite{hitchman1990}. This was followed up by a detailed experimental investigation by Deo, Nathan Mi \cite{deo2007comparison}. In this work it was determined that sidewalls, while performing their intended function of maintaining plane jet two dimensionality, also cause fundamental differences in those setups that have them from those that do not. In particular, while jets without sidewalls eventually asymptoted into a round jet centreline velocity decay of $U_{c} \propto x^{-1}$, the jet with side-walls continued with a decay rate of $U_{c} \propto x^{-0.5}$ well into the far-field.

There were also clear differences noted in the near field with the jet with sidewalls having a longer potential core and a lower entrainment rate than the one without. This is explained by the increased two dimensionality of the jet with sidewalls with a roller-like vortices instead of the more ring-like structures found in jets without sidewalls. Additionally, these roller like vortices have a lower dominant frequency shedding rate than the ring - vortices.

The two-dimensionality of the sidewalled jet was also confirmed in the numerical work of Rangarajan \cite{rangarajan2012}, who used a $k - \epsilon$ model with standard wall functions to characterize the evolution of a plane jet.


\section{Confined Jets}

Confined jets refer to a particular class of flows where the jet spreading is confined in the direction normal to the jet centreline. These flows are characterized by the presence of an appreciable co-flow parallel to the streamwise direction of the primary jet flow. As the jet flow widens after the end of the potential core, depending on appropriate initial and boundary conditions, if this secondary co-flow is insufficient to meet the entrainment needs of the primary jet, a recirculation centre develops close to the wall on either side of the jet centre-line. Downstream of this, the flow expands to fill the confining geometry and thereafter continues as fully-developed pipe flow \cite{rajaratnam1976}.


Hill \cite{hill1965} was the first to derive a full analytical expression for the developement of confined jets. He assumed a self-similar velocity profile wit $U/U_{c} = f(y/R)$ and a constant pressure across the recirculatory zone.Three equations, the continuity, momentum and moment-of-momentum equations were solved with some empirical coefficients obtained from free-jet data. The solutions were deemed to be valid for the regions of jet developement preceding the recirculation zone. Although they showed good agreement with low confinement levels, the solutions diverged significantly from experimental results of highly confined jets due to the significant assumption made regarding the pressure in the recirculation zone.

Craya and Curtet \cite{craya1955,curtet1958} in a series of two studies laid out a much improved theory of confined jets. Once again the integral continuity, momentum and moment of momentum equations were solved using an assumption regarding the self-similarity of the velocity profile. The pressure was derived from the Bernoulli equation. A dimensionless parameter \textit{m} was defined which combined the effects of the velocity ratio of the primary flow to the co-flow and the radius ratio of the confinement. The solution correctly predicted the onset of recirculation for $m > 0.92$. The quantity $1/\sqrt{m}$ was later named as the Craya-Curtet number (Ct) and used as a parameter to characterize a confined jet flow.

Razinsky and Brighton \cite{razinsky1971} extended the analytical confined jets for all regions of flow developement. The velocity was not assumed to be self-similar across all radial sections. Instead 4 separate equations were written to describe the jet developement in the radial direction. Boundary Layer effects were considered and shear stress was calculated using a wall friction factor derived from \cite{schlichting1968} formula. They obtained good agreement with companion experiments that were performed with centreline velocity decay, radial velocity decay and wall-pressure distributions.

Zhu and Shih \cite{zhu1994} performed numerical simulations of confined jet flows where they compared a $k - \epsilon$ RANS model and a Realizable Reynolds Stress Algebraic Equation (RRSAE) for confined jets having low, medium and high recirculation. They observed that for the high recirculation case both models showed considerable deviations from experimental results and concluded that this was a result of the use of wall models near the confining walls.

Yule and Damou \cite{yule1991} were the first to study turbulent quantities of confined jets at different \textit{Ct}. They observed that unlike free - jets the far - filed turbulent intensity continued to increase till the end of the experimental domain and did not asymptote to a value as was observed with free jets. It was thus concluded that the initial and boundary conditions of confined jets had a greater effect on their downstream developement than for free jets.

Kong et. al. \cite{kong2011expt} determined the shape of the large - scale structures using linear stochastic estimation on time-resolved Particle Image Velocimetry (PIV) data. These structures were observed to have a spindle shape with their major axes tilting in the streamwise direction. A companion set of numerical results were obtained using the Large-Eddy Simulation (LES) technique \cite{kong2012les}, which validated the use of this method as a valuable investigative tool for further studies of confined jets.

\section{Conclusions}

The characteristics of evolution of plane jets have been the subject of a great deal of study over the last 40 years or so. It has been conclusively shown that their mean and time-resolved properties downstream of the exit depend overwhelmingly on initial and boundary conditions of the setup. The confined jet, which represents one such setup, is of considerable practical and industrial importance. While the characteristics of confined jets have both been predicted by analytical models \cite{hill1965,curtet1958,razinsky1971}, numerical simulations \cite{kong2012les} and measured via experiments \cite{yule1991,kong2011expt}, there is a dearth of literature which provides a systematic study of the dependence of the behavior of confine jets on initial conditions. Additionally, while the role of coherent structures has been established as being of direct relevance in the developement of the plane jet, their behavioral trends under varying initial and boundary conditions of confinement has received little attention. Modern experimental tools such as Particle Image Velocimetry (PIV) which offer the capability of non-intrusive whole-field measurements instead of point measurements and validated high - quality numerical simulations using LES offer a clear avenue of approach to address this question.

\bibliographystyle{unsrt}
\bibliography{references}
\end{document}
